/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package domain;

/**
 *
 * @author Olivier Jean
 */
public class SimudemieMode {
    
    public static enum simudemieMode {CREATIONPAYS, CREATIONLIEN, CARTEDUMONDE, DIVISIONPAYS, MODIFICATIONPAYS, CREATIONPAYS_REG, SIMULATION, VUEPAYS, MESURESANITAIRE, MALADIE, STATISTIQUE};
    
    private simudemieMode selectedMode;
    
    public SimudemieMode()
    {
        selectedMode = simudemieMode.CARTEDUMONDE;
    }
    
    public simudemieMode getMode()
    {
        return selectedMode;
    }
    
    public void setMode(simudemieMode nouveauMode)
    {
        selectedMode = nouveauMode;
    }
    
}

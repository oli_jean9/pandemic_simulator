/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package domain.drawing;

import domain.Lien;
import domain.Pays;
import domain.Region;
import domain.SimudemieController;
import domain.SimudemieMode;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Point;
import java.util.ArrayList;
import java.awt.Polygon;
import java.awt.geom.Line2D;
import java.awt.*;
import java.awt.geom.AffineTransform;

/**
 *
 * @author Olivier Jean
 */
public class DrawerSimudemie
{
    private final SimudemieController controller;
    private Dimension initialDimension;
    private Double zoomFactor; 
    private Double xRel;
    private Double yRel;
    private double xOffset = 0;
    private double yOffset = 0;
    private double prevZoomFactor = 1;
    private SimudemieMode modeSimudemie;
    
    public DrawerSimudemie(SimudemieController controller, Dimension initialDimension, Double zoomFactor, Double xRel, Double yRel, SimudemieMode mode)
    {
        this.controller = controller;
        this.initialDimension = initialDimension;
        this.zoomFactor = zoomFactor;
        this.xRel = xRel;
        this.yRel = yRel;
        this.modeSimudemie = mode;
    }
    
    public void draw(Graphics g)
    {
       drawPays((Graphics2D)g);
        
       if(zoomFactor > 1.3 || modeSimudemie.getMode() == SimudemieMode.simudemieMode.VUEPAYS || modeSimudemie.getMode() == SimudemieMode.simudemieMode.STATISTIQUE)
       {
        drawRegion((Graphics2D)g);
        drawbordRegion((Graphics2D)g);
       }

       drawbordPays((Graphics2D)g);
       
      
            drawLiens(g);
    }
    
    public void drawRegion(Graphics2D g)
    {
        ArrayList<Pays> listePays = controller.getListePays();
        for (Pays pays:listePays)
        {
            ArrayList<Region> listeRegion = controller.getListeRegion(pays);
            for (Region region:listeRegion)
            {
                ArrayList<Point> pointPolygone = region.getForme().getListePoints();
                int xPoly[]= new int[pointPolygone.size()];
                int yPoly[]= new int[pointPolygone.size()];
                int indexPoint =0;
                for (Point point:pointPolygone)
                {
                    xPoly[indexPoint] = (int)point.getX();
                    yPoly[indexPoint] = (int)point.getY();
                    indexPoint++;
                }
                Shape poly = new Polygon(xPoly, yPoly, xPoly.length);
                
            if (zoomFactor != 1.00) {
            AffineTransform at = new AffineTransform();
            

            double zoomDiv = zoomFactor / prevZoomFactor;
            

            xOffset = (zoomDiv) * (xOffset) + (1 - zoomDiv) * xRel;
            yOffset = (zoomDiv) * (yOffset) + (1 - zoomDiv) * yRel;

            at.translate(xOffset, yOffset);
            at.scale(zoomFactor, zoomFactor);
            prevZoomFactor = zoomFactor;
            poly = at.createTransformedShape(poly);}

            float pctAtteintMaladie = region.getPourcentageInfecte()+region.getPourcentageMort();

            if (pctAtteintMaladie == 0.0)
                g.setColor(new Color(160,160,160));
            
            else if (pctAtteintMaladie > 0.0 && pctAtteintMaladie <= 0.10)
                g.setColor(new Color(102,255,102));
            
            else if (pctAtteintMaladie > 0.10 && pctAtteintMaladie <= 0.20)
                g.setColor(new Color(178,255,102));
            
            else if (pctAtteintMaladie > 0.20 && pctAtteintMaladie <= 0.30)
                g.setColor(new Color(255,255,102));
            
            else if (pctAtteintMaladie > 0.30 && pctAtteintMaladie <= 0.40)
                g.setColor(new Color(255,255,0));
            
            else if (pctAtteintMaladie > 0.40 && pctAtteintMaladie <= 0.50)
                g.setColor(new Color(204,204,0));
            
            else if (pctAtteintMaladie > 0.50 && pctAtteintMaladie <= 0.60)
                g.setColor(new Color(255,178,102));
            
            else if (pctAtteintMaladie > 0.60 && pctAtteintMaladie <= 0.70)
                g.setColor(new Color(255,153,51));
            
            else if (pctAtteintMaladie > 0.70 && pctAtteintMaladie <= 0.80)
                g.setColor(new Color(255,128,0));
            
            else if (pctAtteintMaladie > 0.80 && pctAtteintMaladie <= 0.90)
                g.setColor(new Color(255,0,0));
            
            else if (pctAtteintMaladie > 0.90 && pctAtteintMaladie < 1)
                g.setColor(new Color(204,0,0));
            else
                g.setColor(new Color(102,0,0));
            
            g.fill(poly);
            }
        }
    }
    public void drawbordRegion(Graphics2D g)
    {
        ArrayList<Pays> listePays = controller.getListePays();
        for (Pays pays:listePays)
        {
            ArrayList<Region> listeRegion = controller.getListeRegion(pays);
            for (Region region:listeRegion)
            {
                ArrayList<Point> pointPolygone = region.getForme().getListePoints();
                int xPoly[]= new int[pointPolygone.size()];
                int yPoly[]= new int[pointPolygone.size()];
                int indexPoint =0;
                for (Point point:pointPolygone)
                {
                    xPoly[indexPoint] = (int)point.getX();
                    yPoly[indexPoint] = (int)point.getY();
                    indexPoint++;
                }
                Shape poly = new Polygon(xPoly, yPoly, xPoly.length);
                 
            if (zoomFactor != 1.00) {
            AffineTransform at = new AffineTransform();

            double zoomDiv = zoomFactor / prevZoomFactor;

            xOffset = (zoomDiv) * (xOffset) + (1 - zoomDiv) * xRel;
            yOffset = (zoomDiv) * (yOffset) + (1 - zoomDiv) * yRel;

            at.translate(xOffset, yOffset);
            at.scale(zoomFactor, zoomFactor);
            prevZoomFactor = zoomFactor;
            poly = at.createTransformedShape(poly);}


                g.setColor(new Color(0,0,0));
                g.draw(poly);
            }
        }
    }

    public void drawPays(Graphics2D g)
    {
        ArrayList<Pays> listePays = controller.getListePays();
        for (Pays pays:listePays)
        {
            ArrayList<Point> pointPolygone = controller.getListePoint(pays);
            int xPoly[]= new int[pointPolygone.size()];
            int yPoly[]= new int[pointPolygone.size()];
            int indexPoint =0;
            for (Point point:pointPolygone)
            {
                xPoly[indexPoint] = (int)point.getX();
                yPoly[indexPoint] = (int)point.getY();
                indexPoint++;
            }
            Shape poly = new Polygon(xPoly, yPoly, xPoly.length);
            
            
              if (zoomFactor != 1.00) {
            AffineTransform at = new AffineTransform();
            

            double zoomDiv = zoomFactor / prevZoomFactor;
            
            xOffset = (zoomDiv) * (xOffset) + (1 - zoomDiv) * xRel;
            yOffset = (zoomDiv) * (yOffset) + (1 - zoomDiv) * yRel;
            at.translate(xOffset, yOffset);
            at.scale(zoomFactor, zoomFactor);
            prevZoomFactor = zoomFactor;
            poly = at.createTransformedShape(poly);}
            
            float pctAtteintMaladie = (pays.getPopulationInfecte() + pays.getPopulationMorte())/(float)pays.getPopulation();
            
            if (pctAtteintMaladie == 0.0)
                g.setColor(new Color(160,160,160));
            
            else if (pctAtteintMaladie > 0.0 && pctAtteintMaladie <= 0.10)
                g.setColor(new Color(102,255,102));
            
            else if (pctAtteintMaladie > 0.10 && pctAtteintMaladie <= 0.20)
                g.setColor(new Color(178,255,102));
            
            else if (pctAtteintMaladie > 0.20 && pctAtteintMaladie <= 0.30)
                g.setColor(new Color(255,255,102));
            
            else if (pctAtteintMaladie > 0.30 && pctAtteintMaladie <= 0.40)
                g.setColor(new Color(255,255,0));
            
            else if (pctAtteintMaladie > 0.40 && pctAtteintMaladie <= 0.50)
                g.setColor(new Color(204,204,0));
            
            else if (pctAtteintMaladie > 0.50 && pctAtteintMaladie <= 0.60)
                g.setColor(new Color(255,178,102));
            
            else if (pctAtteintMaladie > 0.60 && pctAtteintMaladie <= 0.70)
                g.setColor(new Color(255,153,51));
            
            else if (pctAtteintMaladie > 0.70 && pctAtteintMaladie <= 0.80)
                g.setColor(new Color(255,128,0));
            
            else if (pctAtteintMaladie > 0.80 && pctAtteintMaladie <= 0.90)
                g.setColor(new Color(255,0,0));
            
            else if (pctAtteintMaladie > 0.90 && pctAtteintMaladie < 1)
                g.setColor(new Color(204,0,0));
            else
                g.setColor(new Color(102,0,0));
            
            g.fill(poly);
        }   
    }
       
    public void drawLiens(Graphics g)
    {
        ArrayList<Lien> listeLiens = controller.getListeLiens();
        Graphics2D g2 = (Graphics2D)g;
        g2.setStroke(new BasicStroke(3));
        
        for (Lien lien:listeLiens)
        {
            switch (lien.getType())
            {
                case AERIEN:
                    g2.setColor(new Color(0,76,153));
                    Shape shape = new Line2D.Double(lien.getPointPaysA().getX(),
                            lien.getPointPaysA().getY(),
                            lien.getPointPaysB().getX(),
                            lien.getPointPaysB().getY());
                    
                    if (zoomFactor != 1.00) {
            AffineTransform at = new AffineTransform();
            

            double zoomDiv = zoomFactor / prevZoomFactor;
            
            xOffset = (zoomDiv) * (xOffset) + (1 - zoomDiv) * xRel;
            yOffset = (zoomDiv) * (yOffset) + (1 - zoomDiv) * yRel;
            at.translate(xOffset, yOffset);
            at.scale(zoomFactor, zoomFactor);
            prevZoomFactor = zoomFactor;
            shape = at.createTransformedShape(shape);}
                    g2.draw(shape);
                    
                    
                    break;
                case MARITIME:
                    g2.setColor(new Color(0,153,153));
                  Shape shapeMaritime = new Line2D.Double(lien.getPointPaysA().getX(),
                            lien.getPointPaysA().getY(),
                            lien.getPointPaysB().getX(),
                            lien.getPointPaysB().getY());
                    
                    
                    
                            if (zoomFactor != 1.00) {
            AffineTransform at = new AffineTransform();
            

            double zoomDiv = zoomFactor / prevZoomFactor;
            
            xOffset = (zoomDiv) * (xOffset) + (1 - zoomDiv) * xRel;
            yOffset = (zoomDiv) * (yOffset) + (1 - zoomDiv) * yRel;
            at.translate(xOffset, yOffset);
            at.scale(zoomFactor, zoomFactor);
            prevZoomFactor = zoomFactor;
            shapeMaritime = at.createTransformedShape(shapeMaritime);}
                    g2.draw(shapeMaritime);
                    break;
            }
        }
    }
    
    public void drawBordersPays(Graphics g)
    {
        ArrayList<Pays> listePays = controller.getListePays();
        for (Pays pays:listePays)
        {
            ArrayList<Point> pointPolygone = controller.getListePoint(pays);
            int index =0; 
            for (Point point:pointPolygone)
            {
                Point pointB = pointPolygone.get((index+1)%pointPolygone.size());
                
                g.setColor(Color.BLACK);
                g.drawLine((int)point.getX(),(int)point.getY(),(int)pointB.getX(),(int)pointB.getY());
                index++;
            }
        } 
    }
    
    public void drawbordPays(Graphics2D g)
    {
        ArrayList<Pays> listePays = controller.getListePays();
        for (Pays pays:listePays)
        {
            ArrayList<Point> pointPolygone = controller.getListePoint(pays);
            int xPoly[]= new int[pointPolygone.size()];
            int yPoly[]= new int[pointPolygone.size()];
            int indexPoint =0;
            for (Point point:pointPolygone)
            {
                xPoly[indexPoint] = (int)point.getX();
                yPoly[indexPoint] = (int)point.getY();
                indexPoint++;
            }
            Shape poly = new Polygon(xPoly, yPoly, xPoly.length);
            
            
              if (zoomFactor != 1.00) {
            AffineTransform at = new AffineTransform();
            

            double zoomDiv = zoomFactor / prevZoomFactor;
            
            xOffset = (zoomDiv) * (xOffset) + (1 - zoomDiv) * xRel;
            yOffset = (zoomDiv) * (yOffset) + (1 - zoomDiv) * yRel;
            at.translate(xOffset, yOffset);
            at.scale(zoomFactor, zoomFactor);
            prevZoomFactor = zoomFactor;
            poly = at.createTransformedShape(poly);}
            
            g.setColor(new Color(0,0,0));
            g.draw(poly);
        }   
    }

    public void drawBordersRegion(Graphics g)
    {
        ArrayList<Pays> listePays = controller.getListePays();
        for (Pays pays:listePays)
            
            for (Region region: pays.getListeRegion())
            {
                ArrayList<Point> pointPolygone = region.getForme().getListePoints();
                int index =0; 
                for (Point point:pointPolygone)
                {
                    Point pointB = pointPolygone.get((index+1)%pointPolygone.size());
                    g.setColor(Color.GRAY);
                    g.drawLine((int)point.getX(),(int)point.getY(),(int)pointB.getX(),(int)pointB.getY());
                    index++;
                }
            } 
    }
}

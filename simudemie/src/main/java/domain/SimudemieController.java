/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package domain;

import java.util.ArrayList;
import java.awt.Point;
import domain.Pays;
import domain.CarteDuMonde;
import domain.MesureSanitaire;
import java.io.IOException;
import java.util.Stack;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 *
 * @author alexa
 */
public class SimudemieController {
    private Projet projet;
    public ArrayList<Projet> ListeProjetController;
    private Stack undoStack;
    private Stack redoStack;

    public SimudemieController(String nomProjet){
        //Initialise la simulation
        projet = new Projet(nomProjet);
        ListeProjetController = new ArrayList<Projet>();
        undoStack = new Stack();
        redoStack = new Stack();
        enregistreAction();
    }
   
    
    public void afficherStatistique(){
        projet.getSimulation().afficherStatistique();
    }
    
    public static byte[] serialize(Object obj) throws IOException {
        try(ByteArrayOutputStream b = new ByteArrayOutputStream()){
            try(ObjectOutputStream o = new ObjectOutputStream(b)){
                o.writeObject(obj);
            }
            return b.toByteArray();
        }
    }

    public static Object deserialize(byte[] bytes) throws IOException, ClassNotFoundException {
        try(ByteArrayInputStream b = new ByteArrayInputStream(bytes)){
            try(ObjectInputStream o = new ObjectInputStream(b)){
                return o.readObject();
            }
        }
    }
    
    public void undo(){
        if (!undoStack.isEmpty()) {
            try{
            byte[] bytesProjetCourant =(byte[]) undoStack.pop();
            Projet courant = (Projet)(deserialize(bytesProjetCourant));
            if (!undoStack.isEmpty()) {
                redoStack.push(serialize(courant));
                byte[] bytesLastProjet =(byte[]) undoStack.pop();
                Projet lastProjet = (Projet)(deserialize(bytesLastProjet));
                projet.setNomProjet(lastProjet.getNomProjet());
                projet.setSimulation(lastProjet.getSimulation());
                projet.setCarteDuMonde(lastProjet.getCarteDuMonde());
                projet.setListeMaladie(lastProjet.getListeMaladie());
            }
            enregistreAction();
            }catch (IOException e)
            {
                System.out.println("exception undo"); 
            }catch (ClassNotFoundException e)
            {
                System.out.println("exception undo"); 
            }
        }
    }
    public void redo()
    {
        if (!redoStack.isEmpty()){
            try{
            byte[] bytesProjetCourant =(byte[]) redoStack.pop();
            Projet courant = (Projet)(deserialize(bytesProjetCourant));
            undoStack.push(serialize(courant));
            projet.setNomProjet(courant.getNomProjet());
            projet.setSimulation(courant.getSimulation());
            projet.setCarteDuMonde(courant.getCarteDuMonde());
            projet.setListeMaladie(courant.getListeMaladie());
            }catch (IOException e)
            {
                System.out.println("exception undo"); 
            }catch (ClassNotFoundException e)
            {
                System.out.println("exception undo"); 
            }
        }
    }
    public void enregistreAction() {
        try{
            undoStack.push(serialize(projet));
        }
        catch(IOException e){
           System.out.println("exception pas serialisable"); 
        }
        }
    
    
    public void creerPays(int p_population, String p_nomPays, Forme p_forme,
            float p_txDepMaritime, 
            float p_txDepTerrestre, 
            float p_txDepAerien, 
            float p_txDepRegion,
            float p_pctPopSaine,
            float p_pctPopInfecte,
            float p_pctPopMorte){
        projet.getCarteDuMonde().creerPays(p_population, p_nomPays, p_forme, p_txDepMaritime,
                p_txDepTerrestre, p_txDepAerien, p_txDepRegion, p_pctPopSaine, p_pctPopInfecte, p_pctPopMorte);
    }
    public ArrayList<Pays> getListePays(){
        return projet.getCarteDuMonde().getListePays();
    }
    
    public void setNomPays(Pays pays,String _nom) {
        int indexPays = projet.getCarteDuMonde().getListePays().indexOf(pays);
        
       ArrayList<Pays> payss = projet.getCarteDuMonde().getListePays();
        payss.get(indexPays).setNomPays(_nom);
    }
    
    public void setPopulation(Pays pays, int _pop){
         int indexPays = projet.getCarteDuMonde().getListePays().indexOf(pays);
        
       ArrayList<Pays> payss = projet.getCarteDuMonde().getListePays();
        payss.get(indexPays).setPopulation(_pop);
    }
    
    public void setTxDepTerrestre(Pays pays, float _tx){
         int indexPays = projet.getCarteDuMonde().getListePays().indexOf(pays);
        
       ArrayList<Pays> payss = projet.getCarteDuMonde().getListePays();
        payss.get(indexPays).setTxDepTerrestre(_tx);
    }
    
     public void setTxDepMaritime(Pays pays, float _tx){
         int indexPays = projet.getCarteDuMonde().getListePays().indexOf(pays);
        
       ArrayList<Pays> payss = projet.getCarteDuMonde().getListePays();
        payss.get(indexPays).setTxDepMaritime(_tx);
    }
     
    public void setTxDepAerien(Pays pays, float _tx){
         int indexPays = projet.getCarteDuMonde().getListePays().indexOf(pays);
        
       ArrayList<Pays> payss = projet.getCarteDuMonde().getListePays();
        payss.get(indexPays).setTxDepAerien(_tx);
    }
    
    public void setTxDepRegionnal(Pays pays, float _tx)
    {
    int indexPays = projet.getCarteDuMonde().getListePays().indexOf(pays);

    ArrayList<Pays> payss = projet.getCarteDuMonde().getListePays();
    payss.get(indexPays).setTxDepRegion(_tx);
    }
      
      
            
    public void creerRegion(Pays pays, String p_nomRegion, 
            float p_pourcentagePopulation, float p_pourcentageSaine,
            float p_pourcentageInfecte,
            float p_pourcentageMort, Forme p_forme){
        pays.creerRegion(p_nomRegion, p_pourcentagePopulation, 
            p_pourcentageSaine,
            p_pourcentageInfecte,
            p_pourcentageMort, p_forme);
    }
    
    public void creerMesureSanitaire(String p_nomMesure,
            float p_impactTauxReproduction,
            float p_impactTauxTransmission,
            float p_impactTauxMortalite, 
            float p_impactTauxGuerison, 
            float p_impactTauxDepTerrestre, 
            float p_impactTauxDepAerien, 
            float p_impactTauxDepMaritime, 
            float p_impactTauxDepInterRegion){
            projet.getCarteDuMonde().creerMesureSanitaire(
                p_nomMesure,
                p_impactTauxReproduction, 
                p_impactTauxTransmission,
                p_impactTauxGuerison,
                p_impactTauxMortalite, 
                p_impactTauxDepTerrestre,
                p_impactTauxDepAerien, 
                p_impactTauxDepMaritime,
                p_impactTauxDepInterRegion);
    }
    
    public void initialiserMaladie(String p_nomMaladie,float p_tauxReproduction, float p_tauxContagion, 
            float p_tauxGuerison, float p_tauxMortalite){
        projet.initialiserMaladie(p_nomMaladie,p_tauxReproduction, p_tauxContagion, p_tauxGuerison, 
                p_tauxMortalite);
    }
    
    public void creerMaladie(String p_nomMaladie,float p_tauxReproduction, float p_tauxContagion, 
            float p_tauxGuerison, float p_tauxMortalite){
        projet.creerMaladie(p_nomMaladie, p_tauxReproduction, p_tauxContagion, p_tauxGuerison, 
                p_tauxMortalite);
    }
    
    
    public void modifierMaladie(String p_nomMaladie,float p_tauxReproduction, float p_tauxContagion, 
            float p_tauxGuerison, float p_tauxMortalite){
        projet.modifierMaladie(p_nomMaladie, p_tauxReproduction, p_tauxContagion, p_tauxGuerison, 
                p_tauxMortalite);
    }
    
    public void selectionnerMaladie(String p_nomMaladie){
        projet.selectionnerMaladie(p_nomMaladie);
    }
    
    public void supprimerMaladie(String p_nomMaladie){
        projet.supprimerMaladie(p_nomMaladie);
    }
    
    public ArrayList<Maladie> getListeMaladie(){
        return projet.getListeMaladie();
    }
    
    public void supprimerPays(Pays p_pays)
    {
        getCarteDuMonde().supprimerPays(p_pays);
    }
            
    
    public void initialiserSimulation(int p_jour, 
            CarteDuMonde p_carteMonde, Maladie p_maladie)
    {
        projet.initialiserSimulation(p_jour, p_carteMonde);
    }
    
    public void modifierMesureSanitaire(String p_nomMesure,
            float p_impactTauxReproduction, 
            float p_impactTauxTransmission,
            float p_impactTauxMortalite, 
            float p_impactTauxGuerison, 
            float p_impactTauxDepTerrestre, 
            float p_impactTauxDepAerien, 
            float p_impactTauxDepMaritime, 
            float p_impactTauxDepInterRegion){
        
        for (MesureSanitaire mesure : projet.getCarteDuMonde().getListeMesures()){
            if (mesure.getNomMesure() == p_nomMesure)
            {
                mesure.modifierMesure(p_impactTauxReproduction, p_impactTauxTransmission, 
                p_impactTauxGuerison, p_impactTauxMortalite, 
                p_impactTauxDepTerrestre, p_impactTauxDepAerien, 
                p_impactTauxDepMaritime, p_impactTauxDepInterRegion);
            }
        }
    }
    
    public float getTauxAcceptationMesure(MesureSanitaire mesure, Pays pays, Region region){
        return mesure.getTauxAcceptationMesure(pays, region);
    }
    
    public void enregistrerProjet(String pathFichier){
        projet.enregistrerFichier(pathFichier);
    }
    
    public void chargerProjet(String pathFichier){
        projet.ouvrirFichier(pathFichier);
    }
    
    public void avancerPasDeTemps(){
        projet.getSimulation().avancerPasDeTemps();
    }
    
    public void appliquerMesure(MesureSanitaire p_mesureSanitaire, Pays p_pays, 
            Region p_region, float p_tauxAcceptation){

        p_mesureSanitaire.appliquerMesure( p_pays, p_region, p_tauxAcceptation);
    }
    public void supprimerApplicationMesureSanitaire(MesureSanitaire p_mesure, Pays p_pays){
        p_mesure.retirerApplicationMesure(p_pays);
    }
    public void retirerMesureSanitaire(String p_nomMesureSanitaire){
        projet.getCarteDuMonde().retirerMesureSanitaire(p_nomMesureSanitaire);
    }
    
    public void ajouterLienTerrestre(Pays paysA, Pays paysB, Point pointPaysA, Point pointPaysB){
        projet.getCarteDuMonde().creerLien(paysA, paysB, Lien.typeLien.TERRESTRE, pointPaysA, pointPaysB);
    }
    public void ajouterLienMaritime(Pays paysA, Pays paysB, Point pointPaysA, Point pointPaysB){
        projet.getCarteDuMonde().creerLien(paysA, paysB, Lien.typeLien.MARITIME, pointPaysA, pointPaysB);
    }
    public void ajouterLienAerien(Pays paysA, Pays paysB, Point pointPaysA, Point pointPaysB){
        projet.getCarteDuMonde().creerLien(paysA, paysB, Lien.typeLien.AERIEN, pointPaysA, pointPaysB);
    }
    public void chargerProjet(){
        //projet.charger();
    }
    public void sauvegarderProjet(){
        
    }
    public Projet getProjet()
    {
        return projet;
    }
    public int getJourSimulation()
    {
        return projet.getSimulation().getJour();
    }
    public CarteDuMonde getCarteDuMonde(){
        return projet.getCarteDuMonde();
    }
    public Maladie getMaladie(){
        return projet.getSimulation().getMaladie();
    }
    public ArrayList<MesureSanitaire> getlisteMesureSanitaire(){
        return projet.getCarteDuMonde().getListeMesures();
    }
    public ArrayList<Region> getListeRegion(Pays pays){
        return pays.getListeRegion();
    }
    public void ajouterTousLesLiensPossible()
    {
        projet.getCarteDuMonde().ajouterTousLesLiensPossible();
    }
    public ArrayList<Point> getListePoint(Pays pays)
    {
        return pays.getForme().getListePoints();
    }
    public ArrayList<Point> getListePoint(Region region)
    {
        return region.getForme().getListePoints();
    }
    public ArrayList<Lien> getListeLiens()
    {
        return projet.getCarteDuMonde().getListeLien();
    } 
    
    public void creationAutomatiqueLiensTerrestre(Pays p_pays)
    {
        getCarteDuMonde().creationAutomatiqueLienTerrestre(p_pays);
    }
    public String getNomProjet()
    {
        return projet.getNomProjet();
    }
    
    public void supprimerLien(Pays p_paysA, Pays p_paysB, Lien.typeLien p_typeLien)
    {
        getCarteDuMonde().supprimerLien(p_paysA, p_paysB, p_typeLien);
    }
    
    public Region estDansRegion(Pays p_pays, int x, int y)
    {
        return getCarteDuMonde().estDansRegion(p_pays, x, y);
    }
    
    public int getPopulationTotaleInfecter()
    {
        return getCarteDuMonde().getPopulationTotaleInfecte();
    }
    
    public Pays getPays(String p_nomPays)
    {
        return getCarteDuMonde().getPays(p_nomPays);
    }
    
    public void resetSimulation()
    {
        getProjet().getSimulation().resetSimulation();
    }
    
    public ArrayList<Float> getPopulationSaineStats()
    {
        return getCarteDuMonde().getPopulationSaineStats();
    }
    
    public ArrayList<Float> getPopulationIntecteStats()
    {
        return getCarteDuMonde().getPopulationInfecteStats();
    }
    
    public ArrayList<Float> getPopulationMorteStats()
    {
        return getCarteDuMonde().getPopulationMortStats();
    }
    
    public void clearStats()
    {
        getCarteDuMonde().clearStats();
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import domain.drawing.DrawerSimudemie;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Dimension;
import java.io.Serializable;
import javax.swing.border.BevelBorder;
import java.awt.Point;
import java.util.ArrayList;
import domain.*;
import java.awt.MouseInfo;

/**
 *
 * @author Olivier Jean
 */
public class DrawingPanel extends javax.swing.JPanel implements Serializable
{
    public Dimension initialDimension;
    private MainWindow mainWindow;
    
    public DrawingPanel()
    {
    }
    
    public DrawingPanel(MainWindow mainWindow)
    {
        this.mainWindow = mainWindow;
        setBorder(new javax.swing.border.BevelBorder(BevelBorder.LOWERED));
        int width = (int) (java.awt.Toolkit.getDefaultToolkit().getScreenSize().width);
        setPreferredSize(new Dimension(width,1));
        setVisible(true);
        int height = (int)(width*0.5);
        initialDimension = new Dimension(width,height);
    }

    @Override
    protected void paintComponent(Graphics g)
    {
        if (mainWindow != null)
        {
            super.paintComponent(g); 
            
            double xRel = MouseInfo.getPointerInfo().getLocation().getX() - getLocationOnScreen().getX();
            double yRel = MouseInfo.getPointerInfo().getLocation().getY() - getLocationOnScreen().getY();
            DrawerSimudemie drawerSimudemie = new DrawerSimudemie(mainWindow.controller, initialDimension, mainWindow.getZoomFactor(), xRel, yRel, mainWindow.modeSimudemie);
            drawerSimudemie.draw(g);
            drawFormeTemporaire(g);
            drawFormeTemporaireRegions(g);
            drawSelection(g);
        }
    }
    
    public void drawPaysSelectionner(Graphics g, Pays p_pays)
    {
        if (p_pays != null)
        {
            ArrayList<Point> pointPolygone = mainWindow.controller.getListePoint(p_pays);
            int index =0; 
            for (Point point:pointPolygone)
            {
                Point pointB = pointPolygone.get((index+1)%pointPolygone.size());
                g.setColor(Color.GREEN);
                g.drawLine((int)point.getX(),(int)point.getY(),(int)pointB.getX(),(int)pointB.getY());
                index++;
            }
        }
    }
    
    public void drawRegionSelectionner(Graphics g, Region p_region)
    {
        if (p_region != null)
        {
            ArrayList<Point> pointPolygone = mainWindow.controller.getListePoint(p_region);
            int index =0;
            for (Point point:pointPolygone)
            {
                Point pointB = pointPolygone.get((index+1)%pointPolygone.size());
                g.setColor(Color.ORANGE);
                g.drawLine((int)point.getX(),(int)point.getY(),(int)pointB.getX(),(int)pointB.getY());
                index++;
            }
        }
    }

    public void drawSelection(Graphics g)
    {
        drawPaysSelectionner(g, mainWindow.selectionPaysA);
        drawPaysSelectionner(g, mainWindow.selectionPaysB);
        drawPaysSelectionner(g, mainWindow.paysDiv);
        drawPaysSelectionner(g, mainWindow.paysModifie);
        drawPaysSelectionner(g, mainWindow.paysVue);
        drawModification(g, mainWindow.paysModifie );
        drawRegionSelectionner(g, mainWindow.selectionRegion); 
    }
    
    public void drawFormeTemporaireRegions(Graphics g){
        for (ArrayList<Point> forme: getMainWindow().getFormeRegion()){
            for (Point point:forme)
            {
                g.setColor(Color.BLACK);
                g.fillOval((int)point.getX()-5, (int)point.getY()-5, 10, 10);
            }
        
            if ((forme.size())>1)
            {
                int indexPoint =0;
                for (Point pointA:forme)
                {
                    Point pointB = forme.get((indexPoint+1)%forme.size());

                    g.setColor(Color.BLACK);
                    g.drawLine((int)pointA.getX(), (int)pointA.getY(), (int)pointB.getX(), (int)pointB.getY());
                    indexPoint++;
                }
            }   
        }
    }
    
    public void drawFormeTemporaire(Graphics g)
    {
        for (Point point:getMainWindow().getFormeTemporaire())
        {
            g.setColor(Color.BLACK);
            g.fillOval((int)point.getX()-5, (int)point.getY()-5, 10, 10);
        }
        
        if ((getMainWindow().getFormeTemporaire().size())>1)
        {
            int indexPoint =0;
            for (Point pointA:getMainWindow().getFormeTemporaire())
            {
                Point pointB = getMainWindow().getFormeTemporaire().get((indexPoint+1)%getMainWindow().getFormeTemporaire().size());
                
                g.setColor(Color.BLACK);
                g.drawLine((int)pointA.getX(), (int)pointA.getY(), (int)pointB.getX(), (int)pointB.getY());
                indexPoint++;
            }
        }
    }
    
      
     public void drawModification(Graphics g, Pays p_pays ){
         
          if (p_pays != null)
        {
            ArrayList<Point> pointPolygone = mainWindow.controller.getListePoint(p_pays);

            for (Point point:pointPolygone)
            {
                g.setColor(Color.BLACK);
                g.fillOval((int)point.getX()-5, (int)point.getY()-5, 10, 10);
            }
        }
    }
     
    public MainWindow getMainWindow()
    {
        return mainWindow;
    }
    
    public void setMainWindow(MainWindow mainWindow)
    {
        this.mainWindow = mainWindow;
    }
    
    public Dimension getInitialDimension(){
        return initialDimension;
    }
}

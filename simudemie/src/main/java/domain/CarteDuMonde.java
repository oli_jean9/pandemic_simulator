package domain;
//Oussama
import java.util.ArrayList;
import java.awt.*;
import java.lang.Math;
import domain.MesureSanitaire;

public class CarteDuMonde implements java.io.Serializable {
    private ArrayList<Pays> listePays;
    private ArrayList<MesureSanitaire> listeMesures;
    private ArrayList<Lien> listeLien;
    
    private ArrayList<Float> pourcentageSaineParJour;
    private ArrayList<Float> pourcentageInfecteParJour;
    private ArrayList<Float> pourcentageMortParJour;

    public CarteDuMonde()
    {   
        listePays = new ArrayList<Pays>();
        listeMesures = new ArrayList<MesureSanitaire>();
        listeLien = new ArrayList<Lien>();
        
        pourcentageSaineParJour = new ArrayList<Float>();
        pourcentageInfecteParJour = new ArrayList<Float>();
        pourcentageMortParJour = new ArrayList<Float>();
        
        //Ajout des mesures sanitaires par defaut
        MesureSanitaire fermerLienAerien = new MesureSanitaire("Fermeture des liens aériens", 0,0, 0, 0, 0, -1, 0, 0);
        listeMesures.add(fermerLienAerien);
        
        MesureSanitaire fermerLienTerrestre = new MesureSanitaire("Fermeture des liens terrestres",0, 0, 0, 0, -1, 0, 0, 0);
        listeMesures.add(fermerLienTerrestre);
        
        MesureSanitaire fermerLienMaritime = new MesureSanitaire("Fermeture des liens maritimes",0, 0, 0, 0, 0, 0, -1, 0);
        listeMesures.add(fermerLienMaritime);
        
    }
    
    public ArrayList<Float> getStatsCarte(){
        ArrayList<Float> output = new ArrayList<Float>();
        float infecte = 0;
        float sains = 0;
        float morts = 0;
        for (Pays pays: listePays){
            infecte += pays.getPopulationInfecte();
            sains += pays.getPopulationSaine();
            morts += pays.getPopulationMorte();
        }
        output.add(infecte);
        output.add(sains);
        output.add(morts);
        return output;
    }
    
    public void creerPays(
            int _pop,
            String _nom,
            Forme _forme, 
            float _txDepMaritime, 
            float _txDepTerrestre, 
            float _txDepAerien, 
            float _txDepRegion,
            float _pctPopSaine,
            float _pctPopInfecte,
            float _pctPopMorte)
    {
        Pays nouveauPays = new Pays(_pop, _nom, _forme, _txDepMaritime,
                _txDepTerrestre, _txDepAerien, _txDepRegion, _pctPopSaine, _pctPopInfecte, _pctPopMorte);
        
        listePays.add(nouveauPays);
    }
    
    public void creerMesureSanitaire(String p_nomMesure,
            float p_impactTauxReproduction,
            float p_impactTauxTransmission,
            float p_impactTauxGuerison, 
            float p_impactTauxMortalite, 
            float p_impactTauxDepTerrestre, 
            float p_impactTauxDepAerien, 
            float p_impactTauxDepMaritime, 
            float p_impactTauxDepInterRegion){
        MesureSanitaire nouvelleMesureSanitaire = new MesureSanitaire(
        p_nomMesure,p_impactTauxReproduction,p_impactTauxTransmission,p_impactTauxGuerison,
        p_impactTauxMortalite, p_impactTauxDepTerrestre, p_impactTauxDepAerien, 
        p_impactTauxDepMaritime,p_impactTauxDepInterRegion);
        listeMesures.add(nouvelleMesureSanitaire);
    }
    public void retirerMesureSanitaire(String p_NomMesure){
        int indexRetirer = -1;
        if (listeMesures.size()!= 0){
           for (int i = 0; i<listeMesures.size();i++){
               if (p_NomMesure == listeMesures.get(i).getNomMesure()){
                   indexRetirer = i;
               }
           }
        }
        if (indexRetirer != -1){
            if (listeMesures.size() == 1){
                listeMesures = new ArrayList<MesureSanitaire>();
            }
            else
            {
                listeMesures.remove(indexRetirer);
            }
            
        }
    
    }
    
    public ArrayList<Pays> getListePays(){
        return listePays;
    }
    
    public ArrayList<MesureSanitaire> getListeMesures()
    {
        return listeMesures;
    }

    public void creerLien(Pays _pays1, Pays _pays2, Lien.typeLien _type, Point _pointPaysA, Point _pointPaysB){
        Lien nouveauLien = new Lien(_pays1, _pays2, _type, _pointPaysA, _pointPaysB);
        listeLien.add(nouveauLien);
    }
    public ArrayList<Lien> getListeLien(){
        return listeLien;
    }
    public ArrayList<Lien> getListLiensPays(Pays _pays){
        ArrayList<Lien> outPutArray = new ArrayList<Lien>();
        for (Lien lien : listeLien) {
            if (lien.getPaysA() == _pays || lien.getPaysB() == _pays) {
                outPutArray.add(lien);
            }
        }
        return outPutArray;
    }
    
    public Pays estDansPays(double x, double y)
    {
        ArrayList<Pays> listePays = getListePays();
        Point point1;
        Point point2;
        double xi;
        double yi;
        double xj;
        double yj;
        boolean intersect;
        
        for (Pays pays:listePays)
        {
            boolean inside = false;
            ArrayList<Point> listePoint = pays.getForme().getListePoints();
            for (int i = 0; i<(listePoint.size()); i++)
            {
                point1 = listePoint.get(i);
                point2 = listePoint.get((i+1)%listePoint.size());
                
                xi = point1.getX();
                yi = point1.getY();
                
                xj = point2.getX();
                yj = point2.getY();
                
                intersect = ((yi > y) != (yj > y)) && x < (((xj-xi)*(y-yi)/(yj-yi))+xi);
                
                if (intersect)
                {
                    if (inside)
                        inside = false;
                    else
                        inside = true;
                } 
            }
            if (inside)
                return pays;
        }
        return null;
    }
    
    public Region estDansRegion(Pays p_pays, int x, int y)
    {
        {
        ArrayList<Region> listeRegion = p_pays.getListeRegion();
        Point point1;
        Point point2;
        double xi;
        double yi;
        double xj;
        double yj;
        boolean intersect;
        
        for (Region region:listeRegion)
        {
            boolean inside = false;
            ArrayList<Point> listePoint = region.getForme().getListePoints();
            for (int i = 0; i<(listePoint.size()); i++)
            {
                point1 = listePoint.get(i);
                point2 = listePoint.get((i+1)%listePoint.size());
                
                xi = point1.getX();
                yi = point1.getY();
                
                xj = point2.getX();
                yj = point2.getY();
                
                intersect = ((yi > y) != (yj > y)) && x < (((xj-xi)*(y-yi)/(yj-yi))+xi);
                
                if (intersect)
                {
                    if (inside)
                        inside = false;
                    else
                        inside = true;
                } 
            }
            if (inside)
                return region;
        }
        return null;
        }
    }
    
    
    public boolean lienExiste(Pays paysA, Pays paysB, Lien.typeLien typeLien)
    {
        for (Lien lien:getListeLien())
            if (((paysA == lien.getPaysA() && paysB == lien.getPaysB() || paysA == lien.getPaysB() && paysB == lien.getPaysA()) &&
                lien.getType() == typeLien))
            {
                return true;
            }
        return false;
    }
    
    public void ajouterTousLesLiensPossible()
    {
        for (Pays paysA:getListePays())
        {
            for (Pays paysB: getListePays())
                if (paysA != paysB)
                {
                    Point pointLesPlusProche[] = pointLesPlusProche(paysA, paysB);
                    if (!lienExiste(paysA, paysB, Lien.typeLien.AERIEN))
                        creerLien(paysA, paysB, Lien.typeLien.AERIEN, pointLesPlusProche[0], pointLesPlusProche[1]);
                    if (!lienExiste(paysA, paysB, Lien.typeLien.TERRESTRE))
                        creerLien(paysA, paysB, Lien.typeLien.TERRESTRE, pointLesPlusProche[0], pointLesPlusProche[1]);
                    if (!lienExiste(paysA, paysB, Lien.typeLien.MARITIME))
                        creerLien(paysA, paysB, Lien.typeLien.MARITIME, pointLesPlusProche[0], pointLesPlusProche[1]);
                }
        }      
    }
    
    public Point[] pointLesPlusProche(Pays p_paysA, Pays p_paysB)
    {
        ArrayList<Point> pointsPaysA = p_paysA.getForme().getListePoints();
        ArrayList<Point> pointsPaysB = p_paysB.getForme().getListePoints();
        Point pointLesPlusProche[] = new Point[2];
        
        double distance = -1;
        
        for (Point pointA:pointsPaysA)
        {
            for (Point pointB:pointsPaysB)
            {
                double distanceTemp = Math.sqrt((Math.pow((pointB.getX()-pointA.getX()), 2))+(Math.pow((pointB.getX()-pointA.getX()), 2)));
                if (distance!=-1 && distanceTemp<distance)
                {
                    distance = distanceTemp;
                    pointLesPlusProche[0] = pointA;
                    pointLesPlusProche[1] = pointB;
                }
            }
        }
        return pointLesPlusProche;          
    }
    public Lien.typeLien strToTypeLien(String str) {
        return Lien.typeLien.valueOf(str);
    }
    
    public void creationAutomatiqueLienTerrestre(Pays p_pays)
    {
        ArrayList<Pays> listePays = getListePays();
        
        ArrayList<Point> pointPaysA = p_pays.getForme().getListePoints();
        
        for (Pays paysB:listePays)
        {
            if (p_pays != paysB)
            {
                ArrayList<Point> pointPaysB = paysB.getForme().getListePoints();
                for (Point pointA : pointPaysA)
                {
                    for (Point pointB : pointPaysB)
                    {
                        if ((pointA.getX()==pointB.getX())&&(pointA.getY()==pointB.getY()))
                        {
                            if (!lienExiste(p_pays, paysB, Lien.typeLien.TERRESTRE))
                            {
                                creerLien(p_pays, paysB, Lien.typeLien.TERRESTRE, pointA, pointB);
                            }
                        }
                    }
                }
            }
        }
    }
    
    public void supprimerLien(Pays p_paysA, Pays p_paysB, Lien.typeLien p_typeLien)
    {
        Lien lienSupprimer = null;
        
        if (lienExiste(p_paysA, p_paysB, p_typeLien))
        {
            for (Lien lien:getListeLien())
            {
                if (((lien.getPaysA() == p_paysA || lien.getPaysB() == p_paysA) &&
                     (lien.getPaysA() == p_paysB || lien.getPaysB() == p_paysB))&&
                        lien.getType()==p_typeLien)
                {
                   lienSupprimer = lien;
                   break;
                }
            }
        }
        getListeLien().remove(lienSupprimer);
    }
    
    public void supprimerApplicationMesure(Pays p_pays)
    {
        for (MesureSanitaire mesure:getListeMesures())
        {
            ArrayList<Integer> indexSupprimer = new ArrayList<Integer>();
            int index = 0;
            for(ListeApplicationMesure application:mesure.getListeApplicationMesure())
            {
                if (application.getPays() == p_pays)
                    indexSupprimer.add(index);
                index++;
            }
            for (Integer indexCourant:indexSupprimer)
            {
                mesure.getListeApplicationMesure().remove(indexCourant);
            }
        }
    }
    
    public void supprimerToutLienPays(Pays p_pays)
    {
        System.out.println(getListeLien().size());
        ArrayList<Lien> listeLienSupprimer = new ArrayList<Lien>();
        for (Lien lien: getListeLien())
        {
            if (lien.getPaysA() == p_pays || lien.getPaysB() == p_pays)
                listeLienSupprimer.add(lien);
        }
        for (int i=0; i<listeLienSupprimer.size(); i++)
        {
            getListeLien().remove(listeLienSupprimer.get(i));
        }
        System.out.println(getListeLien().size());
    }
    
    /*public void supprimerToutLienPays(Pays p_pays)
    {
        System.out.println(getListeLien().size());
        ArrayList<Integer> listeIndexLienSupprimer = new ArrayList<Integer>();
        int index = 0; 
        for (Lien lien: getListeLien())
        {
            if (lien.getPaysA() == p_pays || lien.getPaysB() == p_pays)
                listeIndexLienSupprimer.add(index);
            index++;
        }
        for (Integer indexCourant: listeIndexLienSupprimer)
        {
            System.out.println(getListeLien().get(indexCourant).getPaysA());
            getListeLien().remove(indexCourant);
        }
        System.out.println(getListeLien().size());
    }*/
    
    public void supprimerPays(Pays p_pays)
    {
        supprimerApplicationMesure(p_pays);
        supprimerToutLienPays(p_pays);
        
        Pays paysSupprimer = null;
        
        for (Pays pays: getListePays())
        {
            if (pays == p_pays);
            {
                paysSupprimer = pays;
                break;
            }   
        }
        getListePays().remove(paysSupprimer);
    }
    
    public int getPopulationTotaleInfecte()
    {
        int populationTotalInfecte = 0;
        for (Pays pays:getListePays())
        {
            populationTotalInfecte += pays.getPopulationInfecte();
        }
        return populationTotalInfecte;
    }
    
    public int getPopulationTotale()
    {
        int populationTotale = 0;
        for (Pays pays:getListePays())
        {
            populationTotale += pays.getPopulation();
        }
        return populationTotale;
    }
    
    public int getPopulationTotaleSaine()
    {
        int populationTotaleSaine = 0;
        for (Pays pays:getListePays())
        {
            populationTotaleSaine += pays.getPopulationSaine();
        }
        return populationTotaleSaine;
    }
    
    public int getPopulationTotaleMorte()
    {
        int populationTotaleMorte = 0;
        for (Pays pays:getListePays())
        {
            populationTotaleMorte += pays.getPopulationMorte();
        }
        return populationTotaleMorte;
    }
    
    public Pays getPays(String p_NomPays)
    {
        for (Pays pays: getListePays())
        {
            if (pays.getNomPays().compareTo(p_NomPays)==0)
                return pays;
        }
        return null;
    }
    
    public void enregistreDonneesStatistiques()
    {
        float pourcentageSaine = getPopulationTotaleSaine()/(float)getPopulationTotale();
        pourcentageSaineParJour.add(pourcentageSaine);
        
        float pourcentageInfecte = getPopulationTotaleInfecte()/(float)getPopulationTotale();
        pourcentageInfecteParJour.add(pourcentageInfecte);
        
        float pourcentageMort = getPopulationTotaleMorte()/(float)getPopulationTotale();
        pourcentageMortParJour.add(pourcentageMort);
    }
    
    public ArrayList<Float> getPopulationSaineStats()
    {
        return pourcentageSaineParJour;
    }
    public ArrayList<Float> getPopulationInfecteStats()
    {
        return pourcentageInfecteParJour;
    }
    public ArrayList<Float> getPopulationMortStats()
    {
        return pourcentageMortParJour;
    }
    
    public void clearStats()
    {
        pourcentageInfecteParJour.clear();
        pourcentageMortParJour.clear();
        pourcentageSaineParJour.clear();
        
        for (Pays pays: getListePays())
        {
            pays.clearStats();
            for (Region region:pays.getListeRegion())
                region.clearStats();
        }
    }
}

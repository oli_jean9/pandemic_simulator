package domain;

import java.util.ArrayList;
import java.util.Iterator;

public class MesureSanitaire implements java.io.Serializable, Cloneable{
    
    private String nomMesure;
    private float impactTauxReproduction;
    private float impactTauxTransmission;
    private float impactTauxGuerison;
    private float impactTauxMortalite;
    private float impactTauxDeplacementTerrestre;
    private float impactTauxDeplacementAerien;
    private float impactTauxDeplacementMaritime;
    private float impactTauxDeplacementRegion;
    private ArrayList<ListeApplicationMesure> listeApplicationMesure;
    
    public MesureSanitaire( String p_nomMesure,
        float p_impactTauxReproduction,
        float p_impactTauxTransmission,
        float p_impactTauxGuerison,
        float p_impactTauxMortalite,
        float p_impactTauxDeplacementTerrestre,
        float p_impactTauxDeplacementAerien,
        float p_impactTauxDeplacementMaritime,
        float p_impactTauxDeplacementRegion){
        
        nomMesure = p_nomMesure;
        impactTauxReproduction = p_impactTauxReproduction;
        impactTauxTransmission = p_impactTauxTransmission;
        impactTauxGuerison = p_impactTauxGuerison;
        impactTauxMortalite = p_impactTauxMortalite;
        impactTauxDeplacementTerrestre = p_impactTauxDeplacementTerrestre;
        impactTauxDeplacementAerien = p_impactTauxDeplacementAerien;
        impactTauxDeplacementMaritime = p_impactTauxDeplacementMaritime;
        impactTauxDeplacementRegion = p_impactTauxDeplacementRegion;
        listeApplicationMesure = new ArrayList<ListeApplicationMesure>();
    }
    
    public void modifierMesure(float p_impactTauxReproduction,
        float p_impactTauxTransmission,
        float p_impactTauxGuerison,
        float p_impactTauxMortalite,
        float p_impactTauxDeplacementTerrestre,
        float p_impactTauxDeplacementAerien,
        float p_impactTauxDeplacementMaritime,
        float p_impactTauxDeplacementRegion){
        
        impactTauxReproduction = p_impactTauxReproduction;
        impactTauxTransmission = p_impactTauxTransmission;
        impactTauxGuerison = p_impactTauxGuerison;
        impactTauxMortalite = p_impactTauxMortalite;
        impactTauxDeplacementTerrestre = p_impactTauxDeplacementTerrestre;
        impactTauxDeplacementAerien = p_impactTauxDeplacementAerien;
        impactTauxDeplacementMaritime = p_impactTauxDeplacementMaritime;
        impactTauxDeplacementRegion = p_impactTauxDeplacementRegion;
    }

    public void appliquerMesure(Pays p_pays, Region p_region, float p_tauxAcceptation ){
        ListeApplicationMesure applicationAModifier = verifierApplicationMesureExistante (p_pays, p_region);
        if (applicationAModifier != null){
            applicationAModifier.setTauxAcceptation(p_tauxAcceptation);
        }
        else
        {
            ListeApplicationMesure nouvelleApplicationMesure =  
                    new ListeApplicationMesure(p_pays, p_region, p_tauxAcceptation);
            listeApplicationMesure.add(nouvelleApplicationMesure);
        }
    }
    public void modifierTauxAcceptationMesure(Pays p_pays, Region p_region, float p_tauxAcceptation){
        ListeApplicationMesure applicationAModifier = verifierApplicationMesureExistante (p_pays, p_region);
        if (applicationAModifier != null){
            applicationAModifier.setTauxAcceptation(p_tauxAcceptation);
        }
    }
    
    public void retirerApplicationMesure(Pays p_pays){
        for (Region region:p_pays.getListeRegion() ){
            ListeApplicationMesure applicationARetirer = verifierApplicationMesureExistante (p_pays, region);
            if (applicationARetirer != null){
                listeApplicationMesure.remove(applicationARetirer);
            }
        }
    }
    
    private ListeApplicationMesure verifierApplicationMesureExistante (Pays p_pays, 
            Region p_region){
        for (ListeApplicationMesure application : listeApplicationMesure){
            if (application.getPays() == p_pays
                    && application.getRegion() == p_region){
                return application;
            }
        }
       return null;
    }
    
    public float getTauxAcceptationMesure(Pays p_pays, Region p_region){
        for (ListeApplicationMesure application : listeApplicationMesure){
            if (application.getPays() == p_pays
                    && application.getRegion() == p_region){
                return application.getTauxAcceptation();
            }
        }
        return 0;
    }
    
    public ArrayList<ListeApplicationMesure> getListeApplicationMesure(){
        return listeApplicationMesure;
    }
    public String getNomMesure(){
        return nomMesure;
    }
    public float getImpactTauxReproduction(){
        return impactTauxReproduction;
    }
    public float getImpactTauxTransmission(){
        return impactTauxTransmission;
    }
    public float getImpactTauxGuerison(){
        return impactTauxGuerison;
    }
    public float getImpactTauxMortalite(){
        return impactTauxMortalite;
    }
    public float getImpactTauxDeplacementTerrestre(){
        return impactTauxDeplacementTerrestre;
    }
    public float getImpactTauxDeplacementMaritime(){
        return impactTauxDeplacementMaritime;
    }
    public float getImpactTauxDeplacementAerien(){
        return impactTauxDeplacementAerien;
    }
    public float getImpactTauxDeplacementRegion(){
        return impactTauxDeplacementRegion;
    }
    
    
    public void setNomMesure(String p_nomMesure){
        nomMesure = p_nomMesure;
    }
    public void setImpactTauxReproduction(float p_impactTauxReproduction){
        impactTauxReproduction = p_impactTauxReproduction;
    }
    public void setImpactTauxTransmission(float p_impactTauxTransmission){
        impactTauxTransmission = p_impactTauxTransmission;
    }
    public void setImpactTauxGuerison(float p_impactTauxGuerison){
        impactTauxGuerison = p_impactTauxGuerison;
    }
    public void setImpactTauxMortalite(float p_impactTauxMortalite){
        impactTauxMortalite = p_impactTauxMortalite;
    }
    public void setImpactTauxDeplacementTerrestre(float p_ImpactTauxDeplacementTerrestre){
        impactTauxDeplacementTerrestre = p_ImpactTauxDeplacementTerrestre;
    }
    public void setImpactTauxDeplacementMaritime(float p_ImpactTauxDeplacementMaritime){
        impactTauxDeplacementMaritime = p_ImpactTauxDeplacementMaritime;
    }    
    public void setImpactTauxDeplacementAerien(float p_ImpactTauxDeplacementAerien){
        impactTauxDeplacementAerien = p_ImpactTauxDeplacementAerien;
    }
    public void setImpactTauxDeplacementRegion(float p_ImpactTauxDeplacementRegion){
        impactTauxDeplacementRegion = p_ImpactTauxDeplacementRegion;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package domain;

/**
 *
 * @author Olivier Jean
 */
public class Maladie implements java.io.Serializable
{
    
    private String nom;
    private float tauxReproduction;
    private float tauxTransmission;
    private float tauxGuerison;
    private float tauxMortalite;
    
    public Maladie(String p_nom,
            float p_tauxReproduction,
            float p_tauxTransmission,
            float p_tauxGuerison,
            float p_tauxMortalite)
    {
        nom = p_nom;
        tauxReproduction = p_tauxReproduction;
        tauxTransmission = p_tauxTransmission;
        tauxGuerison = p_tauxGuerison;
        tauxMortalite = p_tauxMortalite;
    }

    public String getNom()
    {
        return nom;
    }
    
    public void setNom(String p_nom)
    {
        nom = p_nom;
    }
    
    public float getTauxReproduction()
    {
        return tauxReproduction;
    }
    
    public float getTauxTransmission()
    {
        return tauxTransmission;
    }
    
    public void setTauxReproduction (float p_tauxReproduction){
        tauxReproduction = p_tauxReproduction;
    }
            
    
    public void setTauxTransmission(float p_tauxTransmission)
    {
        tauxTransmission = p_tauxTransmission;
    }
    
    public float getTauxGuerison()
    {
        return tauxGuerison;
    }
    
    public void setTauxGuerison(float p_tauxGuerison)
    {
        tauxGuerison = p_tauxGuerison;
    }
    
    public float getTauxMortalite()
    {
        return tauxMortalite;
    }
    
    public void setTauxMortalite(float p_tauxMortalite)
    {
        tauxMortalite = p_tauxMortalite;
    }
}
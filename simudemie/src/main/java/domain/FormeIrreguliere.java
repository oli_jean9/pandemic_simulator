package domain;

import java.awt.*;
import java.util.ArrayList;


public class FormeIrreguliere extends Forme implements java.io.Serializable{
    private String nomFormeIrreguliere;

    public FormeIrreguliere()
    {
        Point point1 = new Point(200,200);
        Point point2 = new Point(600,200);
        Point point3 = new Point(400,400);
        Point point4 = new Point(200,400);
        
        listePoints.add(point1);
        listePoints.add(point2);
        listePoints.add(point3);
        listePoints.add(point4);
    }
    
    public FormeIrreguliere(ArrayList<Point> _listePoints)
    {
        for (Point point:_listePoints)
        {
            getListePoints().add(new Point((int)point.getX(),(int)point.getY()));
        }
    }
    
    public void setFormeIrreguliere(ArrayList<Point> _listePoints){
        listePoints = _listePoints;
    }
}

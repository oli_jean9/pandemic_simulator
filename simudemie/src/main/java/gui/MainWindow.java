/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import domain.*;
import java.awt.Graphics2D;

import java.util.concurrent.TimeUnit;
import javax.swing.JPanel;
import javax.swing.table.DefaultTableModel;
import java.util.ArrayList;
import java.awt.Point;
import java.awt.image.BufferedImage;
import java.util.Objects;
import javax.swing.JFileChooser;
import java.io.File;
import java.text.NumberFormat;
import java.util.Locale;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.UIManager;
import javax.swing.SwingWorker;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PiePlot;
import org.jfree.data.general.DefaultPieDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import org.jfree.chart.plot.PlotOrientation;
import javax.swing.JFrame;
import java.awt.Color;
import org.jfree.chart.plot.XYPlot;



/**
 *
 * @author Olivier Jean
 */
public class MainWindow extends javax.swing.JFrame
{

    /**
     * Creates new form MainWindow
     */
    
    public SimudemieController controller;
    public SimudemieMode modeSimudemie;
    public ArrayList<Point> formeTemporaire;
    public ArrayList<Point> toutlespointPays;
    public boolean simulationEnCours = false;
    private Point pointLienA = new Point();
    private Point pointLienB = new Point();
    public Pays selectionPaysA;
    public Pays selectionPaysB;
    public Pays paysVue;
    public Region regionVue;
    public Region selectionRegion;
    private int pos = -1;
    private Double zoomFactor = 1.00;
    public Pays paysModifie;
    DefaultTableModel model;
    ArrayList<ArrayList<String>> numdata = new ArrayList<ArrayList<String>>();
    ArrayList<ArrayList<Point>> formeRegion = new ArrayList<ArrayList<Point>>();
    public Pays paysDiv;
    private int pointDiv = -1;
    private int formDiv = -1;
    private int pointTemp = -1;
    
    public MainWindow() {
        controller = new SimudemieController("controller");
        modeSimudemie = new SimudemieMode();
        formeTemporaire = new ArrayList<Point>();
        initComponents();
        model = (DefaultTableModel) tableRegion.getModel();
    }
    
    public ArrayList<ArrayList<Point>> getFormeRegion(){
        return formeRegion;
    }
    
    public ArrayList<Point> getFormeTemporaire()
    {
        return formeTemporaire;
    }
    
    public double getZoomFactor(){
        return zoomFactor;
    }
    
    public ArrayList<Point> geToutlespointPays(){
        return toutlespointPays;
    }
    
    public void demarrerSimulationEnContinue()
    {
        new SwingWorker() 
        {
            @Override
            protected Object doInBackground() throws Exception
            {
            simulationEnCours = true;
            long msParPas = Long.parseLong(String.valueOf(msParJour.getValue()));

            while (simulationEnCours && (controller.getPopulationTotaleInfecter()>=1))
            {
                waitSimulation(msParPas);
                controller.avancerPasDeTemps();
                numeroJour.setText(String.valueOf(controller.getJourSimulation()));
                drawingPanel.repaint();
                liveStatsMode();
                liveStatsModePays();
                controller.enregistreAction();
            }
            if (controller.getPopulationTotaleInfecter()<=1)
                plusDeMalade.setText("Il n'y a plus d'infectés");
            drawingPanel.repaint();
            return null;
            }
        }.execute();
    }
    
    void demarrerSimulationTempsFixe(final int nombreDeJour)
    {
        new SwingWorker() 
        {
            @Override
            protected Object doInBackground() throws Exception
            {
            for(int i=0; i < nombreDeJour; i++)
                {
                long msParPas = Long.valueOf(msParJour.getValue().toString());
                waitSimulation(msParPas);
                controller.avancerPasDeTemps();
                numeroJour.setText(String.valueOf(controller.getJourSimulation()));
                drawingPanel.repaint();
                liveStatsMode();
                liveStatsModePays();
                controller.enregistreAction();
                }
            if (controller.getPopulationTotaleInfecter()<=1)
                plusDeMalade.setText("Il n'y a plus d'infectés");
            drawingPanel.repaint();
            return null;
            }
        }.execute();
    }
    
    public void waitSimulation(long p_secondes)
    {
        try
        {
        TimeUnit.MILLISECONDS.sleep(p_secondes);
        }
        catch(InterruptedException ex)
        {
        Thread.currentThread().interrupt();
        }
    }
    
    
    private void updateInfoPays(int x, int y)
    {
        Pays pays = controller.getCarteDuMonde().estDansPays(x, y);
        if (pays != null)
        {
            if (NomPaysText.toString()!= pays.getNomPays())
                NomPaysText.setText(pays.getNomPays());
            
                populationText.setText(NumberFormat.getNumberInstance(Locale.CANADA_FRENCH).format(Integer.valueOf(pays.getPopulation())));
                
                float sains = (pays.getPopulationSaine()/(float)pays.getPopulation())*100;
                String sainsFormat = String.format("%.1f", sains);
                SainsText.setText(sainsFormat + " %");
                
                float infecte = (pays.getPopulationInfecte()/(float)pays.getPopulation())*100;
                String infecteFormat = String.format("%.1f", infecte);
                InfecterText.setText(infecteFormat + " %");
                
                float mort = (pays.getPopulationMorte()/(float)pays.getPopulation())*100;
                String mortFormat = String.format("%.1f", mort);
                MortText.setText(mortFormat + " %");
        }
        else
        {
            NomPaysText.setText("");
            populationText.setText("");
            SainsText.setText("");
            InfecterText.setText("");
            MortText.setText("");
        }
    }

    private void annulerDernierPoint()
    {
        if (getFormeTemporaire().size()>0)
        {
            getFormeTemporaire().remove(getFormeTemporaire().size()-1);
            drawingPanel.repaint();
        }
    }
    
    private void addPointOnPays(int x, int y)
    {
        Pays pays = controller.getCarteDuMonde().estDansPays(x, y);
        if (pays == null)
            erreurLabel.setText("La region doit se situer sur un pays existant");
        else{
            erreurLabel.setText("");
            Point pointTemp = new Point(x,y);
            for (ArrayList<Point> arrPoint: formeRegion){
                for (Point pt: arrPoint){
                    if (pt.distance(pointTemp) < 15){
                        pointTemp = new Point(pt);
                    }
                }
            }
            formeTemporaire.add(pointTemp);
            drawingPanel.repaint();
        }
    }
    
    private void addPoint(int x, int y)
    {
        Pays pays = controller.getCarteDuMonde().estDansPays(x, y);
        
        if (pays != null)
            erreurLabel.setText("Le point ne peut se trouver dans un pays existant");
        else
        {
            erreurLabel.setText("");
            
            Point pointTemp = new Point(x,y);
            for (Pays paysB:controller.getListePays())
            {
                for(Point point: controller.getListePoint(paysB))
                {
                    if (Point.distance((int)pointTemp.getX(),(int)pointTemp.getY(), (int)point.getX(), (int)point.getY()) < 10)
                    {    
                        pointTemp.setLocation(point.getX(), point.getY());
                    }
                }
            }
            formeTemporaire.add(pointTemp);
            drawingPanel.repaint();
        }
    }
    
    public void clearSelection()
    {
        selectionPaysA = null;
        selectionPaysB = null;
        paysDiv = null;
        paysModifie = null;
    }
    
    public void clearInfoModifPays()
    {
        nomPaysModifie.setText("");
        populationModifie.setValue(1);
        tauxAerienModifie.setValue(0);
        tauxTerrestreModifie.setValue(0);
        tauxMaritimeModifie.setValue(0);
        tauxRegionnalModifie.setValue(0);
    }
    
    public void changerPanelSousMenu(JPanel panel)
    {
        clearSelection();
        if (panel != VuePaysPanel)
            paysVue = null;
        SousMenuPanel.removeAll();
        SousMenuPanel.add(panel);
        SousMenuPanel.repaint();
        SousMenuPanel.revalidate();
        repaint();
    }
    
    public boolean validerInfosPays()
    {
        return (validerNomPays()&&
                validerPourcentage()&&
                validerForme());
    }

    public boolean validerNomPays()
    {
        String nomPaysEntrer = nomPays.getText();
        if (nomPaysEntrer.isEmpty())
        {
                erreurInfosPays.setText("NOM INVALIDE: VIDE");
                drawingPanel.repaint();
            return false;
        }
        
        for (Pays pays:controller.getListePays())
        {
            if (nomPaysEntrer.compareTo(pays.getNomPays())==0)
            {
                erreurInfosPays.setText("NOM INVALIDE: DÉJÀ PRÉSENT");
                drawingPanel.repaint();
                return false;
            }
        }
        return true;
    }
    
     public boolean validerNomPaysModifie()
    {
        String nomPaysEntrer = nomPays.getText();
        if (nomPaysEntrer.isEmpty())
        {
                erreurPaysModifie.setText("NOM INVALIDE: VIDE");
                drawingPanel.repaint();
            return false;
        }
        
        for (Pays pays:controller.getListePays())
        {
            if (nomPaysEntrer.compareTo(pays.getNomPays())==0)
            {
                erreurInfosPays.setText("NOM INVALIDE: DÉJÀ PRÉSENT");
                drawingPanel.repaint();
                return false;
            }
        }
        return true;
    }

    public boolean validerForme()
    {
        if (getFormeTemporaire().size()<3)
        {
            erreurInfosPays.setText("FORME INVALIDE");
            drawingPanel.repaint();
            return false;
        }
        return true;
    }
    
    public boolean validerPourcentage()
    {
        float totalPct = Float.parseFloat(pctSain.getValue().toString())+
                Float.parseFloat(pctInfecter.getValue().toString())+
                Float.parseFloat(pctMort.getValue().toString());
        if (totalPct != 100.0f)
        {
            float pctDiff = 100.0f-totalPct;
            erreurPctPop.setText("ERREUR: Le total doit être 100% Diff= "+ String.valueOf(pctDiff)+"%");
            drawingPanel.repaint();
            return false;
        }
        return true;
    }
    
    public boolean validerInfoNouvelleMesure()
    {
        boolean infoValide = true;
        if (NomNouvelleMesure.getText().equals(""))
            {
                infoValide = false;
                ErreurInfoNouvelleMesure.setText("NOM MESURE VIDE");
                drawingPanel.repaint();
                
            }
        for (MesureSanitaire mesure:controller.getlisteMesureSanitaire())
        {
            if (NomNouvelleMesure.getText() == mesure.getNomMesure())
            {
                infoValide = false;
                ErreurInfoNouvelleMesure.setText("NOM MESURE DEJA PRESENT");
                drawingPanel.repaint();
            }
        }
        return infoValide;
    }
    
    public boolean creerMesure(){
        if (validerInfoNouvelleMesure()){
            controller.creerMesureSanitaire(NomNouvelleMesure.getText(),
                (float)NMImpactTauxReproduction.getValue(),
                (float)NMImpactTauxContagion.getValue(),
                (float)NMImpactTauxMortalite.getValue(),
                (float)NMImpactTauxGuerison.getValue(), 
                (float)NMImpactTauxTerrestre.getValue(), 
                (float)NMImpactTauxAerien.getValue(),
                (float)NMImpactTauxMaritime.getValue(),
                (float)NMImpactTauxRegion.getValue());
            return true;
        }
        return false;
    }
    
    public void clearCreerMesure(){
        NomNouvelleMesure.setText("");
        NMImpactTauxReproduction.setValue(0f);
        NMImpactTauxContagion.setValue(0f);
        NMImpactTauxMortalite.setValue(0f);   
        NMImpactTauxGuerison.setValue(0f);
        NMImpactTauxTerrestre.setValue(0f);                
        NMImpactTauxAerien.setValue(0f);            
        NMImpactTauxMaritime.setValue(0f);               
        NMImpactTauxRegion.setValue(0f);
        ErreurInfoNouvelleMesure.setText("");
        InfoAjouterMesure.setText("");
        repaint();
    }
    
    public void clearInfoAjouterPays()
    {
        nomPays.setText("");
        population.setValue(1);
        pctSain.setValue(0f);
        pctInfecter.setValue(0f);
        pctMort.setValue(0f);
        tauxTerrestre.setValue(0f);
        tauxMaritime.setValue(0f);
        tauxAerien.setValue(0f);
        tauxRegionnal.setValue(0f);
        erreurPctPop.setText("");
        erreurInfosPays.setText("");
        formeIrr.setSelected(true);
    }
    
    public static void wait(int ms)
    {
        try
        {
            Thread.sleep(ms);
        }
        catch(InterruptedException ex)
        {
            Thread.currentThread().interrupt();
        }
    }
    
    public void enregistrer(){
        UIManager.put("FileChooser.saveButtonText","Save");
        JFrame parentFrame = new JFrame();
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setDialogTitle("Specify a file to save");   

        int userSelection = fileChooser.showSaveDialog(parentFrame);

        if (userSelection == JFileChooser.APPROVE_OPTION) {
            File fileToSave = fileChooser.getSelectedFile();
            controller.enregistrerProjet(fileToSave.getAbsolutePath() + ".ser");
        }
    }
    
    public void charger(){
        UIManager.put("FileChooser.saveButtonText","Load");
        JFrame parentFrame = new JFrame();
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setDialogTitle("Specify a file to load"); 
        fileChooser.setApproveButtonText("Load");
        int userSelection = fileChooser.showSaveDialog(parentFrame);

        if (userSelection == JFileChooser.APPROVE_OPTION) {
            File fileToSave = fileChooser.getSelectedFile();
            controller.chargerProjet(fileToSave.getAbsolutePath());
        repaint();
        }
    }
    
    public void updateVuePays(java.awt.event.MouseEvent evt)
    {
        Pays paysClick = controller.getCarteDuMonde().estDansPays(evt.getX(), evt.getY());
        if (paysClick != null)
        {
            if (modeSimudemie.getMode()!= SimudemieMode.simudemieMode.STATISTIQUE)
                modeSimudemie.setMode(SimudemieMode.simudemieMode.VUEPAYS);
            paysVue = paysClick;
            changerPanelSousMenu(VuePaysPanel);
            vuePaysLabel.setText(paysVue.getNomPays());
            popVuePays.setText(NumberFormat.getNumberInstance(Locale.CANADA_FRENCH).format(Integer.valueOf(paysVue.getPopulation())));
            
            float sains = (paysVue.getPopulationSaine()/(float)paysVue.getPopulation())*100;
            String sainsFormat = String.format("%.1f", sains);
            sainsVue.setText(sainsFormat + " %");

            float infecte = (paysVue.getPopulationInfecte()/(float)paysVue.getPopulation())*100;
            String infecteFormat = String.format("%.1f", infecte);
            infecterVue.setText(infecteFormat + " %");

            float mort = (paysVue.getPopulationMorte()/(float)paysVue.getPopulation())*100;
            String mortFormat = String.format("%.1f", mort);
            mortsVue.setText(mortFormat + " %");
            
            float txTerrestre = paysVue.getTxDepTerrestre();
            String txTerrestreFormat = String.format("%.3f", txTerrestre);
            txTerrestreVue.setText(txTerrestreFormat);
            
            float txAerien = paysVue.getTxDepAerien();
            String txAerienFormat = String.format("%.3f", txAerien);
            txAerienVue.setText(txAerienFormat);
            
            float txMaritime = paysVue.getTxDepMaritime();
            String txMaritimeFormat = String.format("%.3f", txMaritime);
            txMaritimeVue.setText(txMaritimeFormat);
            
            float txRegionnal = paysVue.getTxDepRegion();
            String txRegionnalFormat = String.format("%.3f", txRegionnal);
            txRegionnalVue.setText(txRegionnalFormat);
           
        }
        else
        {
            modeSimudemie.setMode(SimudemieMode.simudemieMode.CARTEDUMONDE);
            paysVue = null;
            selectionRegion = null;
            changerPanelSousMenu(BienvenuePanel);
        }
        
        repaint();
    }
    
    void updateInfoVueRegion(java.awt.event.MouseEvent evt)
    {
        if (paysVue != null)
        {
            Region region = controller.estDansRegion(paysVue, evt.getX(), evt.getY());
            if (region != null)
            {
                regionVue = region;
                nomRegionVue.setText(regionVue.getNomRegion());

                int populationRegion = (int)(regionVue.getPourcentagePopulation()*paysVue.getPopulation());
                String populationRegionFormat = NumberFormat.getNumberInstance(Locale.CANADA_FRENCH).format(Integer.valueOf(populationRegion));
                populationRegionVue.setText(populationRegionFormat);

                float pctPop = (region.getPourcentagePopulation()*100);
                String pctPopFormat = String.format("%.1f", pctPop);
                pctPopPaysVue.setText(pctPopFormat + " %");

                float pctSainsRegion = (region.getPourcentageSaine()*100);
                String pctSainsRegionFormat = String.format("%.1f", pctSainsRegion);
                pctSainsRegionVue.setText(pctSainsRegionFormat + " %");

                float pctInfecterRegion = (region.getPourcentageInfecte()*100);
                String pctInfecterRegionFormat = String.format("%.1f", pctInfecterRegion);
                pctInfecteRegionVue.setText(pctInfecterRegionFormat + " %");

                float pctMortRegion = (region.getPourcentageMort()*100);
                String pctMortRegionFormat = String.format("%.1f", pctMortRegion);
                pctmortRegionVue.setText(pctMortRegionFormat + " %");

                int populationSaineRegion = (int)(regionVue.getPourcentageSaine()*regionVue.getPourcentagePopulation()*paysVue.getPopulation());
                String populationSaineRegionFormat = NumberFormat.getNumberInstance(Locale.CANADA_FRENCH).format(Integer.valueOf(populationSaineRegion));
                totalSainsRegionVue.setText(populationSaineRegionFormat);

                int populationInfecterRegion = (int)(regionVue.getPourcentageInfecte()*regionVue.getPourcentagePopulation()*paysVue.getPopulation());
                String populationInfecterRegionFormat = NumberFormat.getNumberInstance(Locale.CANADA_FRENCH).format(Integer.valueOf(populationInfecterRegion));
                totalInfecterRegionVue.setText(populationInfecterRegionFormat);

                int populationMortRegion = (int)(regionVue.getPourcentageMort()*regionVue.getPourcentagePopulation()*paysVue.getPopulation());
                String populationMortRegionRegionFormat = NumberFormat.getNumberInstance(Locale.CANADA_FRENCH).format(Integer.valueOf(populationMortRegion));
                totalMortRegionVue.setText(populationMortRegionRegionFormat);
            }
        
            else
            {
                regionVue = null;
                nomRegionVue.setText("");
                pctPopPaysVue.setText("");
                populationRegionVue.setText("");
                pctSainsRegionVue.setText("");
                pctInfecteRegionVue.setText("");
                pctmortRegionVue.setText("");
                totalSainsRegionVue.setText("");
                totalInfecterRegionVue.setText("");
                totalMortRegionVue.setText("");
            }
            repaint();
        }
    }
    
    public void updateStatistiques()
    {
        modeSimudemie.setMode(SimudemieMode.simudemieMode.STATISTIQUE);
        
        if (paysVue == null)
        {
            selectionRegion = null;
            initGraph(controller.getPopulationSaineStats(), controller.getPopulationIntecteStats(), controller.getPopulationMorteStats(), "Monde entier");
        }
        else if (paysVue != null && selectionRegion == null){
            initGraph(paysVue.getPopulationSaineStats(), paysVue.getPopulationInfecteStats(), paysVue.getPopulationMortStats(), paysVue.getNomPays());
        }
        else if (paysVue != null && selectionRegion != null){
            initGraph(selectionRegion.getPopulationSaineStats(), selectionRegion.getPopulationInfecteStats(), selectionRegion.getPopulationMortStats(), selectionRegion.getNomRegion());
        }
        repaint();
        revalidate();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        MenuBouton = new javax.swing.JPanel();
        conceptionLabel = new javax.swing.JLabel();
        nbrJours = new javax.swing.JSpinner();
        nbrJourrLabel = new javax.swing.JLabel();
        arreterSimulation = new javax.swing.JButton();
        statistiquesBouton = new javax.swing.JButton();
        simulationLabel1 = new javax.swing.JLabel();
        ajouterPays = new javax.swing.JButton();
        CreerMaladieBouton = new javax.swing.JButton();
        btnMesureSanitaire = new javax.swing.JButton();
        ajouterMesureSanitaire = new javax.swing.JButton();
        nouveauLien = new javax.swing.JButton();
        nouvelleDivision = new javax.swing.JButton();
        stop = new javax.swing.JButton();
        UndoSimulation = new javax.swing.JButton();
        jLabel26 = new javax.swing.JLabel();
        jLabel28 = new javax.swing.JLabel();
        jLabel29 = new javax.swing.JLabel();
        demarrerSimulation = new javax.swing.JButton();
        ModifierPays = new javax.swing.JButton();
        msJourLabel = new javax.swing.JLabel();
        msParJour = new javax.swing.JSpinner();
        enContinueRadio = new javax.swing.JRadioButton();
        btnAppliquerMesureSanitaire = new javax.swing.JButton();
        jLabel36 = new javax.swing.JLabel();
        numeroJour = new javax.swing.JLabel();
        plusDeMalade = new javax.swing.JLabel();
        btnModifierMaladie = new javax.swing.JButton();
        btnAppliquerMaladie = new javax.swing.JButton();
        SousMenuPanel = new javax.swing.JLayeredPane();
        BienvenuePanel = new javax.swing.JPanel();
        bienvenue = new javax.swing.JLabel();
        jLabel27 = new javax.swing.JLabel();
        AjouterPays = new javax.swing.JPanel();
        NouveauPaysLabel = new javax.swing.JLabel();
        AnnulerNouveauPays = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        annulerDernierPoint = new javax.swing.JButton();
        jLabel3 = new javax.swing.JLabel();
        nomPays = new javax.swing.JTextField();
        populationLabel = new javax.swing.JLabel();
        population = new javax.swing.JSpinner();
        jLabel4 = new javax.swing.JLabel();
        terrestreLabel = new javax.swing.JLabel();
        tauxTerrestre = new javax.swing.JSpinner();
        maritimeLabel = new javax.swing.JLabel();
        tauxMaritime = new javax.swing.JSpinner();
        aerienLabel = new javax.swing.JLabel();
        tauxAerien = new javax.swing.JSpinner();
        regionnalLabel = new javax.swing.JLabel();
        tauxRegionnal = new javax.swing.JSpinner();
        creerPaysButton = new javax.swing.JButton();
        erreurInfosPays = new javax.swing.JLabel();
        jLabel25 = new javax.swing.JLabel();
        pctSain = new javax.swing.JSpinner();
        pctSaineLabel = new javax.swing.JLabel();
        pctInfecter = new javax.swing.JSpinner();
        pctInfecterLabel = new javax.swing.JLabel();
        pctMort = new javax.swing.JSpinner();
        pctMorteLabel = new javax.swing.JLabel();
        erreurPctPop = new javax.swing.JLabel();
        formeIrr = new javax.swing.JCheckBox();
        comboForme = new javax.swing.JComboBox<>();
        jLabel30 = new javax.swing.JLabel();
        CreerMaladie = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        AnnulerCreerMaladie = new javax.swing.JButton();
        LabelNomMaladie = new javax.swing.JLabel();
        LabelTauxContagion = new javax.swing.JLabel();
        LabelTauxGuerison = new javax.swing.JLabel();
        LabelTauxMortalite = new javax.swing.JLabel();
        txtNomMaladie = new javax.swing.JTextField();
        NumericTauxContagion = new javax.swing.JSpinner();
        NumericTauxGuerison = new javax.swing.JSpinner();
        NumericTauxMortalite = new javax.swing.JSpinner();
        btnOKMaladie = new javax.swing.JButton();
        InfoMaladie = new javax.swing.JLabel();
        jLabel61 = new javax.swing.JLabel();
        NumericTauxReproduction = new javax.swing.JSpinner();
        AppliquerMaladie = new javax.swing.JPanel();
        jLabel55 = new javax.swing.JLabel();
        jLabel56 = new javax.swing.JLabel();
        jLabel57 = new javax.swing.JLabel();
        jLabel58 = new javax.swing.JLabel();
        jLabel59 = new javax.swing.JLabel();
        tauxTransmissionApplicationMaladie = new javax.swing.JSpinner();
        tauxGuerisonApplicationMaladie = new javax.swing.JSpinner();
        tauxMortaliteApplicationMaladie = new javax.swing.JSpinner();
        btnEnregistrerApplicationMaladie = new javax.swing.JButton();
        btnQuitterApplicationMaladie = new javax.swing.JToggleButton();
        maladieAppliquerMaladie = new javax.swing.JComboBox<>();
        nomMaladieAppliquerProjet = new javax.swing.JTextField();
        jLabel60 = new javax.swing.JLabel();
        jLabel63 = new javax.swing.JLabel();
        tauxReproductionApplicationMaladie = new javax.swing.JSpinner();
        ModifierMaladie = new javax.swing.JPanel();
        jLabel50 = new javax.swing.JLabel();
        jLabel51 = new javax.swing.JLabel();
        listeMaladieModifMaladie = new javax.swing.JComboBox<>();
        jLabel52 = new javax.swing.JLabel();
        jLabel53 = new javax.swing.JLabel();
        jLabel54 = new javax.swing.JLabel();
        tauxTransmissionModifMaladie = new javax.swing.JSpinner();
        tauxGuerisonModifMaladie = new javax.swing.JSpinner();
        tauxMortaliteModifMaladie = new javax.swing.JSpinner();
        btnEnregistrerModifMaladie = new javax.swing.JButton();
        btnQuitterEnregistreMaladie = new javax.swing.JToggleButton();
        btnSupprimerMaladie = new javax.swing.JButton();
        tauxReproductionModifMaladie = new javax.swing.JSpinner();
        jLabel62 = new javax.swing.JLabel();
        DiviserPays = new javax.swing.JPanel();
        titreDivisionPays = new javax.swing.JLabel();
        selectedPays = new javax.swing.JTextField();
        jLabel24 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tableRegion = new javax.swing.JTable();
        diviser = new javax.swing.JButton();
        annulerDivision = new javax.swing.JButton();
        enleverSelectionRegion = new javax.swing.JButton();
        ajoutLigne = new javax.swing.JButton();
        erreurDivision = new javax.swing.JLabel();
        effLigne = new javax.swing.JButton();
        nouvForme = new javax.swing.JButton();
        AjouterMesureSanitaire = new javax.swing.JPanel();
        jLabel15 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        NomNouvelleMesure = new javax.swing.JTextField();
        jLabel17 = new javax.swing.JLabel();
        jLabel18 = new javax.swing.JLabel();
        jLabel19 = new javax.swing.JLabel();
        jLabel20 = new javax.swing.JLabel();
        jLabel21 = new javax.swing.JLabel();
        jLabel22 = new javax.swing.JLabel();
        jLabel23 = new javax.swing.JLabel();
        NMImpactTauxContagion = new javax.swing.JSpinner();
        NMImpactTauxGuerison = new javax.swing.JSpinner();
        NMImpactTauxMortalite = new javax.swing.JSpinner();
        NMImpactTauxTerrestre = new javax.swing.JSpinner();
        NMImpactTauxMaritime = new javax.swing.JSpinner();
        NMImpactTauxAerien = new javax.swing.JSpinner();
        NMImpactTauxRegion = new javax.swing.JSpinner();
        CreerMesure = new javax.swing.JButton();
        AnnulerCreationMesure = new javax.swing.JButton();
        ErreurInfoNouvelleMesure = new javax.swing.JLabel();
        InfoAjouterMesure = new javax.swing.JLabel();
        jLabel64 = new javax.swing.JLabel();
        NMImpactTauxReproduction = new javax.swing.JSpinner();
        AppliquerMesureSanitaire = new javax.swing.JPanel();
        jLabel31 = new javax.swing.JLabel();
        AppliquerMesureListeMesure = new javax.swing.JComboBox<>();
        jLabel32 = new javax.swing.JLabel();
        jLabel33 = new javax.swing.JLabel();
        AppliquerMesureListePays = new javax.swing.JComboBox<>();
        jScrollPane1 = new javax.swing.JScrollPane();
        tableApplicationMesure = new javax.swing.JTable();
        btnEnregistrer = new javax.swing.JButton();
        btnQuitter = new javax.swing.JButton();
        AppliquerMesureErreurInfo = new javax.swing.JLabel();
        btnSupprimerApplicationMesure = new javax.swing.JButton();
        EditionLien = new javax.swing.JPanel();
        creerLien = new javax.swing.JButton();
        terminerEditionLien = new javax.swing.JButton();
        nomPaysA = new javax.swing.JTextField();
        nomPaysB = new javax.swing.JTextField();
        typeLien = new javax.swing.JComboBox<>();
        enleverSelection = new javax.swing.JButton();
        erreurCreationLien = new javax.swing.JLabel();
        lblTitreLien = new javax.swing.JLabel();
        lblpaysA = new javax.swing.JLabel();
        lblpaysB = new javax.swing.JLabel();
        lblType = new javax.swing.JLabel();
        terrestre = new javax.swing.JLabel();
        SupprimerLienLabel = new javax.swing.JLabel();
        Aerien = new javax.swing.JLabel();
        Maritime = new javax.swing.JLabel();
        supprimerTerrestre = new javax.swing.JButton();
        supprimerAerien = new javax.swing.JButton();
        supprimerMaritime = new javax.swing.JButton();
        ModifierMesureSanitaire = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        ComboListeMesureSanitaire = new javax.swing.JComboBox<>();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        ImpactTauxReproduction = new javax.swing.JSpinner();
        ImpactTauxGuerison = new javax.swing.JSpinner();
        ImpactTauxMortalite = new javax.swing.JSpinner();
        ImpactTauxTerrestre = new javax.swing.JSpinner();
        ImpactTauxMaritime = new javax.swing.JSpinner();
        ImpactTauxAerien = new javax.swing.JSpinner();
        ImpactTauxRegion = new javax.swing.JSpinner();
        btnEnregistrerMesure = new javax.swing.JButton();
        btnAnnulerEnregistrementMesure = new javax.swing.JButton();
        InfoModifierMesure = new javax.swing.JLabel();
        btnRetirerMesure = new javax.swing.JButton();
        jLabel65 = new javax.swing.JLabel();
        ImpactTauxTransmission = new javax.swing.JSpinner();
        ModificationPaysPanel = new javax.swing.JPanel();
        NouveauPaysLabel1 = new javax.swing.JLabel();
        ModificationTerminée = new javax.swing.JButton();
        nomPaysModifie = new javax.swing.JTextField();
        jLabel34 = new javax.swing.JLabel();
        populationModifie = new javax.swing.JSpinner();
        jLabel35 = new javax.swing.JLabel();
        erreurPaysModifie = new javax.swing.JLabel();
        nouvelleDivision1 = new javax.swing.JButton();
        pctSaineLabel1 = new javax.swing.JLabel();
        tauxTerrestreModifie = new javax.swing.JSpinner();
        pctInfecterLabel1 = new javax.swing.JLabel();
        tauxMaritimeModifie = new javax.swing.JSpinner();
        tauxAerienModifie = new javax.swing.JSpinner();
        pctMorteLabel1 = new javax.swing.JLabel();
        jLabel38 = new javax.swing.JLabel();
        tauxRegionnalModifie = new javax.swing.JSpinner();
        jLabel37 = new javax.swing.JLabel();
        supprimerPays = new javax.swing.JButton();
        VuePaysPanel = new javax.swing.JPanel();
        vuePaysLabel = new javax.swing.JLabel();
        popVueLabel = new javax.swing.JLabel();
        popVuePays = new javax.swing.JLabel();
        sainsVueLabel = new javax.swing.JLabel();
        infecterVueLabel = new javax.swing.JLabel();
        mortVueLabel = new javax.swing.JLabel();
        popVue = new javax.swing.JLabel();
        sainsVue = new javax.swing.JLabel();
        infecterVue = new javax.swing.JLabel();
        mortsVue = new javax.swing.JLabel();
        jLabel39 = new javax.swing.JLabel();
        jLabel40 = new javax.swing.JLabel();
        jLabel41 = new javax.swing.JLabel();
        jLabel42 = new javax.swing.JLabel();
        jLabel43 = new javax.swing.JLabel();
        txTerrestreVue = new javax.swing.JLabel();
        txAerienVue = new javax.swing.JLabel();
        txMaritimeVue = new javax.swing.JLabel();
        txRegionnalVue = new javax.swing.JLabel();
        jLabel44 = new javax.swing.JLabel();
        jLabel45 = new javax.swing.JLabel();
        nomRegionVue = new javax.swing.JLabel();
        jLabel46 = new javax.swing.JLabel();
        pctPopPaysVuelabel = new javax.swing.JLabel();
        populationRegionVue = new javax.swing.JLabel();
        pctPopPaysVue = new javax.swing.JLabel();
        jLabel47 = new javax.swing.JLabel();
        jLabel48 = new javax.swing.JLabel();
        jLabel49 = new javax.swing.JLabel();
        pctSainsRegionVue = new javax.swing.JLabel();
        totalSainsRegionVue = new javax.swing.JLabel();
        pctInfecteRegionVue = new javax.swing.JLabel();
        totalInfecterRegionVue = new javax.swing.JLabel();
        pctmortRegionVue = new javax.swing.JLabel();
        totalMortRegionVue = new javax.swing.JLabel();
        statPanel = new javax.swing.JPanel();
        graphMonde = new javax.swing.JPanel();
        graphStatistique = new javax.swing.JPanel();
        drawingPanel = new gui.DrawingPanel(this);
        erreurLabel = new javax.swing.JLabel();
        InfosPaysPanel = new javax.swing.JPanel();
        MortText = new javax.swing.JFormattedTextField();
        Mort = new javax.swing.JLabel();
        populationInfoLabel = new javax.swing.JLabel();
        populationText = new javax.swing.JFormattedTextField();
        NomPays = new javax.swing.JLabel();
        InfecterText = new javax.swing.JFormattedTextField();
        SainsText = new javax.swing.JFormattedTextField();
        NomPaysText = new javax.swing.JFormattedTextField();
        Infecter = new javax.swing.JLabel();
        Sains = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        StatsPanel = new javax.swing.JPanel();
        ToolBarMenu = new javax.swing.JToolBar();
        btnFile = new javax.swing.JButton();
        Save = new javax.swing.JButton();
        Load = new javax.swing.JButton();
        Undo = new javax.swing.JButton();
        Redo = new javax.swing.JButton();
        exportJpeg = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setBackground(new java.awt.Color(51, 51, 51));

        MenuBouton.setBackground(new java.awt.Color(51, 51, 51));
        MenuBouton.setBorder(javax.swing.BorderFactory.createEtchedBorder(new java.awt.Color(51, 51, 51), null));

        conceptionLabel.setBackground(new java.awt.Color(255, 255, 255));
        conceptionLabel.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        conceptionLabel.setForeground(new java.awt.Color(255, 255, 255));
        conceptionLabel.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        conceptionLabel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/3d-display.png"))); // NOI18N
        conceptionLabel.setText("CONCEPTION");

        nbrJourrLabel.setFont(new java.awt.Font("Dialog", 0, 10)); // NOI18N
        nbrJourrLabel.setForeground(new java.awt.Color(255, 255, 255));
        nbrJourrLabel.setText("Nbr de jours:");

        arreterSimulation.setBackground(new java.awt.Color(51, 51, 51));
        arreterSimulation.setForeground(new java.awt.Color(255, 255, 255));
        arreterSimulation.setIcon(new javax.swing.ImageIcon(getClass().getResource("/pause.png"))); // NOI18N
        arreterSimulation.setBorder(null);
        arreterSimulation.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                arreterSimulationActionPerformed(evt);
            }
        });

        statistiquesBouton.setBackground(new java.awt.Color(102, 102, 102));
        statistiquesBouton.setForeground(new java.awt.Color(255, 255, 255));
        statistiquesBouton.setText("STATISTIQUES");
        statistiquesBouton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                statistiquesBoutonActionPerformed(evt);
            }
        });

        simulationLabel1.setBackground(new java.awt.Color(255, 255, 255));
        simulationLabel1.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        simulationLabel1.setForeground(new java.awt.Color(255, 255, 255));
        simulationLabel1.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        simulationLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/gloves.png"))); // NOI18N
        simulationLabel1.setText("SIMULATION");

        ajouterPays.setBackground(new java.awt.Color(102, 102, 102));
        ajouterPays.setForeground(new java.awt.Color(255, 255, 255));
        ajouterPays.setText("NOUVEAU PAYS");
        ajouterPays.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ajouterPaysActionPerformed(evt);
            }
        });

        CreerMaladieBouton.setBackground(new java.awt.Color(102, 102, 102));
        CreerMaladieBouton.setForeground(new java.awt.Color(255, 255, 255));
        CreerMaladieBouton.setText("CREER MALADIE");
        CreerMaladieBouton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CreerMaladieBoutonActionPerformed(evt);
            }
        });

        btnMesureSanitaire.setBackground(new java.awt.Color(102, 102, 102));
        btnMesureSanitaire.setForeground(new java.awt.Color(255, 255, 255));
        btnMesureSanitaire.setText("MODIFIER MESURE SANITAIRE");
        btnMesureSanitaire.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnMesureSanitaireActionPerformed(evt);
            }
        });

        ajouterMesureSanitaire.setBackground(new java.awt.Color(102, 102, 102));
        ajouterMesureSanitaire.setForeground(new java.awt.Color(255, 255, 255));
        ajouterMesureSanitaire.setText("AJOUTER MESURE SANITAIRE");
        ajouterMesureSanitaire.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ajouterMesureSanitaireActionPerformed(evt);
            }
        });

        nouveauLien.setBackground(new java.awt.Color(102, 102, 102));
        nouveauLien.setForeground(new java.awt.Color(255, 255, 255));
        nouveauLien.setText("EDITION LIEN");
        nouveauLien.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                nouveauLienActionPerformed(evt);
            }
        });

        nouvelleDivision.setBackground(new java.awt.Color(102, 102, 102));
        nouvelleDivision.setForeground(new java.awt.Color(255, 255, 255));
        nouvelleDivision.setText("DIVISER PAYS");
        nouvelleDivision.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                nouvelleDivisionActionPerformed(evt);
            }
        });

        stop.setBackground(new java.awt.Color(51, 51, 51));
        stop.setForeground(new java.awt.Color(255, 255, 255));
        stop.setIcon(new javax.swing.ImageIcon(getClass().getResource("/stop.png"))); // NOI18N
        stop.setBorder(null);
        stop.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                stopActionPerformed(evt);
            }
        });

        UndoSimulation.setBackground(new java.awt.Color(51, 51, 51));
        UndoSimulation.setForeground(new java.awt.Color(255, 255, 255));
        UndoSimulation.setIcon(new javax.swing.ImageIcon(getClass().getResource("/back.png"))); // NOI18N
        UndoSimulation.setBorder(null);
        UndoSimulation.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                UndoSimulationActionPerformed(evt);
            }
        });

        jLabel26.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel26.setIcon(new javax.swing.ImageIcon(getClass().getResource("/earth.png"))); // NOI18N

        jLabel28.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel28.setIcon(new javax.swing.ImageIcon(getClass().getResource("/bacteria.png"))); // NOI18N

        jLabel29.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel29.setIcon(new javax.swing.ImageIcon(getClass().getResource("/disinfectant.png"))); // NOI18N

        demarrerSimulation.setBackground(new java.awt.Color(51, 51, 51));
        demarrerSimulation.setForeground(new java.awt.Color(255, 255, 255));
        demarrerSimulation.setIcon(new javax.swing.ImageIcon(getClass().getResource("/play.png"))); // NOI18N
        demarrerSimulation.setBorder(null);
        demarrerSimulation.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                demarrerSimulationActionPerformed(evt);
            }
        });

        ModifierPays.setBackground(new java.awt.Color(102, 102, 102));
        ModifierPays.setForeground(new java.awt.Color(255, 255, 255));
        ModifierPays.setText("MODIFIER PAYS");
        ModifierPays.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ModifierPaysActionPerformed(evt);
            }
        });

        msJourLabel.setFont(new java.awt.Font("Dialog", 0, 10)); // NOI18N
        msJourLabel.setForeground(new java.awt.Color(255, 255, 255));
        msJourLabel.setText("ms/jour");

        msParJour.setModel(new javax.swing.SpinnerNumberModel(100L, 0L, null, 1L));

        enContinueRadio.setBackground(new java.awt.Color(51, 51, 51));
        enContinueRadio.setForeground(new java.awt.Color(255, 255, 255));
        enContinueRadio.setText("En continue");
        enContinueRadio.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                enContinueRadioActionPerformed(evt);
            }
        });

        btnAppliquerMesureSanitaire.setBackground(new java.awt.Color(102, 102, 102));
        btnAppliquerMesureSanitaire.setForeground(new java.awt.Color(255, 255, 255));
        btnAppliquerMesureSanitaire.setText("APPLIQUER MESURE SANITAIRE");
        btnAppliquerMesureSanitaire.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAppliquerMesureSanitaireActionPerformed(evt);
            }
        });

        jLabel36.setFont(new java.awt.Font("Dialog", 0, 10)); // NOI18N
        jLabel36.setForeground(new java.awt.Color(255, 255, 255));
        jLabel36.setText("#jour");

        numeroJour.setForeground(new java.awt.Color(255, 255, 255));
        numeroJour.setText("0");

        plusDeMalade.setForeground(new java.awt.Color(255, 51, 51));
        plusDeMalade.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);

        btnModifierMaladie.setBackground(new java.awt.Color(102, 102, 102));
        btnModifierMaladie.setForeground(new java.awt.Color(255, 255, 255));
        btnModifierMaladie.setText("MODIFIER MALADIE");
        btnModifierMaladie.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnModifierMaladieActionPerformed(evt);
            }
        });

        btnAppliquerMaladie.setBackground(new java.awt.Color(102, 102, 102));
        btnAppliquerMaladie.setForeground(new java.awt.Color(255, 255, 255));
        btnAppliquerMaladie.setText("APPLIQUER MALADIE");
        btnAppliquerMaladie.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAppliquerMaladieActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout MenuBoutonLayout = new javax.swing.GroupLayout(MenuBouton);
        MenuBouton.setLayout(MenuBoutonLayout);
        MenuBoutonLayout.setHorizontalGroup(
            MenuBoutonLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(MenuBoutonLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(MenuBoutonLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(simulationLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(MenuBoutonLayout.createSequentialGroup()
                        .addGroup(MenuBoutonLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(btnMesureSanitaire, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(ajouterMesureSanitaire, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel29, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(CreerMaladieBouton, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel28, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(ModifierPays, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(nouveauLien, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(nouvelleDivision, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, MenuBoutonLayout.createSequentialGroup()
                                .addComponent(demarrerSimulation, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(arreterSimulation, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(stop, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(UndoSimulation, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(statistiquesBouton, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(conceptionLabel, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel26, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(ajouterPays, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(btnAppliquerMesureSanitaire, javax.swing.GroupLayout.DEFAULT_SIZE, 226, Short.MAX_VALUE)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, MenuBoutonLayout.createSequentialGroup()
                                .addGap(106, 106, 106)
                                .addComponent(plusDeMalade, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, MenuBoutonLayout.createSequentialGroup()
                                .addGroup(MenuBoutonLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(msJourLabel)
                                    .addComponent(nbrJourrLabel))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(MenuBoutonLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(nbrJours)
                                    .addComponent(msParJour, javax.swing.GroupLayout.DEFAULT_SIZE, 55, Short.MAX_VALUE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(MenuBoutonLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(enContinueRadio)
                                    .addGroup(MenuBoutonLayout.createSequentialGroup()
                                        .addComponent(jLabel36)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(numeroJour))))
                            .addComponent(btnModifierMaladie, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(btnAppliquerMaladie, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        MenuBoutonLayout.setVerticalGroup(
            MenuBoutonLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(MenuBoutonLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(simulationLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(2, 2, 2)
                .addGroup(MenuBoutonLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(msJourLabel)
                    .addComponent(msParJour, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel36)
                    .addComponent(numeroJour))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(MenuBoutonLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(nbrJours, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(nbrJourrLabel)
                    .addComponent(enContinueRadio))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(MenuBoutonLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(stop, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(arreterSimulation, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(demarrerSimulation, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(UndoSimulation, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(plusDeMalade)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(statistiquesBouton)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(conceptionLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel26, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(ajouterPays)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(nouvelleDivision)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(nouveauLien)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(ModifierPays)
                .addGap(8, 8, 8)
                .addComponent(jLabel28, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(CreerMaladieBouton)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnAppliquerMaladie)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnModifierMaladie)
                .addGap(9, 9, 9)
                .addComponent(jLabel29, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(ajouterMesureSanitaire)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnMesureSanitaire)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnAppliquerMesureSanitaire)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        SousMenuPanel.setBorder(javax.swing.BorderFactory.createEtchedBorder(new java.awt.Color(51, 51, 51), null));
        SousMenuPanel.setLayout(new java.awt.CardLayout());

        BienvenuePanel.setBackground(new java.awt.Color(5, 24, 28));

        bienvenue.setBackground(new java.awt.Color(5, 24, 28));
        bienvenue.setFont(new java.awt.Font("Dialog", 2, 24)); // NOI18N
        bienvenue.setForeground(new java.awt.Color(255, 255, 255));
        bienvenue.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        bienvenue.setText("SIMUDEMIE");

        jLabel27.setIcon(new javax.swing.ImageIcon(getClass().getResource("/viruspicture.jpg"))); // NOI18N

        javax.swing.GroupLayout BienvenuePanelLayout = new javax.swing.GroupLayout(BienvenuePanel);
        BienvenuePanel.setLayout(BienvenuePanelLayout);
        BienvenuePanelLayout.setHorizontalGroup(
            BienvenuePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel27, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
            .addGroup(BienvenuePanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(bienvenue, javax.swing.GroupLayout.DEFAULT_SIZE, 271, Short.MAX_VALUE)
                .addContainerGap())
        );
        BienvenuePanelLayout.setVerticalGroup(
            BienvenuePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(BienvenuePanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(bienvenue, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel27, javax.swing.GroupLayout.PREFERRED_SIZE, 659, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(305, Short.MAX_VALUE))
        );

        SousMenuPanel.add(BienvenuePanel, "card4");

        AjouterPays.setBackground(new java.awt.Color(51, 51, 51));
        AjouterPays.setPreferredSize(new java.awt.Dimension(250, 585));

        NouveauPaysLabel.setForeground(new java.awt.Color(255, 255, 255));
        NouveauPaysLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        NouveauPaysLabel.setText("NOUVEAU PAYS");

        AnnulerNouveauPays.setBackground(new java.awt.Color(102, 102, 102));
        AnnulerNouveauPays.setForeground(new java.awt.Color(255, 255, 255));
        AnnulerNouveauPays.setText("ANNULER");
        AnnulerNouveauPays.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                AnnulerNouveauPaysActionPerformed(evt);
            }
        });

        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel2.setText("Forme");

        annulerDernierPoint.setBackground(new java.awt.Color(102, 102, 102));
        annulerDernierPoint.setFont(new java.awt.Font("Dialog", 0, 8)); // NOI18N
        annulerDernierPoint.setForeground(new java.awt.Color(255, 255, 255));
        annulerDernierPoint.setText("Annuler Point");
        annulerDernierPoint.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                annulerDernierPointActionPerformed(evt);
            }
        });

        jLabel3.setForeground(new java.awt.Color(255, 255, 255));
        jLabel3.setText("Nom du pays :");

        nomPays.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                nomPaysActionPerformed(evt);
            }
        });

        populationLabel.setForeground(new java.awt.Color(255, 255, 255));
        populationLabel.setText("Population :");

        population.setModel(new javax.swing.SpinnerNumberModel(1, 1, 2147483647, 1));

        jLabel4.setForeground(new java.awt.Color(255, 255, 255));
        jLabel4.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel4.setText("Taux de déplacement");

        terrestreLabel.setForeground(new java.awt.Color(255, 255, 255));
        terrestreLabel.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        terrestreLabel.setText("Terrestre");

        tauxTerrestre.setModel(new javax.swing.SpinnerNumberModel(Float.valueOf(0.0f), Float.valueOf(0.0f), Float.valueOf(1.0f), Float.valueOf(0.01f)));

        maritimeLabel.setForeground(new java.awt.Color(255, 255, 255));
        maritimeLabel.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        maritimeLabel.setText("Maritime");

        tauxMaritime.setModel(new javax.swing.SpinnerNumberModel(Float.valueOf(0.0f), Float.valueOf(0.0f), Float.valueOf(1.0f), Float.valueOf(0.01f)));

        aerienLabel.setForeground(new java.awt.Color(255, 255, 255));
        aerienLabel.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        aerienLabel.setText("Aérien");

        tauxAerien.setModel(new javax.swing.SpinnerNumberModel(Float.valueOf(0.0f), Float.valueOf(0.0f), Float.valueOf(1.0f), Float.valueOf(0.01f)));

        regionnalLabel.setForeground(new java.awt.Color(255, 255, 255));
        regionnalLabel.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        regionnalLabel.setText("Régionnal");

        tauxRegionnal.setModel(new javax.swing.SpinnerNumberModel(Float.valueOf(0.0f), Float.valueOf(0.0f), Float.valueOf(1.0f), Float.valueOf(0.01f)));

        creerPaysButton.setBackground(new java.awt.Color(102, 102, 102));
        creerPaysButton.setForeground(new java.awt.Color(255, 255, 255));
        creerPaysButton.setText("CRÉER");
        creerPaysButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                creerPaysButtonActionPerformed(evt);
            }
        });

        erreurInfosPays.setFont(new java.awt.Font("Dialog", 0, 9)); // NOI18N
        erreurInfosPays.setForeground(new java.awt.Color(255, 0, 51));
        erreurInfosPays.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);

        jLabel25.setForeground(new java.awt.Color(255, 255, 255));
        jLabel25.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel25.setText("Niveau d'infection de la population");

        pctSain.setModel(new javax.swing.SpinnerNumberModel(Float.valueOf(0.0f), Float.valueOf(0.0f), Float.valueOf(100.0f), Float.valueOf(0.01f)));

        pctSaineLabel.setForeground(new java.awt.Color(255, 255, 255));
        pctSaineLabel.setText("% Saine");

        pctInfecter.setModel(new javax.swing.SpinnerNumberModel(Float.valueOf(0.0f), Float.valueOf(0.0f), Float.valueOf(100.0f), Float.valueOf(0.01f)));

        pctInfecterLabel.setForeground(new java.awt.Color(255, 255, 255));
        pctInfecterLabel.setText("% Infecté");

        pctMort.setModel(new javax.swing.SpinnerNumberModel(0.0d, 0.0d, 100.0d, 0.01d));

        pctMorteLabel.setForeground(new java.awt.Color(255, 255, 255));
        pctMorteLabel.setText("% Morte");

        erreurPctPop.setFont(new java.awt.Font("Dialog", 0, 10)); // NOI18N
        erreurPctPop.setForeground(new java.awt.Color(255, 0, 51));
        erreurPctPop.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);

        formeIrr.setBackground(new java.awt.Color(51, 51, 51));
        formeIrr.setForeground(new java.awt.Color(255, 255, 255));
        formeIrr.setSelected(true);
        formeIrr.setText("Forme Irregulière");
        formeIrr.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                formeIrrItemStateChanged(evt);
            }
        });

        comboForme.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Rectangle", "Carré", "Triangle", "Cercle" }));
        comboForme.setEnabled(false);

        jLabel30.setForeground(new java.awt.Color(255, 255, 255));
        jLabel30.setText("Forme Regulière");

        javax.swing.GroupLayout AjouterPaysLayout = new javax.swing.GroupLayout(AjouterPays);
        AjouterPays.setLayout(AjouterPaysLayout);
        AjouterPaysLayout.setHorizontalGroup(
            AjouterPaysLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(AjouterPaysLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(AjouterPaysLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(AjouterPaysLayout.createSequentialGroup()
                        .addComponent(erreurPctPop, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addContainerGap())
                    .addComponent(NouveauPaysLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel25, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(AjouterPaysLayout.createSequentialGroup()
                        .addGroup(AjouterPaysLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(populationLabel)
                            .addComponent(jLabel3))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(AjouterPaysLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(population)
                            .addComponent(nomPays, javax.swing.GroupLayout.Alignment.TRAILING))
                        .addContainerGap())
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, AjouterPaysLayout.createSequentialGroup()
                        .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGap(81, 81, 81))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, AjouterPaysLayout.createSequentialGroup()
                        .addGroup(AjouterPaysLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, AjouterPaysLayout.createSequentialGroup()
                                .addGap(14, 14, 14)
                                .addGroup(AjouterPaysLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(comboForme, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel30))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGroup(AjouterPaysLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(formeIrr)
                                    .addComponent(annulerDernierPoint, javax.swing.GroupLayout.PREFERRED_SIZE, 86, javax.swing.GroupLayout.PREFERRED_SIZE))))
                        .addGap(6, 6, 6))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, AjouterPaysLayout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addGroup(AjouterPaysLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, AjouterPaysLayout.createSequentialGroup()
                                .addGroup(AjouterPaysLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(pctSaineLabel, javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(pctInfecterLabel, javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(pctMorteLabel, javax.swing.GroupLayout.Alignment.TRAILING))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(AjouterPaysLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(AjouterPaysLayout.createSequentialGroup()
                                        .addGroup(AjouterPaysLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                            .addComponent(pctInfecter, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 1, Short.MAX_VALUE)
                                            .addComponent(pctMort, javax.swing.GroupLayout.PREFERRED_SIZE, 62, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGap(1, 1, 1))
                                    .addComponent(pctSain, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 62, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, AjouterPaysLayout.createSequentialGroup()
                                .addComponent(regionnalLabel)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(tauxRegionnal, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, AjouterPaysLayout.createSequentialGroup()
                                .addComponent(aerienLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 63, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(tauxAerien, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, AjouterPaysLayout.createSequentialGroup()
                                .addComponent(terrestreLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(tauxTerrestre, javax.swing.GroupLayout.PREFERRED_SIZE, 61, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, AjouterPaysLayout.createSequentialGroup()
                                .addComponent(maritimeLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(tauxMaritime, javax.swing.GroupLayout.PREFERRED_SIZE, 61, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addContainerGap())
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, AjouterPaysLayout.createSequentialGroup()
                        .addGroup(AjouterPaysLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(erreurInfosPays, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(AnnulerNouveauPays, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(creerPaysButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addContainerGap())))
        );
        AjouterPaysLayout.setVerticalGroup(
            AjouterPaysLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(AjouterPaysLayout.createSequentialGroup()
                .addGap(12, 12, 12)
                .addComponent(NouveauPaysLabel)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(AjouterPaysLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(nomPays))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(AjouterPaysLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(population, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(populationLabel))
                .addGap(18, 18, 18)
                .addComponent(jLabel25)
                .addGap(22, 22, 22)
                .addGroup(AjouterPaysLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(pctSain, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(pctSaineLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(6, 6, 6)
                .addGroup(AjouterPaysLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(pctInfecter, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(pctInfecterLabel))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(AjouterPaysLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(pctMort, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(pctMorteLabel))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(erreurPctPop)
                .addGap(45, 45, 45)
                .addComponent(jLabel4)
                .addGap(28, 28, 28)
                .addGroup(AjouterPaysLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(tauxTerrestre)
                    .addComponent(terrestreLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(AjouterPaysLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(tauxMaritime, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(maritimeLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(AjouterPaysLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(tauxAerien, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(aerienLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(AjouterPaysLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(tauxRegionnal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(regionnalLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(38, 38, 38)
                .addComponent(jLabel2)
                .addGap(20, 20, 20)
                .addGroup(AjouterPaysLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(formeIrr)
                    .addComponent(jLabel30))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(AjouterPaysLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(annulerDernierPoint)
                    .addComponent(comboForme, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(creerPaysButton)
                .addGap(5, 5, 5)
                .addComponent(AnnulerNouveauPays)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(erreurInfosPays)
                .addContainerGap())
        );

        SousMenuPanel.add(AjouterPays, "card2");

        CreerMaladie.setBackground(new java.awt.Color(51, 51, 51));
        CreerMaladie.setPreferredSize(new java.awt.Dimension(250, 585));

        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("CREER MALADIE");

        AnnulerCreerMaladie.setBackground(new java.awt.Color(102, 102, 102));
        AnnulerCreerMaladie.setForeground(new java.awt.Color(255, 255, 255));
        AnnulerCreerMaladie.setText("QUITTER");
        AnnulerCreerMaladie.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                AnnulerCreerMaladieActionPerformed(evt);
            }
        });

        LabelNomMaladie.setForeground(new java.awt.Color(255, 255, 255));
        LabelNomMaladie.setText("Nom maladie");

        LabelTauxContagion.setForeground(new java.awt.Color(255, 255, 255));
        LabelTauxContagion.setText("Taux transmission (%)");

        LabelTauxGuerison.setForeground(new java.awt.Color(255, 255, 255));
        LabelTauxGuerison.setText("Taux guérison (%)");

        LabelTauxMortalite.setForeground(new java.awt.Color(255, 255, 255));
        LabelTauxMortalite.setText("Taux mortalité (%)");

        NumericTauxContagion.setModel(new javax.swing.SpinnerNumberModel(Float.valueOf(0.0f), Float.valueOf(0.0f), Float.valueOf(1.0f), Float.valueOf(0.01f)));

        NumericTauxGuerison.setModel(new javax.swing.SpinnerNumberModel(Float.valueOf(0.0f), Float.valueOf(0.0f), Float.valueOf(1.0f), Float.valueOf(0.01f)));

        NumericTauxMortalite.setModel(new javax.swing.SpinnerNumberModel(Float.valueOf(0.0f), Float.valueOf(0.0f), Float.valueOf(1.0f), Float.valueOf(0.01f)));

        btnOKMaladie.setBackground(new java.awt.Color(102, 102, 102));
        btnOKMaladie.setForeground(new java.awt.Color(255, 255, 255));
        btnOKMaladie.setText("OK");
        btnOKMaladie.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnOKMaladieActionPerformed(evt);
            }
        });

        jLabel61.setForeground(new java.awt.Color(255, 255, 255));
        jLabel61.setText("Taux de reproduction");

        NumericTauxReproduction.setModel(new javax.swing.SpinnerNumberModel(0.0f, 0.0f, null, 1.0f));

        javax.swing.GroupLayout CreerMaladieLayout = new javax.swing.GroupLayout(CreerMaladie);
        CreerMaladie.setLayout(CreerMaladieLayout);
        CreerMaladieLayout.setHorizontalGroup(
            CreerMaladieLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(CreerMaladieLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(CreerMaladieLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnOKMaladie, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(InfoMaladie, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(CreerMaladieLayout.createSequentialGroup()
                        .addComponent(LabelNomMaladie, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(txtNomMaladie, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(AnnulerCreerMaladie, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, CreerMaladieLayout.createSequentialGroup()
                        .addComponent(LabelTauxMortalite, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(NumericTauxMortalite, javax.swing.GroupLayout.PREFERRED_SIZE, 58, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, CreerMaladieLayout.createSequentialGroup()
                        .addComponent(jLabel61, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(NumericTauxReproduction, javax.swing.GroupLayout.PREFERRED_SIZE, 58, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, CreerMaladieLayout.createSequentialGroup()
                        .addGroup(CreerMaladieLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(LabelTauxGuerison, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(LabelTauxContagion, javax.swing.GroupLayout.DEFAULT_SIZE, 215, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(CreerMaladieLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(NumericTauxContagion, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 58, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(NumericTauxGuerison, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 58, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap())
        );
        CreerMaladieLayout.setVerticalGroup(
            CreerMaladieLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(CreerMaladieLayout.createSequentialGroup()
                .addGap(13, 13, 13)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(CreerMaladieLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(LabelNomMaladie)
                    .addComponent(txtNomMaladie, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(28, 28, 28)
                .addGroup(CreerMaladieLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(NumericTauxReproduction, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel61))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(CreerMaladieLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(NumericTauxContagion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(LabelTauxContagion))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(CreerMaladieLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(NumericTauxGuerison, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(LabelTauxGuerison, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(CreerMaladieLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(NumericTauxMortalite, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(LabelTauxMortalite))
                .addGap(35, 35, 35)
                .addComponent(btnOKMaladie)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(AnnulerCreerMaladie)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 668, Short.MAX_VALUE)
                .addComponent(InfoMaladie)
                .addContainerGap())
        );

        LabelNomMaladie.getAccessibleContext().setAccessibleName("LabelNomMaladie");

        SousMenuPanel.add(CreerMaladie, "card3");

        AppliquerMaladie.setBackground(new java.awt.Color(51, 51, 51));

        jLabel55.setForeground(new java.awt.Color(255, 255, 255));
        jLabel55.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel55.setText("APPLIQUER MALADIE");

        jLabel56.setForeground(new java.awt.Color(255, 255, 255));
        jLabel56.setText("Selectionner maladie");

        jLabel57.setForeground(new java.awt.Color(255, 255, 255));
        jLabel57.setText("Taux transmission");

        jLabel58.setForeground(new java.awt.Color(255, 255, 255));
        jLabel58.setText("Taux guerison");

        jLabel59.setForeground(new java.awt.Color(255, 255, 255));
        jLabel59.setText("Taux mortalité");

        tauxTransmissionApplicationMaladie.setModel(new javax.swing.SpinnerNumberModel(Float.valueOf(0.0f), Float.valueOf(0.0f), Float.valueOf(1.0f), Float.valueOf(0.01f)));

        tauxGuerisonApplicationMaladie.setModel(new javax.swing.SpinnerNumberModel(Float.valueOf(0.0f), Float.valueOf(0.0f), Float.valueOf(1.0f), Float.valueOf(0.01f)));

        tauxMortaliteApplicationMaladie.setModel(new javax.swing.SpinnerNumberModel(Float.valueOf(0.0f), Float.valueOf(0.0f), Float.valueOf(1.0f), Float.valueOf(0.01f)));

        btnEnregistrerApplicationMaladie.setBackground(new java.awt.Color(102, 102, 102));
        btnEnregistrerApplicationMaladie.setForeground(new java.awt.Color(255, 255, 255));
        btnEnregistrerApplicationMaladie.setText("APPLIQUER MALADIE");
        btnEnregistrerApplicationMaladie.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEnregistrerApplicationMaladieActionPerformed(evt);
            }
        });

        btnQuitterApplicationMaladie.setBackground(new java.awt.Color(102, 102, 102));
        btnQuitterApplicationMaladie.setForeground(new java.awt.Color(255, 255, 255));
        btnQuitterApplicationMaladie.setText("QUITTER");
        btnQuitterApplicationMaladie.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnQuitterApplicationMaladieActionPerformed(evt);
            }
        });

        maladieAppliquerMaladie.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                maladieAppliquerMaladieItemStateChanged(evt);
            }
        });

        nomMaladieAppliquerProjet.setEditable(false);

        jLabel60.setForeground(new java.awt.Color(255, 255, 255));
        jLabel60.setText("Maladie appliquer au projet");

        jLabel63.setForeground(new java.awt.Color(255, 255, 255));
        jLabel63.setText("Taux reproduction");

        tauxReproductionApplicationMaladie.setModel(new javax.swing.SpinnerNumberModel(Float.valueOf(0.0f), Float.valueOf(0.0f), Float.valueOf(1.0f), Float.valueOf(0.01f)));

        javax.swing.GroupLayout AppliquerMaladieLayout = new javax.swing.GroupLayout(AppliquerMaladie);
        AppliquerMaladie.setLayout(AppliquerMaladieLayout);
        AppliquerMaladieLayout.setHorizontalGroup(
            AppliquerMaladieLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(AppliquerMaladieLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(AppliquerMaladieLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel55, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(AppliquerMaladieLayout.createSequentialGroup()
                        .addGroup(AppliquerMaladieLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, AppliquerMaladieLayout.createSequentialGroup()
                                .addComponent(jLabel57)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(tauxTransmissionApplicationMaladie, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, AppliquerMaladieLayout.createSequentialGroup()
                                .addComponent(jLabel58)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(tauxGuerisonApplicationMaladie, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, AppliquerMaladieLayout.createSequentialGroup()
                                .addComponent(jLabel59)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(tauxMortaliteApplicationMaladie, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(btnQuitterApplicationMaladie, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(btnEnregistrerApplicationMaladie, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(AppliquerMaladieLayout.createSequentialGroup()
                                .addGroup(AppliquerMaladieLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel56)
                                    .addComponent(jLabel60))
                                .addGroup(AppliquerMaladieLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addGroup(AppliquerMaladieLayout.createSequentialGroup()
                                        .addGap(31, 31, 31)
                                        .addComponent(maladieAppliquerMaladie, javax.swing.GroupLayout.PREFERRED_SIZE, 92, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(AppliquerMaladieLayout.createSequentialGroup()
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(nomMaladieAppliquerProjet))))
                            .addGroup(AppliquerMaladieLayout.createSequentialGroup()
                                .addComponent(jLabel63)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(tauxReproductionApplicationMaladie, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(0, 18, Short.MAX_VALUE)))
                .addContainerGap())
        );
        AppliquerMaladieLayout.setVerticalGroup(
            AppliquerMaladieLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(AppliquerMaladieLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel55)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(AppliquerMaladieLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(nomMaladieAppliquerProjet, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel60))
                .addGap(41, 41, 41)
                .addGroup(AppliquerMaladieLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel56)
                    .addComponent(maladieAppliquerMaladie, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(AppliquerMaladieLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel63)
                    .addComponent(tauxReproductionApplicationMaladie, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(AppliquerMaladieLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(tauxTransmissionApplicationMaladie, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel57))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(AppliquerMaladieLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(tauxGuerisonApplicationMaladie, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel58))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(AppliquerMaladieLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(tauxMortaliteApplicationMaladie, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel59))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnEnregistrerApplicationMaladie)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnQuitterApplicationMaladie)
                .addContainerGap(665, Short.MAX_VALUE))
        );

        SousMenuPanel.add(AppliquerMaladie, "card13");

        ModifierMaladie.setBackground(new java.awt.Color(51, 51, 51));

        jLabel50.setForeground(new java.awt.Color(255, 255, 255));
        jLabel50.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel50.setText("MODIFIER MALADIE");

        jLabel51.setForeground(new java.awt.Color(255, 255, 255));
        jLabel51.setText("Nom maladie");

        listeMaladieModifMaladie.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                listeMaladieModifMaladieItemStateChanged(evt);
            }
        });

        jLabel52.setForeground(new java.awt.Color(255, 255, 255));
        jLabel52.setText("Taux transmission (%)");

        jLabel53.setForeground(new java.awt.Color(255, 255, 255));
        jLabel53.setText("Taux guerison (%)");

        jLabel54.setForeground(new java.awt.Color(255, 255, 255));
        jLabel54.setText("Taux mortalité (%)");

        tauxTransmissionModifMaladie.setModel(new javax.swing.SpinnerNumberModel(Float.valueOf(0.0f), Float.valueOf(0.0f), Float.valueOf(1.0f), Float.valueOf(0.01f)));

        tauxGuerisonModifMaladie.setModel(new javax.swing.SpinnerNumberModel(Float.valueOf(0.0f), Float.valueOf(0.0f), Float.valueOf(1.0f), Float.valueOf(0.01f)));

        tauxMortaliteModifMaladie.setModel(new javax.swing.SpinnerNumberModel(Float.valueOf(0.0f), Float.valueOf(0.0f), Float.valueOf(1.0f), Float.valueOf(0.01f)));

        btnEnregistrerModifMaladie.setBackground(new java.awt.Color(102, 102, 102));
        btnEnregistrerModifMaladie.setForeground(new java.awt.Color(255, 255, 255));
        btnEnregistrerModifMaladie.setText("ENREGISTRER");
        btnEnregistrerModifMaladie.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEnregistrerModifMaladieActionPerformed(evt);
            }
        });

        btnQuitterEnregistreMaladie.setBackground(new java.awt.Color(102, 102, 102));
        btnQuitterEnregistreMaladie.setForeground(new java.awt.Color(255, 255, 255));
        btnQuitterEnregistreMaladie.setText("QUITTER");
        btnQuitterEnregistreMaladie.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnQuitterEnregistreMaladieActionPerformed(evt);
            }
        });

        btnSupprimerMaladie.setBackground(new java.awt.Color(102, 102, 102));
        btnSupprimerMaladie.setForeground(new java.awt.Color(255, 255, 255));
        btnSupprimerMaladie.setText("SUPPRIMER MALADIE");
        btnSupprimerMaladie.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSupprimerMaladieActionPerformed(evt);
            }
        });

        tauxReproductionModifMaladie.setModel(new javax.swing.SpinnerNumberModel(0.0f, 0.0f, null, 1.0f));

        jLabel62.setForeground(new java.awt.Color(255, 255, 255));
        jLabel62.setText("Taux reproduction");

        javax.swing.GroupLayout ModifierMaladieLayout = new javax.swing.GroupLayout(ModifierMaladie);
        ModifierMaladie.setLayout(ModifierMaladieLayout);
        ModifierMaladieLayout.setHorizontalGroup(
            ModifierMaladieLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(ModifierMaladieLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(ModifierMaladieLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnEnregistrerModifMaladie, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel50, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(ModifierMaladieLayout.createSequentialGroup()
                        .addComponent(jLabel51)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(listeMaladieModifMaladie, javax.swing.GroupLayout.PREFERRED_SIZE, 127, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(btnQuitterEnregistreMaladie, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnSupprimerMaladie, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(ModifierMaladieLayout.createSequentialGroup()
                        .addGroup(ModifierMaladieLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel52)
                            .addComponent(jLabel53)
                            .addComponent(jLabel54)
                            .addComponent(jLabel62))
                        .addGap(56, 56, 56)
                        .addGroup(ModifierMaladieLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(tauxGuerisonModifMaladie, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(tauxTransmissionModifMaladie, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(tauxMortaliteModifMaladie, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(tauxReproductionModifMaladie, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(0, 40, Short.MAX_VALUE)))
                .addContainerGap())
        );
        ModifierMaladieLayout.setVerticalGroup(
            ModifierMaladieLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(ModifierMaladieLayout.createSequentialGroup()
                .addGap(26, 26, 26)
                .addComponent(jLabel50)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(ModifierMaladieLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel51)
                    .addComponent(listeMaladieModifMaladie, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnSupprimerMaladie)
                .addGap(25, 25, 25)
                .addGroup(ModifierMaladieLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(tauxReproductionModifMaladie, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel62))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(ModifierMaladieLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(tauxTransmissionModifMaladie, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel52))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(ModifierMaladieLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(tauxGuerisonModifMaladie, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel53))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(ModifierMaladieLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(tauxMortaliteModifMaladie, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel54))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnEnregistrerModifMaladie)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnQuitterEnregistreMaladie)
                .addContainerGap(655, Short.MAX_VALUE))
        );

        SousMenuPanel.add(ModifierMaladie, "card12");

        DiviserPays.setBackground(new java.awt.Color(51, 51, 51));
        DiviserPays.setForeground(new java.awt.Color(153, 153, 153));
        DiviserPays.setPreferredSize(new java.awt.Dimension(400, 698));

        titreDivisionPays.setForeground(new java.awt.Color(255, 255, 255));
        titreDivisionPays.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        titreDivisionPays.setText("DIVISER PAYS");

        selectedPays.setEditable(false);

        jLabel24.setForeground(new java.awt.Color(255, 255, 255));
        jLabel24.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel24.setText("Pays selectionné");

        tableRegion.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Nom", "%Pays", "%Sains", "%Infectés", "%Morts"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.Float.class, java.lang.Float.class, java.lang.Float.class, java.lang.Float.class
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }
        });
        tableRegion.setGridColor(new java.awt.Color(0, 0, 0));
        tableRegion.setOpaque(false);
        jScrollPane2.setViewportView(tableRegion);

        diviser.setBackground(new java.awt.Color(102, 102, 102));
        diviser.setForeground(new java.awt.Color(255, 255, 255));
        diviser.setText("DIVISER");
        diviser.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                diviserActionPerformed(evt);
            }
        });

        annulerDivision.setBackground(new java.awt.Color(102, 102, 102));
        annulerDivision.setForeground(new java.awt.Color(255, 255, 255));
        annulerDivision.setText("ANNULER");
        annulerDivision.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                annulerDivisionActionPerformed(evt);
            }
        });

        enleverSelectionRegion.setBackground(new java.awt.Color(102, 102, 102));
        enleverSelectionRegion.setForeground(new java.awt.Color(255, 255, 255));
        enleverSelectionRegion.setText("ENLEVER SELECTION");
        enleverSelectionRegion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                enleverSelectionRegionActionPerformed(evt);
            }
        });

        ajoutLigne.setBackground(new java.awt.Color(102, 102, 102));
        ajoutLigne.setForeground(new java.awt.Color(255, 255, 255));
        ajoutLigne.setText("AJOUTER REGION");
        ajoutLigne.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ajoutLigneActionPerformed(evt);
            }
        });

        erreurDivision.setForeground(new java.awt.Color(255, 0, 0));

        effLigne.setBackground(new java.awt.Color(102, 102, 102));
        effLigne.setForeground(new java.awt.Color(255, 255, 255));
        effLigne.setText("SUPPRIMER REGION");
        effLigne.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                effLigneActionPerformed(evt);
            }
        });

        nouvForme.setText("SOUMETTRE FORME");
        nouvForme.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                nouvFormeActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout DiviserPaysLayout = new javax.swing.GroupLayout(DiviserPays);
        DiviserPays.setLayout(DiviserPaysLayout);
        DiviserPaysLayout.setHorizontalGroup(
            DiviserPaysLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
            .addGroup(DiviserPaysLayout.createSequentialGroup()
                .addGroup(DiviserPaysLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(DiviserPaysLayout.createSequentialGroup()
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(DiviserPaysLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(enleverSelectionRegion, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(titreDivisionPays, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(DiviserPaysLayout.createSequentialGroup()
                                .addGroup(DiviserPaysLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(erreurDivision)
                                    .addGroup(DiviserPaysLayout.createSequentialGroup()
                                        .addComponent(jLabel24, javax.swing.GroupLayout.PREFERRED_SIZE, 113, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(selectedPays, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addGap(0, 107, Short.MAX_VALUE))
                            .addComponent(diviser, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(annulerDivision, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, DiviserPaysLayout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addGroup(DiviserPaysLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(ajoutLigne, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(effLigne, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(nouvForme, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap())
        );
        DiviserPaysLayout.setVerticalGroup(
            DiviserPaysLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(DiviserPaysLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(titreDivisionPays)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(DiviserPaysLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(selectedPays, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel24))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 309, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(nouvForme)
                .addGap(4, 4, 4)
                .addComponent(ajoutLigne)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(effLigne)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(enleverSelectionRegion)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(diviser)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(annulerDivision)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(erreurDivision)
                .addGap(109, 109, 109))
        );

        SousMenuPanel.add(DiviserPays, "card8");

        AjouterMesureSanitaire.setBackground(new java.awt.Color(51, 51, 51));
        AjouterMesureSanitaire.setForeground(new java.awt.Color(255, 255, 255));

        jLabel15.setForeground(new java.awt.Color(255, 255, 255));
        jLabel15.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel15.setText("AJOUTER MESURE SANITAIRE");

        jLabel16.setForeground(new java.awt.Color(255, 255, 255));
        jLabel16.setText("Nom mesure");

        NomNouvelleMesure.setHorizontalAlignment(javax.swing.JTextField.RIGHT);

        jLabel17.setForeground(new java.awt.Color(255, 255, 255));
        jLabel17.setText("Impact taux contagion");

        jLabel18.setForeground(new java.awt.Color(255, 255, 255));
        jLabel18.setText("Impact taux guerison");

        jLabel19.setForeground(new java.awt.Color(255, 255, 255));
        jLabel19.setText("Impact taux mortalité");

        jLabel20.setForeground(new java.awt.Color(255, 255, 255));
        jLabel20.setText("Impact taux dép. terrestre");

        jLabel21.setForeground(new java.awt.Color(255, 255, 255));
        jLabel21.setText("Impact taux dép. maritime");

        jLabel22.setForeground(new java.awt.Color(255, 255, 255));
        jLabel22.setText("Impact taux dép. aérien");

        jLabel23.setForeground(new java.awt.Color(255, 255, 255));
        jLabel23.setText("Impact taux dép. région");

        NMImpactTauxContagion.setModel(new javax.swing.SpinnerNumberModel(Float.valueOf(0.0f), Float.valueOf(-1.0f), Float.valueOf(1.0f), Float.valueOf(0.01f)));

        NMImpactTauxGuerison.setModel(new javax.swing.SpinnerNumberModel(Float.valueOf(0.0f), Float.valueOf(-1.0f), Float.valueOf(1.0f), Float.valueOf(0.01f)));

        NMImpactTauxMortalite.setModel(new javax.swing.SpinnerNumberModel(Float.valueOf(0.0f), Float.valueOf(-1.0f), Float.valueOf(1.0f), Float.valueOf(0.01f)));

        NMImpactTauxTerrestre.setModel(new javax.swing.SpinnerNumberModel(Float.valueOf(0.0f), Float.valueOf(-1.0f), Float.valueOf(1.0f), Float.valueOf(0.01f)));

        NMImpactTauxMaritime.setModel(new javax.swing.SpinnerNumberModel(Float.valueOf(0.0f), Float.valueOf(-1.0f), Float.valueOf(1.0f), Float.valueOf(0.01f)));

        NMImpactTauxAerien.setModel(new javax.swing.SpinnerNumberModel(Float.valueOf(0.0f), Float.valueOf(-1.0f), Float.valueOf(1.0f), Float.valueOf(0.01f)));

        NMImpactTauxRegion.setModel(new javax.swing.SpinnerNumberModel(Float.valueOf(0.0f), Float.valueOf(-1.0f), Float.valueOf(1.0f), Float.valueOf(0.01f)));

        CreerMesure.setBackground(new java.awt.Color(102, 102, 102));
        CreerMesure.setForeground(new java.awt.Color(255, 255, 255));
        CreerMesure.setText("Créer mesure");
        CreerMesure.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CreerMesureActionPerformed(evt);
            }
        });

        AnnulerCreationMesure.setBackground(new java.awt.Color(102, 102, 102));
        AnnulerCreationMesure.setForeground(new java.awt.Color(255, 255, 255));
        AnnulerCreationMesure.setText("Quitter");
        AnnulerCreationMesure.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                AnnulerCreationMesureActionPerformed(evt);
            }
        });

        ErreurInfoNouvelleMesure.setFont(new java.awt.Font("Tahoma", 0, 9)); // NOI18N
        ErreurInfoNouvelleMesure.setForeground(new java.awt.Color(255, 0, 0));
        ErreurInfoNouvelleMesure.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        InfoAjouterMesure.setForeground(new java.awt.Color(255, 0, 0));
        InfoAjouterMesure.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        InfoAjouterMesure.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));

        jLabel64.setForeground(new java.awt.Color(255, 255, 255));
        jLabel64.setText("Impact taux reproduction");

        NMImpactTauxReproduction.setModel(new javax.swing.SpinnerNumberModel(Float.valueOf(0.0f), Float.valueOf(-1.0f), Float.valueOf(1.0f), Float.valueOf(0.01f)));

        javax.swing.GroupLayout AjouterMesureSanitaireLayout = new javax.swing.GroupLayout(AjouterMesureSanitaire);
        AjouterMesureSanitaire.setLayout(AjouterMesureSanitaireLayout);
        AjouterMesureSanitaireLayout.setHorizontalGroup(
            AjouterMesureSanitaireLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(AjouterMesureSanitaireLayout.createSequentialGroup()
                .addGroup(AjouterMesureSanitaireLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel15, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(AjouterMesureSanitaireLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel16)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(InfoAjouterMesure, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
            .addGroup(AjouterMesureSanitaireLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(AjouterMesureSanitaireLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(AjouterMesureSanitaireLayout.createSequentialGroup()
                        .addComponent(ErreurInfoNouvelleMesure, javax.swing.GroupLayout.PREFERRED_SIZE, 217, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(117, Short.MAX_VALUE))
                    .addGroup(AjouterMesureSanitaireLayout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(NomNouvelleMesure, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(25, 25, 25))
                    .addGroup(AjouterMesureSanitaireLayout.createSequentialGroup()
                        .addGroup(AjouterMesureSanitaireLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel17)
                            .addGroup(AjouterMesureSanitaireLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addGroup(AjouterMesureSanitaireLayout.createSequentialGroup()
                                    .addGroup(AjouterMesureSanitaireLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jLabel18)
                                        .addComponent(jLabel19)
                                        .addComponent(jLabel20)
                                        .addComponent(jLabel22)
                                        .addComponent(jLabel23)
                                        .addComponent(jLabel21))
                                    .addGap(28, 28, 28)
                                    .addGroup(AjouterMesureSanitaireLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(NMImpactTauxGuerison, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(NMImpactTauxMortalite, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(NMImpactTauxTerrestre, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(NMImpactTauxMaritime, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(NMImpactTauxAerien, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(NMImpactTauxRegion, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(NMImpactTauxContagion, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(NMImpactTauxReproduction, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addGroup(AjouterMesureSanitaireLayout.createSequentialGroup()
                                    .addComponent(CreerMesure)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(AnnulerCreationMesure)))
                            .addComponent(jLabel64))
                        .addGap(0, 0, Short.MAX_VALUE))))
        );
        AjouterMesureSanitaireLayout.setVerticalGroup(
            AjouterMesureSanitaireLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(AjouterMesureSanitaireLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel15)
                .addGap(18, 18, 18)
                .addGroup(AjouterMesureSanitaireLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel16)
                    .addComponent(NomNouvelleMesure, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(40, 40, 40)
                .addGroup(AjouterMesureSanitaireLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel64, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(NMImpactTauxReproduction, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(AjouterMesureSanitaireLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel17)
                    .addComponent(NMImpactTauxContagion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(6, 6, 6)
                .addGroup(AjouterMesureSanitaireLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel18)
                    .addComponent(NMImpactTauxGuerison, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(AjouterMesureSanitaireLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel19)
                    .addComponent(NMImpactTauxMortalite, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(AjouterMesureSanitaireLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel20)
                    .addComponent(NMImpactTauxTerrestre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(AjouterMesureSanitaireLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel21)
                    .addComponent(NMImpactTauxMaritime, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(AjouterMesureSanitaireLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel22)
                    .addComponent(NMImpactTauxAerien, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(AjouterMesureSanitaireLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel23)
                    .addComponent(NMImpactTauxRegion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(34, 34, 34)
                .addGroup(AjouterMesureSanitaireLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(CreerMesure)
                    .addComponent(AnnulerCreationMesure))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 512, Short.MAX_VALUE)
                .addComponent(InfoAjouterMesure)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(ErreurInfoNouvelleMesure)
                .addContainerGap())
        );

        SousMenuPanel.add(AjouterMesureSanitaire, "card6");

        AppliquerMesureSanitaire.setBackground(new java.awt.Color(51, 51, 51));

        jLabel31.setForeground(new java.awt.Color(255, 255, 255));
        jLabel31.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel31.setText("APPLIQUER MESURE SANITAIRE");

        AppliquerMesureListeMesure.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                AppliquerMesureListeMesureItemStateChanged(evt);
            }
        });

        jLabel32.setForeground(new java.awt.Color(255, 255, 255));
        jLabel32.setText("Nom Mesure Sanitaire");

        jLabel33.setForeground(new java.awt.Color(255, 255, 255));
        jLabel33.setText("Pays");

        AppliquerMesureListePays.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                AppliquerMesureListePaysItemStateChanged(evt);
            }
        });

        tableApplicationMesure.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Nom Région", "Taux d'adhésion"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.Float.class
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }
        });
        jScrollPane1.setViewportView(tableApplicationMesure);

        btnEnregistrer.setBackground(new java.awt.Color(102, 102, 102));
        btnEnregistrer.setForeground(new java.awt.Color(255, 255, 255));
        btnEnregistrer.setText("ENREGISTRER");
        btnEnregistrer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEnregistrerActionPerformed(evt);
            }
        });

        btnQuitter.setBackground(new java.awt.Color(102, 102, 102));
        btnQuitter.setForeground(new java.awt.Color(255, 255, 255));
        btnQuitter.setText("QUITTER");
        btnQuitter.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnQuitterActionPerformed(evt);
            }
        });

        AppliquerMesureErreurInfo.setForeground(new java.awt.Color(255, 0, 0));
        AppliquerMesureErreurInfo.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        AppliquerMesureErreurInfo.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        btnSupprimerApplicationMesure.setBackground(new java.awt.Color(102, 102, 102));
        btnSupprimerApplicationMesure.setForeground(new java.awt.Color(255, 255, 255));
        btnSupprimerApplicationMesure.setText("SUPPRIMER APPLICATION");
        btnSupprimerApplicationMesure.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSupprimerApplicationMesureActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout AppliquerMesureSanitaireLayout = new javax.swing.GroupLayout(AppliquerMesureSanitaire);
        AppliquerMesureSanitaire.setLayout(AppliquerMesureSanitaireLayout);
        AppliquerMesureSanitaireLayout.setHorizontalGroup(
            AppliquerMesureSanitaireLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(AppliquerMesureSanitaireLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(AppliquerMesureSanitaireLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                    .addComponent(jLabel31, javax.swing.GroupLayout.DEFAULT_SIZE, 388, Short.MAX_VALUE)
                    .addComponent(AppliquerMesureListeMesure, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(AppliquerMesureListePays, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnEnregistrer, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnQuitter, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(AppliquerMesureErreurInfo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(AppliquerMesureSanitaireLayout.createSequentialGroup()
                        .addGroup(AppliquerMesureSanitaireLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel32, javax.swing.GroupLayout.PREFERRED_SIZE, 146, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel33, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(btnSupprimerApplicationMesure, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        AppliquerMesureSanitaireLayout.setVerticalGroup(
            AppliquerMesureSanitaireLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(AppliquerMesureSanitaireLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel31)
                .addGap(29, 29, 29)
                .addComponent(jLabel32)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(AppliquerMesureListeMesure, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLabel33)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(AppliquerMesureListePays, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(16, 16, 16)
                .addComponent(btnSupprimerApplicationMesure, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 240, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btnEnregistrer)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnQuitter)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 408, Short.MAX_VALUE)
                .addComponent(AppliquerMesureErreurInfo)
                .addGap(54, 54, 54))
        );

        SousMenuPanel.add(AppliquerMesureSanitaire, "card10");

        EditionLien.setBackground(new java.awt.Color(51, 51, 51));
        EditionLien.setForeground(new java.awt.Color(255, 255, 255));

        creerLien.setBackground(new java.awt.Color(102, 102, 102));
        creerLien.setForeground(new java.awt.Color(255, 255, 255));
        creerLien.setText("CRÉER");
        creerLien.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                creerLienActionPerformed(evt);
            }
        });

        terminerEditionLien.setBackground(new java.awt.Color(102, 102, 102));
        terminerEditionLien.setForeground(new java.awt.Color(255, 255, 255));
        terminerEditionLien.setText("TERMINER");
        terminerEditionLien.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                terminerEditionLienActionPerformed(evt);
            }
        });

        nomPaysA.setEditable(false);

        nomPaysB.setEditable(false);

        typeLien.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "MARITIME", "AERIEN", "TERRESTRE" }));

        enleverSelection.setBackground(new java.awt.Color(102, 102, 102));
        enleverSelection.setForeground(new java.awt.Color(255, 255, 255));
        enleverSelection.setText("ENLEVER SELECTION");
        enleverSelection.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                enleverSelectionActionPerformed(evt);
            }
        });

        erreurCreationLien.setForeground(new java.awt.Color(255, 51, 51));
        erreurCreationLien.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);

        lblTitreLien.setForeground(new java.awt.Color(255, 255, 255));
        lblTitreLien.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblTitreLien.setText("NOUVEAU LIEN");

        lblpaysA.setForeground(new java.awt.Color(255, 255, 255));
        lblpaysA.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblpaysA.setText("Nom du pays A");

        lblpaysB.setForeground(new java.awt.Color(255, 255, 255));
        lblpaysB.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblpaysB.setText("Nom du pays B");

        lblType.setForeground(new java.awt.Color(255, 255, 255));
        lblType.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblType.setText("Type du lien");

        terrestre.setForeground(new java.awt.Color(255, 255, 255));
        terrestre.setText("TERRESTRE");

        SupprimerLienLabel.setForeground(new java.awt.Color(255, 255, 255));
        SupprimerLienLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        SupprimerLienLabel.setText("SUPPRIMER LIEN");

        Aerien.setForeground(new java.awt.Color(255, 255, 255));
        Aerien.setText("AERIEN");

        Maritime.setForeground(new java.awt.Color(255, 255, 255));
        Maritime.setText("MARITIME");

        supprimerTerrestre.setBackground(new java.awt.Color(102, 102, 102));
        supprimerTerrestre.setForeground(new java.awt.Color(255, 255, 255));
        supprimerTerrestre.setText("SUPPRIMER");
        supprimerTerrestre.setEnabled(false);
        supprimerTerrestre.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                supprimerTerrestreActionPerformed(evt);
            }
        });

        supprimerAerien.setBackground(new java.awt.Color(102, 102, 102));
        supprimerAerien.setForeground(new java.awt.Color(255, 255, 255));
        supprimerAerien.setText("SUPPRIMER");
        supprimerAerien.setEnabled(false);
        supprimerAerien.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                supprimerAerienActionPerformed(evt);
            }
        });

        supprimerMaritime.setBackground(new java.awt.Color(102, 102, 102));
        supprimerMaritime.setForeground(new java.awt.Color(255, 255, 255));
        supprimerMaritime.setText("SUPPRIMER");
        supprimerMaritime.setEnabled(false);
        supprimerMaritime.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                supprimerMaritimeActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout EditionLienLayout = new javax.swing.GroupLayout(EditionLien);
        EditionLien.setLayout(EditionLienLayout);
        EditionLienLayout.setHorizontalGroup(
            EditionLienLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, EditionLienLayout.createSequentialGroup()
                .addGroup(EditionLienLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, EditionLienLayout.createSequentialGroup()
                        .addGroup(EditionLienLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(EditionLienLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(lblpaysB, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(lblpaysA, javax.swing.GroupLayout.DEFAULT_SIZE, 136, Short.MAX_VALUE))
                            .addComponent(lblType, javax.swing.GroupLayout.PREFERRED_SIZE, 136, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(EditionLienLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(nomPaysB)
                            .addComponent(nomPaysA)
                            .addComponent(typeLien, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addComponent(lblTitreLien, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, EditionLienLayout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(EditionLienLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(erreurCreationLien, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(SupprimerLienLabel, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(creerLien, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(terminerEditionLien, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(enleverSelection, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, EditionLienLayout.createSequentialGroup()
                                .addGap(0, 0, Short.MAX_VALUE)
                                .addGroup(EditionLienLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, EditionLienLayout.createSequentialGroup()
                                        .addComponent(Maritime)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(supprimerMaritime))
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, EditionLienLayout.createSequentialGroup()
                                        .addComponent(terrestre)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(supprimerTerrestre))
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, EditionLienLayout.createSequentialGroup()
                                        .addComponent(Aerien)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(supprimerAerien)))))))
                .addContainerGap())
        );
        EditionLienLayout.setVerticalGroup(
            EditionLienLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, EditionLienLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblTitreLien)
                .addGap(21, 21, 21)
                .addGroup(EditionLienLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(nomPaysA, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblpaysA))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(EditionLienLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(nomPaysB, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblpaysB))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(EditionLienLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(typeLien, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblType))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(creerLien)
                .addGap(16, 16, 16)
                .addComponent(SupprimerLienLabel)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(EditionLienLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(terrestre)
                    .addComponent(supprimerTerrestre, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(EditionLienLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(Aerien)
                    .addComponent(supprimerAerien, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(EditionLienLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(Maritime)
                    .addComponent(supprimerMaritime, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(30, 30, 30)
                .addComponent(enleverSelection)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(terminerEditionLien)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(erreurCreationLien)
                .addContainerGap(649, Short.MAX_VALUE))
        );

        SousMenuPanel.add(EditionLien, "card6");

        ModifierMesureSanitaire.setBackground(new java.awt.Color(51, 51, 51));
        ModifierMesureSanitaire.setPreferredSize(new java.awt.Dimension(250, 585));

        jLabel5.setForeground(new java.awt.Color(255, 255, 255));
        jLabel5.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel5.setText("MODIFIER MESURE SANITAIRE");
        jLabel5.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        ComboListeMesureSanitaire.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                ComboListeMesureSanitaireItemStateChanged(evt);
            }
        });
        ComboListeMesureSanitaire.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ComboListeMesureSanitaireActionPerformed(evt);
            }
        });

        jLabel6.setForeground(new java.awt.Color(255, 255, 255));
        jLabel6.setText("Nom mesure sanitaire");

        jLabel7.setForeground(new java.awt.Color(255, 255, 255));
        jLabel7.setText("Impact de la mesure sanitaire");

        jLabel8.setForeground(new java.awt.Color(255, 255, 255));
        jLabel8.setText("Taux transmission");

        jLabel9.setForeground(new java.awt.Color(255, 255, 255));
        jLabel9.setText("Taux guérison");

        jLabel10.setForeground(new java.awt.Color(255, 255, 255));
        jLabel10.setText("Taux déplacement terrestre");

        jLabel11.setForeground(new java.awt.Color(255, 255, 255));
        jLabel11.setText("Taux mortalité");

        jLabel12.setForeground(new java.awt.Color(255, 255, 255));
        jLabel12.setText("Taux déplacement maritime");

        jLabel13.setForeground(new java.awt.Color(255, 255, 255));
        jLabel13.setText("Taux déplacement aerien");

        jLabel14.setForeground(new java.awt.Color(255, 255, 255));
        jLabel14.setText("Taux déplacement région");

        ImpactTauxReproduction.setModel(new javax.swing.SpinnerNumberModel(Float.valueOf(0.0f), Float.valueOf(-1.0f), Float.valueOf(1.0f), Float.valueOf(0.01f)));

        ImpactTauxGuerison.setModel(new javax.swing.SpinnerNumberModel(Float.valueOf(0.0f), Float.valueOf(-1.0f), Float.valueOf(1.0f), Float.valueOf(0.01f)));

        ImpactTauxMortalite.setModel(new javax.swing.SpinnerNumberModel(Float.valueOf(0.0f), Float.valueOf(-1.0f), Float.valueOf(1.0f), Float.valueOf(0.01f)));

        ImpactTauxTerrestre.setModel(new javax.swing.SpinnerNumberModel(Float.valueOf(0.0f), Float.valueOf(-1.0f), Float.valueOf(1.0f), Float.valueOf(0.01f)));

        ImpactTauxMaritime.setModel(new javax.swing.SpinnerNumberModel(Float.valueOf(0.0f), Float.valueOf(-1.0f), Float.valueOf(1.0f), Float.valueOf(0.01f)));

        ImpactTauxAerien.setModel(new javax.swing.SpinnerNumberModel(Float.valueOf(0.0f), Float.valueOf(-1.0f), Float.valueOf(1.0f), Float.valueOf(0.01f)));

        ImpactTauxRegion.setModel(new javax.swing.SpinnerNumberModel(Float.valueOf(0.0f), Float.valueOf(-1.0f), Float.valueOf(1.0f), Float.valueOf(0.01f)));

        btnEnregistrerMesure.setBackground(new java.awt.Color(102, 102, 102));
        btnEnregistrerMesure.setForeground(new java.awt.Color(255, 255, 255));
        btnEnregistrerMesure.setText("Enregistrer");
        btnEnregistrerMesure.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEnregistrerMesureActionPerformed(evt);
            }
        });

        btnAnnulerEnregistrementMesure.setBackground(new java.awt.Color(102, 102, 102));
        btnAnnulerEnregistrementMesure.setForeground(new java.awt.Color(255, 255, 255));
        btnAnnulerEnregistrementMesure.setText("Quitter");
        btnAnnulerEnregistrementMesure.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAnnulerEnregistrementMesureActionPerformed(evt);
            }
        });

        InfoModifierMesure.setFont(new java.awt.Font("Tahoma", 0, 9)); // NOI18N
        InfoModifierMesure.setForeground(new java.awt.Color(255, 0, 0));
        InfoModifierMesure.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);

        btnRetirerMesure.setBackground(new java.awt.Color(102, 102, 102));
        btnRetirerMesure.setForeground(new java.awt.Color(255, 255, 255));
        btnRetirerMesure.setText("RETIRER MESURE");
        btnRetirerMesure.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRetirerMesureActionPerformed(evt);
            }
        });

        jLabel65.setForeground(new java.awt.Color(255, 255, 255));
        jLabel65.setText("Taux reproduction");

        ImpactTauxTransmission.setModel(new javax.swing.SpinnerNumberModel(Float.valueOf(0.0f), Float.valueOf(-1.0f), Float.valueOf(1.0f), Float.valueOf(0.01f)));

        javax.swing.GroupLayout ModifierMesureSanitaireLayout = new javax.swing.GroupLayout(ModifierMesureSanitaire);
        ModifierMesureSanitaire.setLayout(ModifierMesureSanitaireLayout);
        ModifierMesureSanitaireLayout.setHorizontalGroup(
            ModifierMesureSanitaireLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(InfoModifierMesure, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(ModifierMesureSanitaireLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(ModifierMesureSanitaireLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnEnregistrerMesure, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(ComboListeMesureSanitaire, javax.swing.GroupLayout.Alignment.TRAILING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel5, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 238, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, ModifierMesureSanitaireLayout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGroup(ModifierMesureSanitaireLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(ModifierMesureSanitaireLayout.createSequentialGroup()
                                .addGroup(ModifierMesureSanitaireLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel9)
                                    .addComponent(jLabel11)
                                    .addComponent(jLabel8)
                                    .addComponent(jLabel65))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGroup(ModifierMesureSanitaireLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(ImpactTauxMortalite, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(ImpactTauxGuerison, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(ImpactTauxReproduction, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(ImpactTauxTransmission, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(ModifierMesureSanitaireLayout.createSequentialGroup()
                                .addComponent(jLabel10)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(ImpactTauxTerrestre, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(ModifierMesureSanitaireLayout.createSequentialGroup()
                                .addComponent(jLabel12)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(ImpactTauxMaritime, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(ModifierMesureSanitaireLayout.createSequentialGroup()
                                .addGroup(ModifierMesureSanitaireLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel14)
                                    .addComponent(jLabel13))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGroup(ModifierMesureSanitaireLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(ImpactTauxAerien, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(ImpactTauxRegion, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                    .addComponent(btnAnnulerEnregistrementMesure, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(ModifierMesureSanitaireLayout.createSequentialGroup()
                        .addGroup(ModifierMesureSanitaireLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel6)
                            .addComponent(jLabel7))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(btnRetirerMesure, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        ModifierMesureSanitaireLayout.setVerticalGroup(
            ModifierMesureSanitaireLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(ModifierMesureSanitaireLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel5)
                .addGap(13, 13, 13)
                .addComponent(jLabel6)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(ComboListeMesureSanitaire, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnRetirerMesure)
                .addGap(27, 27, 27)
                .addComponent(jLabel7)
                .addGap(8, 8, 8)
                .addGroup(ModifierMesureSanitaireLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel65)
                    .addComponent(ImpactTauxReproduction, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(ModifierMesureSanitaireLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8)
                    .addComponent(ImpactTauxTransmission, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(ModifierMesureSanitaireLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel9)
                    .addComponent(ImpactTauxGuerison, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(ModifierMesureSanitaireLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel11)
                    .addComponent(ImpactTauxMortalite, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(23, 23, 23)
                .addGroup(ModifierMesureSanitaireLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel10)
                    .addComponent(ImpactTauxTerrestre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(ModifierMesureSanitaireLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel12)
                    .addComponent(ImpactTauxMaritime, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(ModifierMesureSanitaireLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel13)
                    .addComponent(ImpactTauxAerien, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(ModifierMesureSanitaireLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel14)
                    .addComponent(ImpactTauxRegion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(btnEnregistrerMesure)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnAnnulerEnregistrementMesure)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 58, Short.MAX_VALUE)
                .addComponent(InfoModifierMesure)
                .addContainerGap())
        );

        SousMenuPanel.add(ModifierMesureSanitaire, "card5");

        ModificationPaysPanel.setBackground(new java.awt.Color(51, 51, 51));
        ModificationPaysPanel.setForeground(new java.awt.Color(255, 255, 255));

        NouveauPaysLabel1.setForeground(new java.awt.Color(255, 255, 255));
        NouveauPaysLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        NouveauPaysLabel1.setText("MODIFICATION PAYS");

        ModificationTerminée.setBackground(new java.awt.Color(102, 102, 102));
        ModificationTerminée.setForeground(new java.awt.Color(255, 255, 255));
        ModificationTerminée.setText("MODIFICATION TERMINÉE");
        ModificationTerminée.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ModificationTerminéeActionPerformed(evt);
            }
        });

        nomPaysModifie.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                nomPaysModifieActionPerformed(evt);
            }
        });

        jLabel34.setForeground(new java.awt.Color(255, 255, 255));
        jLabel34.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel34.setText("Nouveau nom du pays :");

        populationModifie.setModel(new javax.swing.SpinnerNumberModel(1, 1, 2147483647, 1));

        jLabel35.setForeground(new java.awt.Color(255, 255, 255));
        jLabel35.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel35.setText("Nouvelle population :");

        erreurPaysModifie.setForeground(new java.awt.Color(255, 0, 0));

        nouvelleDivision1.setBackground(new java.awt.Color(102, 102, 102));
        nouvelleDivision1.setForeground(new java.awt.Color(255, 255, 255));
        nouvelleDivision1.setText("MODIFIER RÉGIONS");
        nouvelleDivision1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                nouvelleDivision1ActionPerformed(evt);
            }
        });

        pctSaineLabel1.setForeground(new java.awt.Color(255, 255, 255));
        pctSaineLabel1.setText("Terrestre");

        tauxTerrestreModifie.setModel(new javax.swing.SpinnerNumberModel(Float.valueOf(0.0f), Float.valueOf(0.0f), Float.valueOf(100.0f), Float.valueOf(0.01f)));

        pctInfecterLabel1.setForeground(new java.awt.Color(255, 255, 255));
        pctInfecterLabel1.setText("Maritime");

        tauxMaritimeModifie.setModel(new javax.swing.SpinnerNumberModel(Float.valueOf(0.0f), Float.valueOf(0.0f), Float.valueOf(100.0f), Float.valueOf(0.01f)));

        tauxAerienModifie.setModel(new javax.swing.SpinnerNumberModel(0.0f, 0.0f, null, 0.01f));

        pctMorteLabel1.setForeground(new java.awt.Color(255, 255, 255));
        pctMorteLabel1.setText("Aérien");

        jLabel38.setForeground(new java.awt.Color(255, 255, 255));
        jLabel38.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel38.setText("Taux de déplacement");

        tauxRegionnalModifie.setModel(new javax.swing.SpinnerNumberModel(0.0f, 0.0f, null, 0.01f));

        jLabel37.setForeground(new java.awt.Color(255, 255, 255));
        jLabel37.setText("Régionnal");

        supprimerPays.setBackground(new java.awt.Color(102, 102, 102));
        supprimerPays.setForeground(new java.awt.Color(255, 255, 255));
        supprimerPays.setText("SUPPRIMER");
        supprimerPays.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                supprimerPaysActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout ModificationPaysPanelLayout = new javax.swing.GroupLayout(ModificationPaysPanel);
        ModificationPaysPanel.setLayout(ModificationPaysPanelLayout);
        ModificationPaysPanelLayout.setHorizontalGroup(
            ModificationPaysPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(ModificationPaysPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(ModificationPaysPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel38, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(ModificationPaysPanelLayout.createSequentialGroup()
                        .addGroup(ModificationPaysPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(erreurPaysModifie, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(ModificationTerminée, javax.swing.GroupLayout.DEFAULT_SIZE, 279, Short.MAX_VALUE)
                            .addComponent(nouvelleDivision1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, ModificationPaysPanelLayout.createSequentialGroup()
                                .addGap(0, 0, Short.MAX_VALUE)
                                .addGroup(ModificationPaysPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(ModificationPaysPanelLayout.createSequentialGroup()
                                        .addGap(10, 10, 10)
                                        .addGroup(ModificationPaysPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                            .addComponent(pctInfecterLabel1)
                                            .addComponent(pctMorteLabel1)))
                                    .addComponent(pctSaineLabel1, javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jLabel37, javax.swing.GroupLayout.Alignment.TRAILING))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(ModificationPaysPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(tauxRegionnalModifie)
                                    .addComponent(tauxTerrestreModifie)
                                    .addComponent(tauxMaritimeModifie)
                                    .addComponent(tauxAerienModifie, javax.swing.GroupLayout.PREFERRED_SIZE, 79, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addComponent(NouveauPaysLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(ModificationPaysPanelLayout.createSequentialGroup()
                                .addGroup(ModificationPaysPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(jLabel34, javax.swing.GroupLayout.DEFAULT_SIZE, 143, Short.MAX_VALUE)
                                    .addComponent(jLabel35, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(ModificationPaysPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(nomPaysModifie)
                                    .addComponent(populationModifie, javax.swing.GroupLayout.PREFERRED_SIZE, 1, Short.MAX_VALUE)))
                            .addComponent(supprimerPays, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addContainerGap())))
        );
        ModificationPaysPanelLayout.setVerticalGroup(
            ModificationPaysPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(ModificationPaysPanelLayout.createSequentialGroup()
                .addGap(12, 12, 12)
                .addComponent(NouveauPaysLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(ModificationPaysPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(nomPaysModifie)
                    .addComponent(jLabel34))
                .addGap(9, 9, 9)
                .addGroup(ModificationPaysPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel35, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(populationModifie, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(jLabel38)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(ModificationPaysPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(pctSaineLabel1)
                    .addComponent(tauxTerrestreModifie, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(ModificationPaysPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(tauxMaritimeModifie, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(pctInfecterLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(ModificationPaysPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(tauxAerienModifie, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(pctMorteLabel1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(ModificationPaysPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(tauxRegionnalModifie, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel37, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(erreurPaysModifie, javax.swing.GroupLayout.PREFERRED_SIZE, 146, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(nouvelleDivision1)
                .addGap(18, 18, 18)
                .addComponent(ModificationTerminée)
                .addGap(18, 18, 18)
                .addComponent(supprimerPays)
                .addGap(463, 463, 463))
        );

        SousMenuPanel.add(ModificationPaysPanel, "card9");

        VuePaysPanel.setBackground(new java.awt.Color(51, 51, 51));
        VuePaysPanel.setForeground(new java.awt.Color(255, 255, 255));

        vuePaysLabel.setBackground(new java.awt.Color(51, 51, 51));
        vuePaysLabel.setFont(new java.awt.Font("Dialog", 0, 18)); // NOI18N
        vuePaysLabel.setForeground(new java.awt.Color(255, 255, 255));
        vuePaysLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);

        popVueLabel.setForeground(new java.awt.Color(255, 255, 255));
        popVueLabel.setText("Population:");

        popVuePays.setForeground(new java.awt.Color(255, 255, 255));

        sainsVueLabel.setForeground(new java.awt.Color(255, 255, 255));
        sainsVueLabel.setText("Sains:");

        infecterVueLabel.setForeground(new java.awt.Color(255, 255, 255));
        infecterVueLabel.setText("Infectés:");

        mortVueLabel.setForeground(new java.awt.Color(255, 255, 255));
        mortVueLabel.setText("Morts:");

        popVue.setForeground(new java.awt.Color(255, 255, 255));

        sainsVue.setForeground(new java.awt.Color(255, 255, 255));

        infecterVue.setForeground(new java.awt.Color(255, 255, 255));

        mortsVue.setForeground(new java.awt.Color(255, 255, 255));

        jLabel39.setForeground(new java.awt.Color(255, 255, 255));
        jLabel39.setText("Taux de déplacement");

        jLabel40.setForeground(new java.awt.Color(255, 255, 255));
        jLabel40.setText("Terrestre:");

        jLabel41.setForeground(new java.awt.Color(255, 255, 255));
        jLabel41.setText("Aérien:");

        jLabel42.setForeground(new java.awt.Color(255, 255, 255));
        jLabel42.setText("Maritime:");

        jLabel43.setForeground(new java.awt.Color(255, 255, 255));
        jLabel43.setText("Régionnal:");

        txTerrestreVue.setForeground(new java.awt.Color(255, 255, 255));

        txAerienVue.setForeground(new java.awt.Color(255, 255, 255));

        txMaritimeVue.setForeground(new java.awt.Color(255, 255, 255));

        txRegionnalVue.setForeground(new java.awt.Color(255, 255, 255));

        jLabel44.setForeground(new java.awt.Color(255, 255, 255));
        jLabel44.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel44.setText("Informations région pointée");

        jLabel45.setForeground(new java.awt.Color(255, 255, 255));
        jLabel45.setText("Nom:");

        nomRegionVue.setForeground(new java.awt.Color(255, 255, 255));

        jLabel46.setForeground(new java.awt.Color(255, 255, 255));
        jLabel46.setText("Population:");

        pctPopPaysVuelabel.setForeground(new java.awt.Color(255, 255, 255));
        pctPopPaysVuelabel.setText("% du pays:");

        populationRegionVue.setForeground(new java.awt.Color(255, 255, 255));

        pctPopPaysVue.setForeground(new java.awt.Color(255, 255, 255));

        jLabel47.setForeground(new java.awt.Color(255, 255, 255));
        jLabel47.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel47.setText("Sains");

        jLabel48.setForeground(new java.awt.Color(255, 255, 255));
        jLabel48.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel48.setText("Infectés");

        jLabel49.setForeground(new java.awt.Color(255, 255, 255));
        jLabel49.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel49.setText("Morts");

        pctSainsRegionVue.setForeground(new java.awt.Color(255, 255, 255));

        totalSainsRegionVue.setForeground(new java.awt.Color(255, 255, 255));
        totalSainsRegionVue.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);

        pctInfecteRegionVue.setForeground(new java.awt.Color(255, 255, 255));
        pctInfecteRegionVue.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);

        totalInfecterRegionVue.setForeground(new java.awt.Color(255, 255, 255));
        totalInfecterRegionVue.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);

        pctmortRegionVue.setForeground(new java.awt.Color(255, 255, 255));
        pctmortRegionVue.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);

        totalMortRegionVue.setForeground(new java.awt.Color(255, 255, 255));
        totalMortRegionVue.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);

        javax.swing.GroupLayout VuePaysPanelLayout = new javax.swing.GroupLayout(VuePaysPanel);
        VuePaysPanel.setLayout(VuePaysPanelLayout);
        VuePaysPanelLayout.setHorizontalGroup(
            VuePaysPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(VuePaysPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(VuePaysPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel44, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(VuePaysPanelLayout.createSequentialGroup()
                        .addGroup(VuePaysPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(VuePaysPanelLayout.createSequentialGroup()
                                .addGap(31, 31, 31)
                                .addComponent(jLabel45)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(nomRegionVue))
                            .addGroup(VuePaysPanelLayout.createSequentialGroup()
                                .addComponent(jLabel46)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(populationRegionVue))
                            .addGroup(VuePaysPanelLayout.createSequentialGroup()
                                .addGroup(VuePaysPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(sainsVueLabel)
                                    .addComponent(popVueLabel)
                                    .addComponent(infecterVueLabel)
                                    .addComponent(mortVueLabel))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(VuePaysPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(VuePaysPanelLayout.createSequentialGroup()
                                        .addComponent(popVue)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(popVuePays))
                                    .addComponent(sainsVue)
                                    .addComponent(infecterVue)
                                    .addComponent(mortsVue)))
                            .addComponent(jLabel39)
                            .addGroup(VuePaysPanelLayout.createSequentialGroup()
                                .addGroup(VuePaysPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jLabel42)
                                    .addComponent(jLabel43)
                                    .addComponent(jLabel41)
                                    .addComponent(jLabel40))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(VuePaysPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(txTerrestreVue)
                                    .addComponent(txAerienVue)
                                    .addComponent(txMaritimeVue)
                                    .addComponent(txRegionnalVue)))
                            .addGroup(VuePaysPanelLayout.createSequentialGroup()
                                .addComponent(pctPopPaysVuelabel, javax.swing.GroupLayout.PREFERRED_SIZE, 62, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(pctPopPaysVue)))
                        .addContainerGap(167, Short.MAX_VALUE))))
            .addGroup(VuePaysPanelLayout.createSequentialGroup()
                .addGroup(VuePaysPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(vuePaysLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(VuePaysPanelLayout.createSequentialGroup()
                        .addComponent(jLabel47, javax.swing.GroupLayout.PREFERRED_SIZE, 82, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(VuePaysPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(totalSainsRegionVue, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(pctSainsRegionVue, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addGroup(VuePaysPanelLayout.createSequentialGroup()
                        .addGroup(VuePaysPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(jLabel49, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel48, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 82, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(VuePaysPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(totalInfecterRegionVue, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(pctInfecteRegionVue, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(pctmortRegionVue, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(totalMortRegionVue, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addContainerGap())
        );
        VuePaysPanelLayout.setVerticalGroup(
            VuePaysPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(VuePaysPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(vuePaysLabel)
                .addGap(49, 49, 49)
                .addGroup(VuePaysPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(popVueLabel)
                    .addComponent(popVuePays)
                    .addComponent(popVue))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(VuePaysPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(sainsVueLabel)
                    .addComponent(sainsVue))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(VuePaysPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(infecterVueLabel)
                    .addComponent(infecterVue))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(VuePaysPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(mortVueLabel)
                    .addComponent(mortsVue))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel39)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(VuePaysPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel40)
                    .addComponent(txTerrestreVue))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(VuePaysPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel41)
                    .addComponent(txAerienVue))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(VuePaysPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel42)
                    .addComponent(txMaritimeVue))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(VuePaysPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel43)
                    .addComponent(txRegionnalVue))
                .addGap(18, 18, 18)
                .addComponent(jLabel44)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(VuePaysPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel45)
                    .addComponent(nomRegionVue))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(VuePaysPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel46)
                    .addComponent(populationRegionVue))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(VuePaysPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(pctPopPaysVuelabel)
                    .addComponent(pctPopPaysVue))
                .addGap(18, 18, 18)
                .addGroup(VuePaysPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel47, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(VuePaysPanelLayout.createSequentialGroup()
                        .addComponent(pctSainsRegionVue)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(totalSainsRegionVue)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(VuePaysPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(VuePaysPanelLayout.createSequentialGroup()
                        .addComponent(pctInfecteRegionVue)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(totalInfecterRegionVue))
                    .addComponent(jLabel48, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(VuePaysPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(VuePaysPanelLayout.createSequentialGroup()
                        .addComponent(pctmortRegionVue)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(totalMortRegionVue, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jLabel49, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(493, Short.MAX_VALUE))
        );

        SousMenuPanel.add(VuePaysPanel, "card11");

        graphMonde.setBorder(javax.swing.BorderFactory.createTitledBorder("Statistiques du monde"));
        graphMonde.setLayout(new java.awt.BorderLayout());

        graphStatistique.setBorder(javax.swing.BorderFactory.createTitledBorder("Statistiques du pays"));
        graphStatistique.setLayout(new java.awt.BorderLayout());

        javax.swing.GroupLayout statPanelLayout = new javax.swing.GroupLayout(statPanel);
        statPanel.setLayout(statPanelLayout);
        statPanelLayout.setHorizontalGroup(
            statPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(statPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(statPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(graphMonde, javax.swing.GroupLayout.DEFAULT_SIZE, 271, Short.MAX_VALUE)
                    .addComponent(graphStatistique, javax.swing.GroupLayout.DEFAULT_SIZE, 271, Short.MAX_VALUE))
                .addContainerGap())
        );
        statPanelLayout.setVerticalGroup(
            statPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(statPanelLayout.createSequentialGroup()
                .addGap(39, 39, 39)
                .addComponent(graphMonde, javax.swing.GroupLayout.PREFERRED_SIZE, 270, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(graphStatistique, javax.swing.GroupLayout.PREFERRED_SIZE, 252, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(429, Short.MAX_VALUE))
        );

        SousMenuPanel.add(statPanel, "card14");

        drawingPanel.setBackground(new java.awt.Color(204, 204, 204));
        drawingPanel.setBorder(javax.swing.BorderFactory.createEtchedBorder(new java.awt.Color(51, 51, 51), null));
        drawingPanel.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseDragged(java.awt.event.MouseEvent evt) {
                drawingPanelMouseDragged(evt);
            }
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                drawingPanelMouseMoved(evt);
            }
        });
        drawingPanel.addMouseWheelListener(new java.awt.event.MouseWheelListener() {
            public void mouseWheelMoved(java.awt.event.MouseWheelEvent evt) {
                drawingPanelMouseWheelMoved(evt);
            }
        });
        drawingPanel.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                drawingPanelMouseClicked(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                drawingPanelMousePressed(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                drawingPanelMouseReleased(evt);
            }
        });

        erreurLabel.setForeground(new java.awt.Color(255, 0, 51));
        erreurLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);

        InfosPaysPanel.setBackground(new java.awt.Color(204, 204, 204));

        MortText.setEditable(false);
        MortText.setBackground(new java.awt.Color(51, 51, 51));
        MortText.setForeground(new java.awt.Color(255, 255, 255));

        Mort.setText("Morts");

        populationInfoLabel.setText("Population");

        populationText.setEditable(false);
        populationText.setBackground(new java.awt.Color(51, 51, 51));
        populationText.setForeground(new java.awt.Color(255, 255, 255));

        NomPays.setText("Nom du pays");

        InfecterText.setEditable(false);
        InfecterText.setBackground(new java.awt.Color(51, 51, 51));
        InfecterText.setForeground(new java.awt.Color(255, 255, 255));

        SainsText.setEditable(false);
        SainsText.setBackground(new java.awt.Color(51, 51, 51));
        SainsText.setForeground(new java.awt.Color(255, 255, 255));
        SainsText.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                SainsTextActionPerformed(evt);
            }
        });

        NomPaysText.setEditable(false);
        NomPaysText.setBackground(new java.awt.Color(51, 51, 51));
        NomPaysText.setForeground(new java.awt.Color(255, 255, 255));

        Infecter.setText("Infectés");

        Sains.setText("Sains");

        javax.swing.GroupLayout InfosPaysPanelLayout = new javax.swing.GroupLayout(InfosPaysPanel);
        InfosPaysPanel.setLayout(InfosPaysPanelLayout);
        InfosPaysPanelLayout.setHorizontalGroup(
            InfosPaysPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(InfosPaysPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(InfosPaysPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(InfosPaysPanelLayout.createSequentialGroup()
                        .addGap(89, 89, 89)
                        .addGroup(InfosPaysPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(Infecter)
                            .addComponent(Sains)
                            .addComponent(Mort))
                        .addGap(18, 18, 18)
                        .addGroup(InfosPaysPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(InfecterText, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(MortText)
                            .addComponent(SainsText, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 51, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, InfosPaysPanelLayout.createSequentialGroup()
                        .addComponent(NomPays)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(NomPaysText, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, InfosPaysPanelLayout.createSequentialGroup()
                        .addComponent(populationInfoLabel)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(populationText, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        InfosPaysPanelLayout.setVerticalGroup(
            InfosPaysPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(InfosPaysPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(InfosPaysPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(NomPaysText)
                    .addComponent(NomPays, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(InfosPaysPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(populationText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(populationInfoLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(InfosPaysPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(SainsText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(Sains, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(InfosPaysPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(InfecterText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(Infecter, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(InfosPaysPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(MortText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(Mort, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        jButton1.setBackground(new java.awt.Color(102, 102, 102));
        jButton1.setFont(new java.awt.Font("Dialog", 0, 8)); // NOI18N
        jButton1.setForeground(new java.awt.Color(255, 255, 255));
        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/earth.png"))); // NOI18N
        jButton1.setText("VUE GLOBALE");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        StatsPanel.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        StatsPanel.setLayout(new java.awt.BorderLayout());

        javax.swing.GroupLayout drawingPanelLayout = new javax.swing.GroupLayout(drawingPanel);
        drawingPanel.setLayout(drawingPanelLayout);
        drawingPanelLayout.setHorizontalGroup(
            drawingPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(drawingPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(drawingPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(erreurLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(drawingPanelLayout.createSequentialGroup()
                        .addComponent(jButton1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 208, Short.MAX_VALUE)
                        .addComponent(InfosPaysPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(StatsPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        drawingPanelLayout.setVerticalGroup(
            drawingPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(drawingPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(drawingPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(InfosPaysPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(erreurLabel)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(StatsPanel, javax.swing.GroupLayout.PREFERRED_SIZE, 171, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        ToolBarMenu.setBackground(new java.awt.Color(51, 51, 51));
        ToolBarMenu.setBorder(javax.swing.BorderFactory.createEtchedBorder(new java.awt.Color(51, 51, 51), null));
        ToolBarMenu.setRollover(true);

        btnFile.setBackground(new java.awt.Color(51, 51, 51));
        btnFile.setForeground(new java.awt.Color(255, 255, 255));
        btnFile.setText("File");
        btnFile.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        btnFile.setFocusable(false);
        btnFile.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnFile.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        ToolBarMenu.add(btnFile);

        Save.setBackground(new java.awt.Color(51, 51, 51));
        Save.setForeground(new java.awt.Color(255, 255, 255));
        Save.setText("Save");
        Save.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        Save.setFocusable(false);
        Save.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        Save.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        Save.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                SaveActionPerformed(evt);
            }
        });
        ToolBarMenu.add(Save);

        Load.setBackground(new java.awt.Color(51, 51, 51));
        Load.setForeground(new java.awt.Color(255, 255, 255));
        Load.setText("Load");
        Load.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        Load.setFocusable(false);
        Load.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        Load.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        Load.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                LoadActionPerformed(evt);
            }
        });
        ToolBarMenu.add(Load);

        Undo.setBackground(new java.awt.Color(51, 51, 51));
        Undo.setForeground(new java.awt.Color(255, 255, 255));
        Undo.setText("Undo");
        Undo.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        Undo.setFocusable(false);
        Undo.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        Undo.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        Undo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                UndoActionPerformed(evt);
            }
        });
        ToolBarMenu.add(Undo);

        Redo.setBackground(new java.awt.Color(51, 51, 51));
        Redo.setForeground(new java.awt.Color(255, 255, 255));
        Redo.setText("Redo");
        Redo.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        Redo.setFocusable(false);
        Redo.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        Redo.setPreferredSize(new java.awt.Dimension(40, 20));
        Redo.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        Redo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                RedoActionPerformed(evt);
            }
        });
        ToolBarMenu.add(Redo);

        exportJpeg.setBackground(new java.awt.Color(51, 51, 51));
        exportJpeg.setForeground(new java.awt.Color(255, 255, 255));
        exportJpeg.setText("Export");
        exportJpeg.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        exportJpeg.setFocusable(false);
        exportJpeg.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        exportJpeg.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        exportJpeg.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                exportJpegActionPerformed(evt);
            }
        });
        ToolBarMenu.add(exportJpeg);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(MenuBouton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(drawingPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(SousMenuPanel, javax.swing.GroupLayout.PREFERRED_SIZE, 295, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(ToolBarMenu, javax.swing.GroupLayout.PREFERRED_SIZE, 285, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(ToolBarMenu, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(7, 7, 7)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(SousMenuPanel)
                    .addComponent(MenuBouton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(drawingPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        getAccessibleContext().setAccessibleDescription("");

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void drawingPanelMouseMoved(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_drawingPanelMouseMoved
        // TODO add your handling code here:
        
        if (zoomFactor == 1)
        {
            InfosPaysPanel.setEnabled(true);
            updateInfoPays(evt.getX(), evt.getY());
        }
        else
        {
            InfosPaysPanel.setEnabled(false);
            NomPaysText.setText("");
            populationText.setText("");
            SainsText.setText("");
            InfecterText.setText("");
            MortText.setText("");
        }
        
        if (modeSimudemie.getMode()==SimudemieMode.simudemieMode.VUEPAYS || modeSimudemie.getMode()==SimudemieMode.simudemieMode.STATISTIQUE)
        {
            updateInfoVueRegion(evt);
        }
        
        
    }//GEN-LAST:event_drawingPanelMouseMoved

    private void drawingPanelMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_drawingPanelMouseClicked
        // TODO add your handling code here:

        if (modeSimudemie.getMode() == SimudemieMode.simudemieMode.CREATIONPAYS) {
            zoomFactor = 1.00;
            addPoint(evt.getX(), evt.getY());
        } else if (modeSimudemie.getMode() == SimudemieMode.simudemieMode.CREATIONLIEN) {
            drawingPanel.repaint();
            AjoutLienMode(evt);
        } else if (modeSimudemie.getMode()== SimudemieMode.simudemieMode.DIVISIONPAYS) {
            drawingPanel.repaint();
            DivisionPaysMode(evt);
        }
        else if (modeSimudemie.getMode()== SimudemieMode.simudemieMode.MODIFICATIONPAYS) {
            drawingPanel.repaint();
            paysSelectionner(evt);
        }
        else if (modeSimudemie.getMode()== SimudemieMode.simudemieMode.CREATIONPAYS_REG){
            creationPaysReg(evt);
        }
        else if (modeSimudemie.getMode()== SimudemieMode.simudemieMode.MESURESANITAIRE){
            AppliquerMesure(evt);
        }
        else if (modeSimudemie.getMode()== SimudemieMode.simudemieMode.SIMULATION)
        {
            if  (controller.getCarteDuMonde().estDansPays(evt.getX(), evt.getY()) != null)
            {
                paysDiv = controller.getCarteDuMonde().estDansPays(evt.getX(), evt.getY());
            }
            else
            {
                paysDiv = null;
            }
            liveStatsMode();
            liveStatsModePays();
            repaint();
            revalidate();
        }
        else if ((modeSimudemie.getMode()== SimudemieMode.simudemieMode.CARTEDUMONDE || modeSimudemie.getMode()== SimudemieMode.simudemieMode.VUEPAYS )&& zoomFactor == 1) //|| modeSimudemie.getMode()== SimudemieMode.simudemieMode.STATISTIQUE
        {
            updateVuePays(evt);
        }
        else if (modeSimudemie.getMode()== SimudemieMode.simudemieMode.STATISTIQUE)
        {
            if (paysVue == controller.getCarteDuMonde().estDansPays(evt.getX(), evt.getY()) && paysVue != null){
                selectionRegion = controller.estDansRegion(paysVue, evt.getX(), evt.getY());
            }
            else{
                selectionRegion = null;
                paysVue = controller.getCarteDuMonde().estDansPays(evt.getX(), evt.getY());
            }
            
            if (paysVue != null){
                updateInfoVueRegion(evt);
            }
            updateVuePays(evt);
            updateStatistiques();
            repaint();
        }
    }//GEN-LAST:event_drawingPanelMouseClicked

    private void setTableRegionOnClick(Pays pays){
        if (!pays.getNomPays().equals(selectedPays.toString())){
            model.setNumRows(0);
            numdata.clear();
            for (Region reg: pays.getListeRegion()){
                model.addRow(new Object[]{reg.getNomRegion(), reg.getPourcentagePopulation() * 100, reg.getPourcentageSaine() * 100,
                    reg.getPourcentageInfecte() * 100, reg.getPourcentageMort() * 100});
                formeRegion.add(reg.getForme().getListePoints());
            }
        }
        drawingPanel.repaint();
    }

    private void paysSelectionner(java.awt.event.MouseEvent evt){
            Pays pays = controller.getCarteDuMonde().estDansPays(evt.getX(), evt.getY());
            if (pays != null)
                paysModifie = pays;
                nomPaysModifie.setText(paysModifie.getNomPays());
                populationModifie.setValue(paysModifie.getPopulation());
                tauxTerrestreModifie.setValue(paysModifie.getTxDepTerrestre());
                tauxMaritimeModifie.setValue(paysModifie.getTxDepMaritime());
                tauxAerienModifie.setValue(paysModifie.getTxDepAerien());
                tauxRegionnalModifie.setValue(paysModifie.getTxDepRegion());
    }
    
    private void DessinFormeRegion(java.awt.event.MouseEvent evt){
        addPointOnPays(evt.getX(), evt.getY());
        drawingPanel.repaint();
    }

    private void DivisionPaysMode(java.awt.event.MouseEvent evt){
        paysDiv = controller.getCarteDuMonde().estDansPays(evt.getX(), evt.getY());
        if (paysDiv != null){
            if (paysDiv.getNomPays().equals(selectedPays.getText())){
                if (formeRegion.size() <= model.getRowCount()){
                    DessinFormeRegion(evt);
                }
            }
            else {
                selectedPays.setText(paysDiv.getNomPays());
                formeRegion.clear();
                formeTemporaire.clear();
                setTableRegionOnClick(paysDiv);
            }      
        }
        else {
            formeRegion.clear();
            model.setNumRows(0);
            selectedPays.setText("");
            erreurDivision.setText("");
            formeTemporaire.clear();
        }
    }
    private void AjoutLienMode(java.awt.event.MouseEvent evt){
        Pays pays = controller.getCarteDuMonde().estDansPays(evt.getX(), evt.getY());
        if (pays != null) {
            if (nomPaysA.getText().equals("")) {
                nomPaysA.setText(pays.getNomPays());
                selectionPaysA = pays;
                pointLienA = new Point(evt.getX(), evt.getY());
            } else {
                nomPaysB.setText(pays.getNomPays());
                selectionPaysB = pays;
                pointLienB = new Point(evt.getX(), evt.getY());
                activationBoutonSupprimerLien();
            }
        } else {
            selectionPaysA = null;
            selectionPaysB = null;
            desactiverBoutonSupprimerLien();
            nomPaysA.setText("");
            nomPaysB.setText("");
            erreurInfosPays.setText("");
        }
    }
    
    private void AppliquerMesure(java.awt.event.MouseEvent evt){
        Pays pays = controller.getCarteDuMonde().estDansPays(evt.getX(), evt.getY());
        if (pays != null) {
            AppliquerMesureListePays.setSelectedItem(pays.getNomPays());
            
        } else {

        }
    }
    
    
    private void desactiverBoutonSupprimerLien()
    {
        supprimerTerrestre.setEnabled(false);
        supprimerAerien.setEnabled(false);
        supprimerMaritime.setEnabled(false);
    }
    
    private void activationBoutonCarteDuMonde(boolean p_activer)
    {
        if (p_activer)
        {
            ajouterPays.setEnabled(true);
            nouvelleDivision.setEnabled(true);
            nouveauLien.setEnabled(true);
            ModifierPays.setEnabled(true);
            CreerMaladieBouton.setEnabled(true);
            ajouterMesureSanitaire.setEnabled(true);
            btnMesureSanitaire.setEnabled(true);
            btnAppliquerMesureSanitaire.setEnabled(true);
            btnAppliquerMaladie.setEnabled(true);
            btnModifierMaladie.setEnabled(true);
        }
        else
        {
            ajouterPays.setEnabled(false);
            nouvelleDivision.setEnabled(false);
            nouveauLien.setEnabled(false);
            ModifierPays.setEnabled(false);
            CreerMaladieBouton.setEnabled(false);
            ajouterMesureSanitaire.setEnabled(false);
            btnMesureSanitaire.setEnabled(false);
            btnAppliquerMesureSanitaire.setEnabled(false);
            btnAppliquerMaladie.setEnabled(false);
            btnModifierMaladie.setEnabled(false);
        }
    }

    private void activationBoutonSupprimerLien()
    {
        if (controller.getCarteDuMonde().lienExiste(selectionPaysA, selectionPaysB, Lien.typeLien.TERRESTRE))
            supprimerTerrestre.setEnabled(true);
        else
            supprimerTerrestre.setEnabled(false);
        
        if (controller.getCarteDuMonde().lienExiste(selectionPaysA, selectionPaysB, Lien.typeLien.AERIEN))
            supprimerAerien.setEnabled(true);
        else
            supprimerAerien.setEnabled(false);
        
        if (controller.getCarteDuMonde().lienExiste(selectionPaysA, selectionPaysB, Lien.typeLien.MARITIME))
            supprimerMaritime.setEnabled(true);
        else
            supprimerMaritime.setEnabled(false);
    }
    
    private boolean checkFormeRegion(){
        if (formeRegion.size() == model.getRowCount()){
            return true;
        }
        return false;
    }
     
    private boolean checkInfoRegion(){
        numdata.clear();
        ArrayList<String> nomsRegion = new ArrayList<String>();
        float sommePourcPop = 0;
        for (int rowCount = 0; rowCount < model.getRowCount(); rowCount++){
            ArrayList<String> newRegion = new ArrayList<String>();
            float sommePource = 0;
            for (int colCount = 0; colCount < model.getColumnCount(); colCount ++){
                if (model.getValueAt(rowCount, colCount) == null){
                    return false;
                }
                if (colCount == 0){
                    if (nomsRegion.contains(model.getValueAt(rowCount, colCount).toString())){
                        return false;
                    }
                    nomsRegion.add(model.getValueAt(rowCount, colCount).toString());
                }
                else if (colCount == 1){
                    if (Float.parseFloat(String.valueOf(model.getValueAt(rowCount, colCount).toString())) == 0){
                        return false;
                    }
                    sommePourcPop += Float.parseFloat(String.valueOf(model.getValueAt(rowCount, colCount).toString()));
                }
                else if (colCount > 1){
                    sommePource += Float.parseFloat(String.valueOf(model.getValueAt(rowCount, colCount).toString()));
                }
                newRegion.add(model.getValueAt(rowCount, colCount).toString());
            }
            if (sommePource != 100){
                return false;
            }
            numdata.add(newRegion);
        }
        if (model.getRowCount() != formeRegion.size()){
            return false;
        }
        return sommePourcPop == 100;
    }

    private void SainsTextActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_SainsTextActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_SainsTextActionPerformed

    private void SaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_SaveActionPerformed
        // TODO add your handling code here:
        enregistrer();
    }//GEN-LAST:event_SaveActionPerformed

    private void LoadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_LoadActionPerformed
        // TODO add your handling code here:
        charger();
        numeroJour.setText(String.valueOf(controller.getJourSimulation()));
        plusDeMalade.setText("");
        controller.enregistreAction();
    }//GEN-LAST:event_LoadActionPerformed

    private void UndoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_UndoActionPerformed
        // TODO add your handling code here:
        controller.undo();
        numeroJour.setText(String.valueOf(controller.getJourSimulation()));
        liveStatsModePays();
        liveStatsMode();
        repaint();
        revalidate();
    }//GEN-LAST:event_UndoActionPerformed

    private void RedoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_RedoActionPerformed
        // TODO add your handling code here:
        controller.redo();
        numeroJour.setText(String.valueOf(controller.getJourSimulation()));
        liveStatsModePays();
        liveStatsMode();
        repaint();
        revalidate();
    }//GEN-LAST:event_RedoActionPerformed

    private void exportJpegActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_exportJpegActionPerformed
        // TODO add your handling code here:
        
        UIManager.put("FileChooser.saveButtonText","Export");
        JFrame parentFrame = new JFrame();
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setDialogTitle("Specify a file to export jpeg");
        
        int userSelection = fileChooser.showSaveDialog(parentFrame);
        
        if (userSelection == JFileChooser.APPROVE_OPTION) {
            File fileToSave = fileChooser.getSelectedFile();
            InfosPaysPanel.setVisible(false);
            BufferedImage image = new BufferedImage(drawingPanel.getWidth(), drawingPanel.getHeight(), BufferedImage.TYPE_INT_RGB);
            Graphics2D g = image.createGraphics();

            drawingPanel.printAll(g);
            g.dispose();
            try {
                ImageIO.write(image, "jpg", new File(fileToSave.getAbsolutePath() + ".jpg"));
                } catch (IOException e)
                {
                e.printStackTrace();
                }
            InfosPaysPanel.setVisible(true);
        }
    }//GEN-LAST:event_exportJpegActionPerformed

    private void enleverSelectionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_enleverSelectionActionPerformed
        // TODO add your handling code here:
        selectionPaysA = null;
        selectionPaysB = null;
        nomPaysA.setText("");
        nomPaysB.setText("");
        erreurCreationLien.setText("");
    }//GEN-LAST:event_enleverSelectionActionPerformed

    private void terminerEditionLienActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_terminerEditionLienActionPerformed
        // TODO add your handling code here:
        changerPanelSousMenu(BienvenuePanel);
        pointLienA = null;
        pointLienB = null;
        repaint();
        nomPaysA.setText("");
        nomPaysB.setText("");
        erreurCreationLien.setText("");
        modeSimudemie.setMode(SimudemieMode.simudemieMode.CARTEDUMONDE);
    }//GEN-LAST:event_terminerEditionLienActionPerformed

    private void creerLienActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_creerLienActionPerformed
        // TODO add your handling code here:
        if (nomPaysA.getText().equals("") || nomPaysB.getText().equals("")){
            erreurCreationLien.setText("Veuillez cliquer sur les 2 pays à lier");
        }
        else if (nomPaysA.getText().equals(nomPaysB.getText())){
            erreurCreationLien.setText("impossible de lier le même pays");
        }
        else {
            Pays A = null;
            Pays B = null;
            for (Pays p: controller.getListePays()){
                if (p.getNomPays().equals(nomPaysA.getText())){
                    A = p;
                }
                if (p.getNomPays().equals(nomPaysB.getText())){
                    B = p;
                }
            }
            if (controller.getCarteDuMonde().lienExiste(A, B, controller.getCarteDuMonde().strToTypeLien(Objects.requireNonNull(typeLien.getSelectedItem()).toString()))){
                erreurCreationLien.setText("le lien existe déjà");
            }
            else {
                Lien.typeLien type = controller.getCarteDuMonde().strToTypeLien(Objects.requireNonNull(typeLien.getSelectedItem()).toString());
                controller.getCarteDuMonde().creerLien(A, B, type, pointLienA, pointLienB);
                controller.enregistreAction();
                nomPaysA.setText("");
                nomPaysB.setText("");
                desactiverBoutonSupprimerLien();
                selectionPaysA = null;
                selectionPaysB = null;
            }
        }
        drawingPanel.repaint();
    }//GEN-LAST:event_creerLienActionPerformed

    private void btnAnnulerEnregistrementMesureActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAnnulerEnregistrementMesureActionPerformed
        // TODO add your handling code here:
        changerPanelSousMenu(BienvenuePanel);
        ComboListeMesureSanitaire.removeAllItems();
        modeSimudemie.setMode(SimudemieMode.simudemieMode.CARTEDUMONDE);
    }//GEN-LAST:event_btnAnnulerEnregistrementMesureActionPerformed

    private void btnEnregistrerMesureActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEnregistrerMesureActionPerformed
        // TODO add your handling code here:
        for (MesureSanitaire mesure :controller.getlisteMesureSanitaire()){
            if (mesure.getNomMesure() == ComboListeMesureSanitaire.getSelectedItem().toString())
            {
                controller.modifierMesureSanitaire(ComboListeMesureSanitaire.getSelectedItem().toString(),
                    (float)ImpactTauxReproduction.getValue(),   
                    (float)ImpactTauxTransmission.getValue(),
                    (float)ImpactTauxMortalite.getValue(),
                    (float)ImpactTauxGuerison.getValue(),
                    (float)ImpactTauxTerrestre.getValue(),
                    (float)ImpactTauxAerien.getValue(),
                    (float)ImpactTauxMaritime.getValue(),
                    (float)ImpactTauxRegion.getValue());
                controller.enregistreAction();
            }
        }
    }//GEN-LAST:event_btnEnregistrerMesureActionPerformed

    private void ComboListeMesureSanitaireActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ComboListeMesureSanitaireActionPerformed
        // TODO add your handling code here:
        
    }//GEN-LAST:event_ComboListeMesureSanitaireActionPerformed

    private void AnnulerCreationMesureActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_AnnulerCreationMesureActionPerformed
        // TODO add your handling code here:
        changerPanelSousMenu(BienvenuePanel);
        modeSimudemie.setMode(SimudemieMode.simudemieMode.CARTEDUMONDE);
        clearCreerMesure();
    }//GEN-LAST:event_AnnulerCreationMesureActionPerformed

    private void CreerMesureActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CreerMesureActionPerformed
        // TODO add your handling code here:

        drawingPanel.repaint();
        if (creerMesure()){
            InfoAjouterMesure.setText("Mesure créer avec succès");
            drawingPanel.repaint();
            controller.enregistreAction();
            wait(200);
            clearCreerMesure();
        }
    }//GEN-LAST:event_CreerMesureActionPerformed

    private void effLigneActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_effLigneActionPerformed
        // TODO add your handling code here:
        if (model.getRowCount() > 1){
            if (model.getRowCount() == numdata.size()){
                model.setNumRows(model.getRowCount() - 1);
                numdata.remove(model.getRowCount() - 1);
            }
            
            else {
                model.setNumRows(model.getRowCount() -1);
            }
        }
        if (!formeTemporaire.isEmpty()){
            formeTemporaire.clear();
            drawingPanel.repaint();
        }
        else if (formeRegion.size() > 1){
            formeRegion.remove(formeRegion.get(formeRegion.size() - 1));
            drawingPanel.repaint();
        }
    }//GEN-LAST:event_effLigneActionPerformed

    private void ajoutLigneActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ajoutLigneActionPerformed
        // TODO add your handling code here:
        if (!selectedPays.getText().equals("")){
            model.setNumRows(model.getRowCount() + 1);
        }
    }//GEN-LAST:event_ajoutLigneActionPerformed

    private void enleverSelectionRegionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_enleverSelectionRegionActionPerformed
        // TODO add your handling code here:
        model.setNumRows(0);
        selectedPays.setText("");
        erreurDivision.setText("");
        drawingPanel.repaint();
    }//GEN-LAST:event_enleverSelectionRegionActionPerformed

    private void annulerDivisionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_annulerDivisionActionPerformed
        // TODO add your handling code here:
        model.setNumRows(0);
        for (Pays p: controller.getListePays()){
            if (p.getListeRegion().size() == 1){
                p.getListeRegion().get(0).setForme(new FormeIrreguliere(p.getForme().getListePoints()));
            }
        }
        selectedPays.setText("");
        erreurDivision.setText("");
        paysDiv = null;
        formeRegion.clear();
        formeTemporaire.clear();
        this.repaint();
        changerPanelSousMenu(BienvenuePanel);
        modeSimudemie.setMode(SimudemieMode.simudemieMode.CARTEDUMONDE);
    }//GEN-LAST:event_annulerDivisionActionPerformed

    private void diviserActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_diviserActionPerformed
        // TODO add your handling code here:
        if (paysDiv == null){
            erreurDivision.setText("Aucun pays selectionné");
        }
        else if (paysDiv != null){
            if (!formeTemporaire.isEmpty()){
                formeRegion.add(new ArrayList<Point>(formeTemporaire));
                formeTemporaire.clear();
            }
            for (int indice = 0; indice < controller.getCarteDuMonde().getListePays().size(); ++indice){
                if (controller.getCarteDuMonde().getListePays().get(indice) == paysDiv){
                    if (checkInfoRegion() && checkFormeRegion()){
                        controller.getCarteDuMonde().getListePays().get(indice).supprimerRegions();
                        if (model.getRowCount() == 1){
                            for (ArrayList<String> infoReg: numdata){
                                controller.getCarteDuMonde().getListePays().get(indice).creerRegion(infoReg.get(0),
                                    Float.parseFloat(infoReg.get(1))/100, Float.parseFloat(infoReg.get(2))/100,
                                    Float.parseFloat(infoReg.get(3))/100, Float.parseFloat(infoReg.get(4))/100,
                                    new FormeIrreguliere(paysDiv.getForme().getListePoints()));
                                break;
                            }
                            controller.enregistreAction();
                        }
                        else{
                            for (ArrayList<String> infoReg: numdata)
                            {
                                controller.getCarteDuMonde().getListePays().get(indice).creerRegion(infoReg.get(0),
                                    Float.parseFloat(infoReg.get(1))/100, Float.parseFloat(infoReg.get(2))/100,
                                    Float.parseFloat(infoReg.get(3))/100, Float.parseFloat(infoReg.get(4))/100,
                                    new FormeIrreguliere(formeRegion.get(numdata.indexOf(infoReg))));
                            }
                            controller.enregistreAction();
                        }
                        annulerDivisionActionPerformed(evt);
                    }
                    else{
                        erreurDivision.setText("Une ou plusieurs régions invalide(s)");
                    }
                }
            }
        }
    }//GEN-LAST:event_diviserActionPerformed

    private void btnOKMaladieActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnOKMaladieActionPerformed
        if (txtNomMaladie.getText().equals("")){
            InfoMaladie.setText("NOM MALADIE INVALIDE");
            drawingPanel.repaint();
        }
        else
        {
            controller.creerMaladie(txtNomMaladie.getText(),
                (float)NumericTauxReproduction.getValue(),
                (float)NumericTauxContagion.getValue(),
                (float)NumericTauxGuerison.getValue(),
                (float)NumericTauxMortalite.getValue());
            changerPanelSousMenu(BienvenuePanel);
            modeSimudemie.setMode(SimudemieMode.simudemieMode.CARTEDUMONDE);
            controller.enregistreAction();
        }
    }//GEN-LAST:event_btnOKMaladieActionPerformed

    private void AnnulerCreerMaladieActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_AnnulerCreerMaladieActionPerformed
        // TODO add your handling code here:

        changerPanelSousMenu(BienvenuePanel);
        modeSimudemie.setMode(SimudemieMode.simudemieMode.CARTEDUMONDE);
    }//GEN-LAST:event_AnnulerCreerMaladieActionPerformed

    private void creerPaysButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_creerPaysButtonActionPerformed
        // TODO add your handling code here:

        if(validerInfosPays())
        {
                controller.creerPays((int)population.getValue(),
                nomPays.getText(),
                new FormeIrreguliere(getFormeTemporaire()),
                (float)tauxMaritime.getValue(),
                (float)tauxTerrestre.getValue(),
                (float)tauxAerien.getValue(),
                (float)tauxRegionnal.getValue(),
                (Float.parseFloat(pctSain.getValue().toString())/100f),
                (Float.parseFloat(pctInfecter.getValue().toString())/100f),
                (Float.parseFloat(pctMort.getValue().toString())/100f));

            controller.creationAutomatiqueLiensTerrestre(controller.getPays(nomPays.getText()));
            clearInfoAjouterPays();
            changerPanelSousMenu(BienvenuePanel);
            modeSimudemie.setMode(SimudemieMode.simudemieMode.CARTEDUMONDE);
            getFormeTemporaire().clear();
            drawingPanel.repaint();
            controller.enregistreAction();
        }
    }//GEN-LAST:event_creerPaysButtonActionPerformed

    private void annulerDernierPointActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_annulerDernierPointActionPerformed
        // TODO add your handling code here:

        annulerDernierPoint();
    }//GEN-LAST:event_annulerDernierPointActionPerformed

    private void AnnulerNouveauPaysActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_AnnulerNouveauPaysActionPerformed
        // TODO add your handling code here:
        clearInfoAjouterPays();
        changerPanelSousMenu(BienvenuePanel);
        getFormeTemporaire().clear();
        repaint();
        modeSimudemie.setMode(SimudemieMode.simudemieMode.CARTEDUMONDE);
    }//GEN-LAST:event_AnnulerNouveauPaysActionPerformed

    private void nouvelleDivisionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_nouvelleDivisionActionPerformed
        // TODO add your handling code here
        zoomFactor = 1.00;
        annulerDivisionActionPerformed(evt);
        changerPanelSousMenu(DiviserPays);
        modeSimudemie.setMode(SimudemieMode.simudemieMode.DIVISIONPAYS);
    }//GEN-LAST:event_nouvelleDivisionActionPerformed

    private void nouveauLienActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_nouveauLienActionPerformed
        // TODO add your handling code here:
        zoomFactor = 1.00;
        terminerEditionLienActionPerformed(evt);
        changerPanelSousMenu(EditionLien);
        modeSimudemie.setMode(SimudemieMode.simudemieMode.CREATIONLIEN);
    }//GEN-LAST:event_nouveauLienActionPerformed

    private void ajouterMesureSanitaireActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ajouterMesureSanitaireActionPerformed
        // TODO add your handling code here:
        clearCreerMesure();
        changerPanelSousMenu(AjouterMesureSanitaire);
        modeSimudemie.setMode(SimudemieMode.simudemieMode.MESURESANITAIRE);
    }//GEN-LAST:event_ajouterMesureSanitaireActionPerformed

    private void btnMesureSanitaireActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnMesureSanitaireActionPerformed
        // TODO add your handling code here:
        changerPanelSousMenu(ModifierMesureSanitaire);
        modeSimudemie.setMode(SimudemieMode.simudemieMode.MESURESANITAIRE);
        ComboListeMesureSanitaire.removeAllItems();
        for (MesureSanitaire mesure :controller.getlisteMesureSanitaire()){
            ComboListeMesureSanitaire.addItem(mesure.getNomMesure());
        }
        rafraichirModifierMesure();
    }//GEN-LAST:event_btnMesureSanitaireActionPerformed

    private void CreerMaladieBoutonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CreerMaladieBoutonActionPerformed
        // TODO add your handling code here:
        zoomFactor = 1.00;
        changerPanelSousMenu(CreerMaladie);
        modeSimudemie.setMode(SimudemieMode.simudemieMode.MALADIE);
        txtNomMaladie.setText("");
        NumericTauxContagion.setValue(0.0f);
        NumericTauxGuerison.setValue(0.0f);
        NumericTauxMortalite.setValue(0.0f);
        NumericTauxReproduction.setValue(0.0f);
    }//GEN-LAST:event_CreerMaladieBoutonActionPerformed

    private void ajouterPaysActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ajouterPaysActionPerformed
        // TODO add your handling code here:
        zoomFactor = 1.00;
        changerPanelSousMenu(AjouterPays);
        modeSimudemie.setMode(SimudemieMode.simudemieMode.CREATIONPAYS);
        
    }//GEN-LAST:event_ajouterPaysActionPerformed

    private void arreterSimulationActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_arreterSimulationActionPerformed
        // TODO add your handling code here:
        simulationEnCours = false;
    }//GEN-LAST:event_arreterSimulationActionPerformed
    
    private void liveStatsMode(){
        DefaultPieDataset dataset = new DefaultPieDataset();
        ArrayList<Float> stats = controller.getCarteDuMonde().getStatsCarte();
        dataset.setValue("Infectés", stats.get(0));
        dataset.setValue("Sains", stats.get(1));
        dataset.setValue("Morts", stats.get(2));
        JFreeChart chart = ChartFactory.createPieChart(      
         "",   // chart title 
         dataset,          // data    
         true,             // include legend   
         false, 
         false);
        PiePlot piePlot = (PiePlot)chart.getPlot();
        piePlot.setSectionPaint("Infectés", Color.YELLOW);
        piePlot.setSectionPaint("Sains", Color.GREEN);
        piePlot.setSectionPaint("Morts", Color.RED);
        piePlot.setLabelGenerator(null);
        ChartPanel chartPan = new ChartPanel(chart);
        graphMonde.removeAll();
        graphMonde.add(chartPan);
    }
    
    private void liveStatsModePays(){
        graphStatistique.removeAll();
        if (paysDiv != null){
            DefaultPieDataset dataset = new DefaultPieDataset();
            dataset.setValue("Infectés", paysDiv.getPopulationInfecte());
            dataset.setValue("Sains", paysDiv.getPopulationSaine());
            dataset.setValue("Morts", paysDiv.getPopulationMorte());
            JFreeChart chart = ChartFactory.createPieChart(      
             paysDiv.getNomPays(),   // chart title 
             dataset,          // data    
             true,             // include legend   
             true, 
             false);
            ChartPanel chartPan = new ChartPanel(chart);
            PiePlot piePlot = (PiePlot)chart.getPlot();
            piePlot.setSectionPaint("Infectés", Color.YELLOW);
            piePlot.setSectionPaint("Sains", Color.GREEN);
            piePlot.setSectionPaint("Morts", Color.RED);
            piePlot.setLabelGenerator(null);
            graphStatistique.add(chartPan);
        }
    }
    
    private void initGraph(ArrayList<Float> pourcentageSaineParJour, ArrayList<Float> pourcentageInfecteParJour, ArrayList<Float> pourcentageMortParJour, String titre)
    {
        XYSeries series1 = new XYSeries("Saine");
        
        for (int i=0; i<pourcentageSaineParJour.size();i++)
            series1.add(i+1, pourcentageSaineParJour.get(i)*100);

        XYSeries series2 = new XYSeries("Infectée");
        
        for (int i=0; i<pourcentageInfecteParJour.size();i++)
            series2.add(i+1, pourcentageInfecteParJour.get(i)*100);
        
        
        XYSeries series3 = new XYSeries("Morte");
        
        for (int i=0; i<pourcentageMortParJour.size();i++)
            series3.add(i+1, pourcentageMortParJour.get(i)*100);

        XYSeriesCollection dataset = new XYSeriesCollection();
        dataset.addSeries(series1);
        dataset.addSeries(series2);
        dataset.addSeries(series3);
        
        JFreeChart chart = ChartFactory.createXYLineChart(
                titre,
                "Jour",
                "%",
                dataset,
                PlotOrientation.VERTICAL,
                true,
                true,
                false
        );
        
        XYPlot plot = (XYPlot)chart.getPlot();

        plot.getRenderer().setSeriesPaint(0, Color.GREEN);
        plot.getRenderer().setSeriesPaint(1, Color.YELLOW);
        plot.getRenderer().setSeriesPaint(2, Color.RED);

        ChartPanel chartPan = new ChartPanel(chart);
        StatsPanel.removeAll();
        StatsPanel.add(chartPan);
    }

    
    private void demarrerSimulationActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_demarrerSimulationActionPerformed
        // TODO add your handling code here:
        modeSimudemie.setMode(SimudemieMode.simudemieMode.SIMULATION);
        activationBoutonCarteDuMonde(false);
        changerPanelSousMenu(statPanel);
        liveStatsMode();
        liveStatsModePays();
        if (enContinueRadio.isSelected())
            demarrerSimulationEnContinue();
        else
            demarrerSimulationTempsFixe(Integer.valueOf(nbrJours.getValue().toString()));
    }//GEN-LAST:event_demarrerSimulationActionPerformed

    private void supprimerTerrestreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_supprimerTerrestreActionPerformed
        // TODO add your handling code here:
        
        controller.supprimerLien(selectionPaysA, selectionPaysB, Lien.typeLien.TERRESTRE);
        controller.enregistreAction();
        repaint();
        supprimerTerrestre.setEnabled(false);
    }//GEN-LAST:event_supprimerTerrestreActionPerformed

    private void supprimerAerienActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_supprimerAerienActionPerformed
        // TODO add your handling code here:
        controller.supprimerLien(selectionPaysA, selectionPaysB, Lien.typeLien.AERIEN);
        controller.enregistreAction();
        repaint();
        supprimerAerien.setEnabled(false);
    }//GEN-LAST:event_supprimerAerienActionPerformed

    private void supprimerMaritimeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_supprimerMaritimeActionPerformed
        // TODO add your handling code here:
        controller.supprimerLien(selectionPaysA, selectionPaysB, Lien.typeLien.MARITIME);
        controller.enregistreAction();
        repaint();
        supprimerMaritime.setEnabled(false);
    }//GEN-LAST:event_supprimerMaritimeActionPerformed
    private void ModifierPaysActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ModifierPaysActionPerformed
        // TODO add your handling code here:
         zoomFactor = 1.00;
         
         modeSimudemie.setMode(SimudemieMode.simudemieMode.MODIFICATIONPAYS);

         changerPanelSousMenu(ModificationPaysPanel);
         erreurPaysModifie.setText("");
         ModificationTerminée.setEnabled(true);
        
         repaint();
    }//GEN-LAST:event_ModifierPaysActionPerformed

    private void drawingPanelMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_drawingPanelMousePressed
        if (modeSimudemie.getMode() == SimudemieMode.simudemieMode.MODIFICATIONPAYS){
          if(paysModifie != null)
          {
                ArrayList<Point> pointPolygone = controller.getListePoint(paysModifie); 
                for(int i = 0; i < pointPolygone.size(); i++)
                {
                    if (pointPolygone.get(i).distance(evt.getPoint()) < 10)
                        { 
                        pos = i;
                        return;
                        }
                }  
          }  
          
        }
        else if (modeSimudemie.getMode() == SimudemieMode.simudemieMode.DIVISIONPAYS){
            for (ArrayList<Point> formeReg: formeRegion){
                for (Point ptReg: formeReg){
                    if (ptReg.distance(evt.getPoint()) < 10){
                        pointDiv = formeReg.indexOf(ptReg);
                        formDiv = formeRegion.indexOf(formeReg);
                        break;
                    }
                }
            }
            for (Point ptTemp: formeTemporaire){
                if (ptTemp.distance(evt.getPoint()) < 10){
                    pointTemp = formeTemporaire.indexOf(ptTemp);
                    break;
                }
            }
        }
        else if (modeSimudemie.getMode() == SimudemieMode.simudemieMode.CREATIONPAYS ||
                modeSimudemie.getMode() == SimudemieMode.simudemieMode.CREATIONPAYS_REG){
            for (Point ptTemp: formeTemporaire){
                if (ptTemp.distance(evt.getPoint()) < 10){
                    pointTemp = formeTemporaire.indexOf(ptTemp);
                    break;
                }
            }
        }
        
        
    }//GEN-LAST:event_drawingPanelMousePressed

    private void drawingPanelMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_drawingPanelMouseReleased
        pos = -1;
        if (modeSimudemie.getMode() == SimudemieMode.simudemieMode.DIVISIONPAYS){
            pointDiv = -1;
            formDiv = -1;
            pointTemp = -1;
            pointTemp = -1;
        }
        
        
    }//GEN-LAST:event_drawingPanelMouseReleased

    private void drawingPanelMouseDragged(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_drawingPanelMouseDragged
        if (modeSimudemie.getMode() == SimudemieMode.simudemieMode.MODIFICATIONPAYS){
            if (pos == -1)
            return;
            Pays payss = controller.getCarteDuMonde().estDansPays(evt.getX(), evt.getY());
            if((paysModifie != null) && (payss == null || payss == paysModifie) ){
                ArrayList<Point> pointPolygone = controller.getListePoint(paysModifie); 
                    pointPolygone.get(pos).move(evt.getX(),evt.getY());
                for (Pays paysC:controller.getListePays()){
                    for(Point points: controller.getListePoint(paysC)){
                        if (points.distance(evt.getPoint()) < 15){
                            pointPolygone.get(pos).move(points.x, points.y);
                            break;
                        }
                    }
                }
            }
           
            erreurPaysModifie.setText("<html> Ce changement nécessitera une réassignation <br/> de vos régions, "
                      + "veuillez cliquer <br/> sur modifier région et poursuivre<html>");
            ModificationTerminée.setEnabled(false);
            
            
            repaint();
            
        }
        else if (modeSimudemie.getMode() == SimudemieMode.simudemieMode.DIVISIONPAYS){
            Pays pays = controller.getCarteDuMonde().estDansPays(evt.getX(), evt.getY());
            if (pays != null && pointDiv != -1 && formDiv != -1  && pays.getNomPays().equals(selectedPays.getText())){
                formeRegion.get(formDiv).get(pointDiv).move(evt.getX(), evt.getY());
                for (ArrayList<Point> arrPoint: formeRegion){
                    for (Point pt: arrPoint){
                        if (pt.distance(evt.getPoint()) < 15){
                            formeRegion.get(formDiv).get(pointDiv).move(pt.x, pt.y);
                            break;
                        }
                    }
                }
                for (Point pt: paysDiv.getForme().getListePoints()){
                    if (pt.distance(evt.getPoint()) < 15){
                        formeRegion.get(formDiv).get(pointDiv).move(pt.x, pt.y);
                        break;
                    }
                }
            }
            else if (pays != null && pointTemp != -1 && pays.getNomPays().equals(selectedPays.getText())){
                formeTemporaire.get(pointTemp).move(evt.getX(), evt.getY());
                for (ArrayList<Point> arrPoint: formeRegion){
                    for (Point pt: arrPoint){
                        if (pt.distance(evt.getPoint()) < 15){
                            formeTemporaire.get(pointTemp).move(pt.x, pt.y);
                            break;
                        }
                    }
                }
                for (Point pt: paysDiv.getForme().getListePoints()){
                    if (pt.distance(evt.getPoint()) < 15){
                        formeTemporaire.get(pointTemp).move(pt.x, pt.y);
                        break;
                    }
                }
            }
            drawingPanel.repaint();
        }
        else if (modeSimudemie.getMode() == SimudemieMode.simudemieMode.CREATIONPAYS
                || modeSimudemie.getMode() == SimudemieMode.simudemieMode.CREATIONPAYS_REG){
            Pays pays = controller.getCarteDuMonde().estDansPays(evt.getX(), evt.getY());
            if (pays == null && pointTemp != -1){
                System.out.println(pointTemp);
                formeTemporaire.get(pointTemp).move(evt.getX(), evt.getY());
                for (Pays paysB:controller.getListePays()){
                    for(Point point: controller.getListePoint(paysB)){
                        if (point.distance(evt.getPoint()) < 15){
                            formeTemporaire.get(pointTemp).move(point.x, point.y);
                            break;
                        }
                    }
                }
            }
            drawingPanel.repaint();
        }
    }//GEN-LAST:event_drawingPanelMouseDragged

    private void creationPaysReg(java.awt.event.MouseEvent evt){
        formeTemporaire.clear();
        if (Objects.requireNonNull(comboForme.getSelectedItem()).toString() == "Rectangle"){
            addPoint(evt.getX(), evt.getY());
            addPoint(evt.getX() + 300, evt.getY());
            addPoint(evt.getX() + 300, evt.getY() + 150);
            addPoint(evt.getX(), evt.getY() + 150);
            drawingPanel.repaint();
        }
        else if (Objects.requireNonNull(comboForme.getSelectedItem()).toString() == "Carré"){
            addPoint(evt.getX(), evt.getY());
            addPoint(evt.getX() + 150, evt.getY());
            addPoint(evt.getX() + 150, evt.getY() + 150);
            addPoint(evt.getX(), evt.getY() + 150);
            drawingPanel.repaint();
        }
        else if (Objects.requireNonNull(comboForme.getSelectedItem()).toString() == "Triangle"){
            addPoint(evt.getX(), evt.getY());
            addPoint(evt.getX() - 90, evt.getY() + 300);
            addPoint(evt.getX() + 90, evt.getY() + 300);
            drawingPanel.repaint();
        }
    }

    private void formeIrrItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_formeIrrItemStateChanged
        // TODO add your handling code here:
        if (evt.getStateChange() == 1){
            modeSimudemie.setMode(SimudemieMode.simudemieMode.CREATIONPAYS);
            annulerDernierPoint.setEnabled(true);
            comboForme.setEnabled(false);
            formeTemporaire.clear();
            drawingPanel.repaint();
        }
        else{
            modeSimudemie.setMode(SimudemieMode.simudemieMode.CREATIONPAYS_REG);
            annulerDernierPoint.setEnabled(false);
            comboForme.setEnabled(true);
            formeTemporaire.clear();
            drawingPanel.repaint();
        }
    }//GEN-LAST:event_formeIrrItemStateChanged

    private void enContinueRadioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_enContinueRadioActionPerformed
        // TODO add your handling code here:
        
        if (enContinueRadio.isSelected())
            nbrJours.setEnabled(false);
        else
            nbrJours.setEnabled(true);
    }//GEN-LAST:event_enContinueRadioActionPerformed

    private void stopActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_stopActionPerformed
        // TODO add your handling code here:
        simulationEnCours = false;
        modeSimudemie.setMode(SimudemieMode.simudemieMode.CARTEDUMONDE);
        activationBoutonCarteDuMonde(true);
    }//GEN-LAST:event_stopActionPerformed

    private void ModificationTerminéeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ModificationTerminéeActionPerformed
        // TODO add your handling code here:
        
        boolean paysPresent = false;
        System.out.println("test");
        
        if (paysModifie != null)
            {
            for (Pays pays:controller.getListePays())
            {

                if (nomPaysModifie.getText().compareTo(pays.getNomPays())==0 && !(nomPaysModifie.getText().compareTo(paysModifie.getNomPays())==0 ))
                {  
                    paysPresent = true;
                }

            } 
            if(!paysPresent)
            {
                controller.setNomPays(paysModifie, nomPaysModifie.getText());
                controller.setPopulation(paysModifie, (int)populationModifie.getValue());
                controller.setTxDepTerrestre(paysModifie,Float.parseFloat(tauxTerrestreModifie.getValue().toString()));
                controller.setTxDepMaritime(paysModifie,Float.parseFloat(tauxMaritimeModifie.getValue().toString()));
                controller.setTxDepAerien(paysModifie,Float.parseFloat(tauxAerienModifie.getValue().toString()));
                controller.setTxDepRegionnal(paysModifie,Float.parseFloat(tauxRegionnalModifie.getValue().toString()));

                clearInfoModifPays();
                controller.creationAutomatiqueLiensTerrestre(paysModifie);
                changerPanelSousMenu(BienvenuePanel);
                modeSimudemie.setMode(SimudemieMode.simudemieMode.CARTEDUMONDE);
                drawingPanel.repaint();
                controller.enregistreAction();
            }

            else if( paysPresent == true )
            {
                  erreurPaysModifie.setText("CE NOM DE PAYS EST DEJA PRESENT");
                  drawingPanel.repaint();
            }
        }
        
        else
        {
            changerPanelSousMenu(BienvenuePanel);
            modeSimudemie.setMode(SimudemieMode.simudemieMode.CARTEDUMONDE); 
        }
        
    }//GEN-LAST:event_ModificationTerminéeActionPerformed

    private void drawingPanelMouseWheelMoved(java.awt.event.MouseWheelEvent evt) {//GEN-FIRST:event_drawingPanelMouseWheelMoved
   
     if (modeSimudemie.getMode() == SimudemieMode.simudemieMode.CARTEDUMONDE){
        //Zoom in
        if (evt.getWheelRotation() < 0) {
            zoomFactor *= 1.1;
            repaint();
        }
        //Zoom out
        if (evt.getWheelRotation() > 0) {
            zoomFactor /= 1.1;
            repaint();
        }
          }
    }//GEN-LAST:event_drawingPanelMouseWheelMoved

    private void btnAppliquerMesureSanitaireActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAppliquerMesureSanitaireActionPerformed
        // TODO add your handling code here:
        AppliquerMesureListeMesure.removeAllItems();
        for (MesureSanitaire mesure :controller.getlisteMesureSanitaire()){
            AppliquerMesureListeMesure.addItem(mesure.getNomMesure());
        }
        AppliquerMesureListePays.removeAllItems();
        for (Pays pays:controller.getListePays()){
            AppliquerMesureListePays.addItem(pays.getNomPays());
        } 
        changerPanelSousMenu(AppliquerMesureSanitaire);
        modeSimudemie.setMode(SimudemieMode.simudemieMode.MESURESANITAIRE);
        RaffraichirTableApplicationMesure(); 
        
    }//GEN-LAST:event_btnAppliquerMesureSanitaireActionPerformed

    
    public void setTableMesureOnClick(MesureSanitaire mesure, Pays pays){
        DefaultTableModel  model = (DefaultTableModel) tableApplicationMesure.getModel();
        int rowCount = model.getRowCount();
        //Remove rows one by one from the end of the table
        for (int i = rowCount - 1; i >= 0; i--) {
            model.removeRow(i);
        }
        for (Region reg: controller.getListeRegion(pays)){
            model.addRow(new Object[]{reg.getNomRegion(), controller.getTauxAcceptationMesure(mesure, pays, reg)});
        }
    }
    
    public void RaffraichirTableApplicationMesure(){
        if (!controller.getlisteMesureSanitaire().isEmpty()){
            for (MesureSanitaire mesure:controller.getlisteMesureSanitaire()){
                if (mesure.getNomMesure() == AppliquerMesureListeMesure.getSelectedItem()){
                    if (!controller.getListePays().isEmpty()){
                        for (Pays pays:controller.getListePays()){
                            if (pays.getNomPays() == AppliquerMesureListePays.getSelectedItem()){
                                setTableMesureOnClick(mesure, pays);
                            }
                        }
                    }

                }
            }
        }
          
    }
    
    private void btnQuitterActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnQuitterActionPerformed
        // TODO add your handling code here:
        AppliquerMesureErreurInfo.setText("");
        changerPanelSousMenu(BienvenuePanel);
        modeSimudemie.setMode(SimudemieMode.simudemieMode.CARTEDUMONDE);
    }//GEN-LAST:event_btnQuitterActionPerformed

    public void appliquerMesureRegion(MesureSanitaire mesure, Pays pays){
        DefaultTableModel  model = (DefaultTableModel) tableApplicationMesure.getModel();
        int rowCount = model.getRowCount();
        boolean erreurDonnee = false;
        //Remove rows one by one from the end of the table
        for (int i = rowCount - 1; i >= 0; i--) {
            float tauxAcceptation = Float.parseFloat(String.valueOf(tableApplicationMesure.getValueAt(i,1)));
            if (tauxAcceptation < 0 || tauxAcceptation > 1){
                erreurDonnee = true;
                AppliquerMesureErreurInfo.setText("Taux d'acceptation invalide");
            }
            
        }
        if (erreurDonnee == false){
            for (int i = rowCount - 1; i >= 0; i--) {
                for (Region region:controller.getListeRegion(pays)){
                    if (region.getNomRegion() == String.valueOf(tableApplicationMesure.getValueAt(i,0))){
                        float tauxAcceptation = Float.parseFloat(String.valueOf(tableApplicationMesure.getValueAt(i,1)));
                        if (tauxAcceptation >= 0 && tauxAcceptation <= 1){
                            controller.appliquerMesure(mesure, pays, region, tauxAcceptation);
                        }
                        else
                        {

                        }

                    }
                }
            }
        }
    }
    
    private void btnEnregistrerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEnregistrerActionPerformed
        // TODO add your handling code here:
        AppliquerMesureErreurInfo.setText("");
        for (MesureSanitaire mesure:controller.getlisteMesureSanitaire()){
            if (mesure.getNomMesure() == AppliquerMesureListeMesure.getSelectedItem()){
                for (Pays pays:controller.getListePays()){
                    if (pays.getNomPays() == AppliquerMesureListePays.getSelectedItem()){
                        appliquerMesureRegion(mesure, pays);
                        controller.enregistreAction();
                    }
                }
            }
        }
    }//GEN-LAST:event_btnEnregistrerActionPerformed

    private void nomPaysActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_nomPaysActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_nomPaysActionPerformed

    private void nouvFormeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_nouvFormeActionPerformed
        // TODO add your handling code here:
        if (formeTemporaire.size() < 3){
            erreurDivision.setText("Dernière Forme Invalide");
        }
        else{
            formeRegion.add(new ArrayList<Point>(formeTemporaire));
            formeTemporaire.clear();
            drawingPanel.repaint();
            erreurDivision.setText("");
        }
        
    }//GEN-LAST:event_nouvFormeActionPerformed

    private void nomPaysModifieActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_nomPaysModifieActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_nomPaysModifieActionPerformed

    public void rafraichirModifierMesure(){
        if (!controller.getlisteMesureSanitaire().isEmpty()){
            for (MesureSanitaire mesure :controller.getlisteMesureSanitaire()){
                if (mesure.getNomMesure() == ComboListeMesureSanitaire.getSelectedItem().toString()){
                    ImpactTauxReproduction.setValue(mesure.getImpactTauxReproduction());
                    ImpactTauxTransmission.setValue(mesure.getImpactTauxTransmission());
                    ImpactTauxGuerison.setValue(mesure.getImpactTauxGuerison());
                    ImpactTauxMortalite.setValue(mesure.getImpactTauxMortalite());

                    ImpactTauxTerrestre.setValue(mesure.getImpactTauxDeplacementTerrestre());
                    ImpactTauxMaritime.setValue(mesure.getImpactTauxDeplacementMaritime());
                    ImpactTauxAerien.setValue(mesure.getImpactTauxDeplacementAerien());
                    ImpactTauxRegion.setValue(mesure.getImpactTauxDeplacementRegion());
                }
            }
        }
        if (ComboListeMesureSanitaire.getItemCount() == 0){
            ImpactTauxReproduction.setValue(0.0f);
            ImpactTauxTransmission.setValue(0.0f);
            ImpactTauxGuerison.setValue(0.0f);
            ImpactTauxMortalite.setValue(0.0f);

            ImpactTauxTerrestre.setValue(0.0f);
            ImpactTauxMaritime.setValue(0.0f);
            ImpactTauxAerien.setValue(0.0f);
            ImpactTauxRegion.setValue(0.0f);
        }
        
    }
    
    
    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
        zoomFactor = 1.0;
        repaint();
    }//GEN-LAST:event_jButton1ActionPerformed

    private void nouvelleDivision1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_nouvelleDivision1ActionPerformed
        // TODO add your handling code here:
        Pays selectionTemp = paysModifie;
        ModificationTerminéeActionPerformed(evt);
        clearInfoModifPays();
        nouvelleDivisionActionPerformed(evt);
        paysDiv = selectionTemp;
        repaint();
    }//GEN-LAST:event_nouvelleDivision1ActionPerformed

    private void btnEnregistrerModifMaladieActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEnregistrerModifMaladieActionPerformed
        // TODO add your handling code here:
        if (!controller.getListeMaladie().isEmpty()){
            for (Maladie maladie :controller.getListeMaladie()){
                if (maladie.getNom() == listeMaladieModifMaladie.getSelectedItem().toString()){
                    controller.modifierMaladie(listeMaladieModifMaladie.getSelectedItem().toString(),
                            (float)tauxReproductionModifMaladie.getValue(),
                            (float)tauxTransmissionModifMaladie.getValue(),
                            (float)tauxGuerisonModifMaladie.getValue(),
                            (float)tauxMortaliteModifMaladie.getValue());
                    controller.enregistreAction();   
                }   
            }
        }
    }//GEN-LAST:event_btnEnregistrerModifMaladieActionPerformed

    private void raffraichirModifierMaladie(){
        if (!controller.getListeMaladie().isEmpty()){
            for (Maladie maladie :controller.getListeMaladie()){
                if (maladie.getNom() == listeMaladieModifMaladie.getSelectedItem().toString()){
                    tauxReproductionModifMaladie.setValue(maladie.getTauxReproduction());
                    tauxTransmissionModifMaladie.setValue(maladie.getTauxTransmission());
                    tauxGuerisonModifMaladie.setValue(maladie.getTauxGuerison());
                    tauxMortaliteModifMaladie.setValue(maladie.getTauxMortalite());
                }   
            }
        }
        if (listeMaladieModifMaladie.getItemCount()==0)
        {
            tauxReproductionModifMaladie.setValue(0.0f);
            tauxTransmissionModifMaladie.setValue(0.0f);
            tauxGuerisonModifMaladie.setValue(0.0f);
            tauxMortaliteModifMaladie.setValue(0.0f);
        }
    }
    
        private void raffraichirAppliquerMaladie(){
        if (!controller.getListeMaladie().isEmpty())
        {
            for (Maladie maladie :controller.getListeMaladie()){
                if (maladie.getNom() == maladieAppliquerMaladie.getSelectedItem().toString()){
                    tauxReproductionApplicationMaladie.setValue((float)maladie.getTauxReproduction());
                    tauxTransmissionApplicationMaladie.setValue((float)maladie.getTauxTransmission());
                    tauxGuerisonApplicationMaladie.setValue((float)maladie.getTauxGuerison());
                    tauxMortaliteApplicationMaladie.setValue((float)maladie.getTauxMortalite());
                }   
            }
        }
        else
        {
            tauxReproductionApplicationMaladie.setValue(0.0f);
            tauxTransmissionApplicationMaladie.setValue(0.0f);
            tauxGuerisonApplicationMaladie.setValue(0.0f);
            tauxMortaliteApplicationMaladie.setValue(0.0f);
        }

        if (controller.getMaladie()!=null)
            nomMaladieAppliquerProjet.setText(controller.getMaladie().getNom());
    }
        
    
    
    private void btnModifierMaladieActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnModifierMaladieActionPerformed
        // TODO add your handling code here:
        zoomFactor = 1.00;
        changerPanelSousMenu(ModifierMaladie);
        modeSimudemie.setMode(SimudemieMode.simudemieMode.MALADIE);
        listeMaladieModifMaladie.removeAllItems();
        for (Maladie maladie :controller.getListeMaladie()){
            listeMaladieModifMaladie.addItem(maladie.getNom());
        }
        raffraichirModifierMaladie();
    }//GEN-LAST:event_btnModifierMaladieActionPerformed

    private void btnQuitterEnregistreMaladieActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnQuitterEnregistreMaladieActionPerformed
        // TODO add your handling code here:
        changerPanelSousMenu(BienvenuePanel);
        modeSimudemie.setMode(SimudemieMode.simudemieMode.CARTEDUMONDE);
    }//GEN-LAST:event_btnQuitterEnregistreMaladieActionPerformed

    private void listeMaladieModifMaladieItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_listeMaladieModifMaladieItemStateChanged
        // TODO add your handling code here:
        if (!controller.getListeMaladie().isEmpty()){
            if (listeMaladieModifMaladie.getItemCount() != 0){
                raffraichirModifierMaladie();
            }
        }
    }//GEN-LAST:event_listeMaladieModifMaladieItemStateChanged

    private void btnSupprimerMaladieActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSupprimerMaladieActionPerformed
        // TODO add your handling code here:
        if (!controller.getListeMaladie().isEmpty()){
            for (Maladie maladie :controller.getListeMaladie()){
                if (maladie.getNom() == listeMaladieModifMaladie.getSelectedItem().toString()){
                    controller.supprimerMaladie(maladie.getNom());
                    controller.enregistreAction();
                }   
            }
        }
        listeMaladieModifMaladie.removeAllItems();
        for (Maladie maladie :controller.getListeMaladie()){
            listeMaladieModifMaladie.addItem(maladie.getNom());
        }
        raffraichirModifierMaladie();
    }//GEN-LAST:event_btnSupprimerMaladieActionPerformed

    private void btnEnregistrerApplicationMaladieActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEnregistrerApplicationMaladieActionPerformed
        // TODO add your handling code here:
        if (!controller.getListeMaladie().isEmpty()){
            for (Maladie maladie :controller.getListeMaladie()){
                if (maladie.getNom() == maladieAppliquerMaladie.getSelectedItem().toString()){
                    controller.selectionnerMaladie(maladieAppliquerMaladie.getSelectedItem().toString());
                    controller.enregistreAction();
                }   
            }
        }
        raffraichirAppliquerMaladie();
    }//GEN-LAST:event_btnEnregistrerApplicationMaladieActionPerformed

    private void btnQuitterApplicationMaladieActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnQuitterApplicationMaladieActionPerformed
        // TODO add your handling code here:
        changerPanelSousMenu(BienvenuePanel);
        modeSimudemie.setMode(SimudemieMode.simudemieMode.CARTEDUMONDE);
    }//GEN-LAST:event_btnQuitterApplicationMaladieActionPerformed

    private void btnAppliquerMaladieActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAppliquerMaladieActionPerformed
        // TODO add your handling code here:
        zoomFactor = 1.00;
        changerPanelSousMenu(AppliquerMaladie);
        modeSimudemie.setMode(SimudemieMode.simudemieMode.MALADIE);
        maladieAppliquerMaladie.removeAllItems();
        for (Maladie maladie :controller.getListeMaladie()){
            maladieAppliquerMaladie.addItem(maladie.getNom());
        }
        raffraichirAppliquerMaladie();
    }//GEN-LAST:event_btnAppliquerMaladieActionPerformed

    private void maladieAppliquerMaladieItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_maladieAppliquerMaladieItemStateChanged
        // TODO add your handling code here:
        if (!controller.getListeMaladie().isEmpty()){
            if (maladieAppliquerMaladie.getItemCount() != 0){
                raffraichirAppliquerMaladie();
            }
        }
    }//GEN-LAST:event_maladieAppliquerMaladieItemStateChanged

    private void AppliquerMesureListeMesureItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_AppliquerMesureListeMesureItemStateChanged
        // TODO add your handling code here:
        if (AppliquerMesureListeMesure.getItemCount() != 0){
            RaffraichirTableApplicationMesure();
        }
    }//GEN-LAST:event_AppliquerMesureListeMesureItemStateChanged

    private void AppliquerMesureListePaysItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_AppliquerMesureListePaysItemStateChanged
        // TODO add your handling code here:
        if (AppliquerMesureListePays.getItemCount() != 0){
            RaffraichirTableApplicationMesure();
        }
    }//GEN-LAST:event_AppliquerMesureListePaysItemStateChanged

    private void ComboListeMesureSanitaireItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_ComboListeMesureSanitaireItemStateChanged
        // TODO add your handling code here:
        if (ComboListeMesureSanitaire.getItemCount() != 0){
            rafraichirModifierMesure();
        }
    }//GEN-LAST:event_ComboListeMesureSanitaireItemStateChanged

    private void btnRetirerMesureActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRetirerMesureActionPerformed
        // TODO add your handling code here:
        String nomMesure = "";
        if (!controller.getlisteMesureSanitaire().isEmpty()){
            for (MesureSanitaire mesure:controller.getlisteMesureSanitaire()){
              if (ComboListeMesureSanitaire.getSelectedItem().toString() == mesure.getNomMesure()){
                  nomMesure = mesure.getNomMesure();
              }
            }
            
            if (nomMesure != ""){
                controller.retirerMesureSanitaire(nomMesure);
                controller.enregistreAction();
            }
            
            ComboListeMesureSanitaire.removeAllItems();
            for (MesureSanitaire mesure :controller.getlisteMesureSanitaire()){
                ComboListeMesureSanitaire.addItem(mesure.getNomMesure());
            }
            rafraichirModifierMesure();  
        }
        
    }//GEN-LAST:event_btnRetirerMesureActionPerformed

    private void btnSupprimerApplicationMesureActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSupprimerApplicationMesureActionPerformed
        // TODO add your handling code here:
        for (MesureSanitaire mesure:controller.getlisteMesureSanitaire()){
            if (mesure.getNomMesure() == AppliquerMesureListeMesure.getSelectedItem()){
                for (Pays pays:controller.getListePays()){
                    if (pays.getNomPays() == AppliquerMesureListePays.getSelectedItem()){
                        controller.supprimerApplicationMesureSanitaire(mesure, pays);
                        controller.enregistreAction();
                    }
                }
            }
        }
        RaffraichirTableApplicationMesure();
    }//GEN-LAST:event_btnSupprimerApplicationMesureActionPerformed

    private void UndoSimulationActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_UndoSimulationActionPerformed
        // TODO add your handling code here:
        controller.resetSimulation();
        controller.clearStats();
        updateStatistiques();
        StatsPanel.removeAll();
        paysVue = null;
        selectionRegion = null;
        changerPanelSousMenu(BienvenuePanel);
        numeroJour.setText(String.valueOf(controller.getProjet().getSimulation().getJour()));
        modeSimudemie.setMode(SimudemieMode.simudemieMode.CARTEDUMONDE);
        repaint();
    }//GEN-LAST:event_UndoSimulationActionPerformed

    private void statistiquesBoutonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_statistiquesBoutonActionPerformed
        // TODO add your handling code here:
        modeSimudemie.setMode(SimudemieMode.simudemieMode.STATISTIQUE);
        updateStatistiques();
    }//GEN-LAST:event_statistiquesBoutonActionPerformed

    private void supprimerPaysActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_supprimerPaysActionPerformed
        // TODO add your handling code here:
        
        if (paysModifie != null)
            controller.supprimerPays(paysModifie);
        
        clearSelection();
        controller.enregistreAction();
        repaint();
    }//GEN-LAST:event_supprimerPaysActionPerformed



    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel Aerien;
    private javax.swing.JPanel AjouterMesureSanitaire;
    private javax.swing.JPanel AjouterPays;
    private javax.swing.JButton AnnulerCreationMesure;
    private javax.swing.JButton AnnulerCreerMaladie;
    private javax.swing.JButton AnnulerNouveauPays;
    private javax.swing.JPanel AppliquerMaladie;
    private javax.swing.JLabel AppliquerMesureErreurInfo;
    private javax.swing.JComboBox<String> AppliquerMesureListeMesure;
    private javax.swing.JComboBox<String> AppliquerMesureListePays;
    private javax.swing.JPanel AppliquerMesureSanitaire;
    private javax.swing.JPanel BienvenuePanel;
    private javax.swing.JComboBox<String> ComboListeMesureSanitaire;
    private javax.swing.JPanel CreerMaladie;
    private javax.swing.JButton CreerMaladieBouton;
    private javax.swing.JButton CreerMesure;
    private javax.swing.JPanel DiviserPays;
    private javax.swing.JPanel EditionLien;
    private javax.swing.JLabel ErreurInfoNouvelleMesure;
    private javax.swing.JSpinner ImpactTauxAerien;
    private javax.swing.JSpinner ImpactTauxGuerison;
    private javax.swing.JSpinner ImpactTauxMaritime;
    private javax.swing.JSpinner ImpactTauxMortalite;
    private javax.swing.JSpinner ImpactTauxRegion;
    private javax.swing.JSpinner ImpactTauxReproduction;
    private javax.swing.JSpinner ImpactTauxTerrestre;
    private javax.swing.JSpinner ImpactTauxTransmission;
    private javax.swing.JLabel Infecter;
    private javax.swing.JFormattedTextField InfecterText;
    private javax.swing.JLabel InfoAjouterMesure;
    private javax.swing.JLabel InfoMaladie;
    private javax.swing.JLabel InfoModifierMesure;
    private javax.swing.JPanel InfosPaysPanel;
    private javax.swing.JLabel LabelNomMaladie;
    private javax.swing.JLabel LabelTauxContagion;
    private javax.swing.JLabel LabelTauxGuerison;
    private javax.swing.JLabel LabelTauxMortalite;
    private javax.swing.JButton Load;
    private javax.swing.JLabel Maritime;
    private javax.swing.JPanel MenuBouton;
    private javax.swing.JPanel ModificationPaysPanel;
    private javax.swing.JButton ModificationTerminée;
    private javax.swing.JPanel ModifierMaladie;
    private javax.swing.JPanel ModifierMesureSanitaire;
    private javax.swing.JButton ModifierPays;
    private javax.swing.JLabel Mort;
    private javax.swing.JFormattedTextField MortText;
    private javax.swing.JSpinner NMImpactTauxAerien;
    private javax.swing.JSpinner NMImpactTauxContagion;
    private javax.swing.JSpinner NMImpactTauxGuerison;
    private javax.swing.JSpinner NMImpactTauxMaritime;
    private javax.swing.JSpinner NMImpactTauxMortalite;
    private javax.swing.JSpinner NMImpactTauxRegion;
    private javax.swing.JSpinner NMImpactTauxReproduction;
    private javax.swing.JSpinner NMImpactTauxTerrestre;
    private javax.swing.JTextField NomNouvelleMesure;
    private javax.swing.JLabel NomPays;
    private javax.swing.JFormattedTextField NomPaysText;
    private javax.swing.JLabel NouveauPaysLabel;
    private javax.swing.JLabel NouveauPaysLabel1;
    private javax.swing.JSpinner NumericTauxContagion;
    private javax.swing.JSpinner NumericTauxGuerison;
    private javax.swing.JSpinner NumericTauxMortalite;
    private javax.swing.JSpinner NumericTauxReproduction;
    private javax.swing.JButton Redo;
    private javax.swing.JLabel Sains;
    private javax.swing.JFormattedTextField SainsText;
    private javax.swing.JButton Save;
    private javax.swing.JLayeredPane SousMenuPanel;
    private javax.swing.JPanel StatsPanel;
    private javax.swing.JLabel SupprimerLienLabel;
    private javax.swing.JToolBar ToolBarMenu;
    private javax.swing.JButton Undo;
    private javax.swing.JButton UndoSimulation;
    private javax.swing.JPanel VuePaysPanel;
    private javax.swing.JLabel aerienLabel;
    private javax.swing.JButton ajoutLigne;
    private javax.swing.JButton ajouterMesureSanitaire;
    private javax.swing.JButton ajouterPays;
    private javax.swing.JButton annulerDernierPoint;
    private javax.swing.JButton annulerDivision;
    private javax.swing.JButton arreterSimulation;
    private javax.swing.JLabel bienvenue;
    private javax.swing.JButton btnAnnulerEnregistrementMesure;
    private javax.swing.JButton btnAppliquerMaladie;
    private javax.swing.JButton btnAppliquerMesureSanitaire;
    private javax.swing.JButton btnEnregistrer;
    private javax.swing.JButton btnEnregistrerApplicationMaladie;
    private javax.swing.JButton btnEnregistrerMesure;
    private javax.swing.JButton btnEnregistrerModifMaladie;
    private javax.swing.JButton btnFile;
    private javax.swing.JButton btnMesureSanitaire;
    private javax.swing.JButton btnModifierMaladie;
    private javax.swing.JButton btnOKMaladie;
    private javax.swing.JButton btnQuitter;
    private javax.swing.JToggleButton btnQuitterApplicationMaladie;
    private javax.swing.JToggleButton btnQuitterEnregistreMaladie;
    private javax.swing.JButton btnRetirerMesure;
    private javax.swing.JButton btnSupprimerApplicationMesure;
    private javax.swing.JButton btnSupprimerMaladie;
    private javax.swing.JComboBox<String> comboForme;
    private javax.swing.JLabel conceptionLabel;
    private javax.swing.JButton creerLien;
    private javax.swing.JButton creerPaysButton;
    private javax.swing.JButton demarrerSimulation;
    private javax.swing.JButton diviser;
    private gui.DrawingPanel drawingPanel;
    private javax.swing.JButton effLigne;
    private javax.swing.JRadioButton enContinueRadio;
    private javax.swing.JButton enleverSelection;
    private javax.swing.JButton enleverSelectionRegion;
    private javax.swing.JLabel erreurCreationLien;
    private javax.swing.JLabel erreurDivision;
    private javax.swing.JLabel erreurInfosPays;
    private javax.swing.JLabel erreurLabel;
    private javax.swing.JLabel erreurPaysModifie;
    private javax.swing.JLabel erreurPctPop;
    private javax.swing.JButton exportJpeg;
    private javax.swing.JCheckBox formeIrr;
    private javax.swing.JPanel graphMonde;
    private javax.swing.JPanel graphStatistique;
    private javax.swing.JLabel infecterVue;
    private javax.swing.JLabel infecterVueLabel;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel27;
    private javax.swing.JLabel jLabel28;
    private javax.swing.JLabel jLabel29;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel30;
    private javax.swing.JLabel jLabel31;
    private javax.swing.JLabel jLabel32;
    private javax.swing.JLabel jLabel33;
    private javax.swing.JLabel jLabel34;
    private javax.swing.JLabel jLabel35;
    private javax.swing.JLabel jLabel36;
    private javax.swing.JLabel jLabel37;
    private javax.swing.JLabel jLabel38;
    private javax.swing.JLabel jLabel39;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel40;
    private javax.swing.JLabel jLabel41;
    private javax.swing.JLabel jLabel42;
    private javax.swing.JLabel jLabel43;
    private javax.swing.JLabel jLabel44;
    private javax.swing.JLabel jLabel45;
    private javax.swing.JLabel jLabel46;
    private javax.swing.JLabel jLabel47;
    private javax.swing.JLabel jLabel48;
    private javax.swing.JLabel jLabel49;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel50;
    private javax.swing.JLabel jLabel51;
    private javax.swing.JLabel jLabel52;
    private javax.swing.JLabel jLabel53;
    private javax.swing.JLabel jLabel54;
    private javax.swing.JLabel jLabel55;
    private javax.swing.JLabel jLabel56;
    private javax.swing.JLabel jLabel57;
    private javax.swing.JLabel jLabel58;
    private javax.swing.JLabel jLabel59;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel60;
    private javax.swing.JLabel jLabel61;
    private javax.swing.JLabel jLabel62;
    private javax.swing.JLabel jLabel63;
    private javax.swing.JLabel jLabel64;
    private javax.swing.JLabel jLabel65;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JLabel lblTitreLien;
    private javax.swing.JLabel lblType;
    private javax.swing.JLabel lblpaysA;
    private javax.swing.JLabel lblpaysB;
    private javax.swing.JComboBox<String> listeMaladieModifMaladie;
    private javax.swing.JComboBox<String> maladieAppliquerMaladie;
    private javax.swing.JLabel maritimeLabel;
    private javax.swing.JLabel mortVueLabel;
    private javax.swing.JLabel mortsVue;
    private javax.swing.JLabel msJourLabel;
    private javax.swing.JSpinner msParJour;
    private javax.swing.JLabel nbrJourrLabel;
    private javax.swing.JSpinner nbrJours;
    private javax.swing.JTextField nomMaladieAppliquerProjet;
    private javax.swing.JTextField nomPays;
    private javax.swing.JTextField nomPaysA;
    private javax.swing.JTextField nomPaysB;
    private javax.swing.JTextField nomPaysModifie;
    private javax.swing.JLabel nomRegionVue;
    private javax.swing.JButton nouvForme;
    private javax.swing.JButton nouveauLien;
    private javax.swing.JButton nouvelleDivision;
    private javax.swing.JButton nouvelleDivision1;
    private javax.swing.JLabel numeroJour;
    private javax.swing.JLabel pctInfecteRegionVue;
    private javax.swing.JSpinner pctInfecter;
    private javax.swing.JLabel pctInfecterLabel;
    private javax.swing.JLabel pctInfecterLabel1;
    private javax.swing.JSpinner pctMort;
    private javax.swing.JLabel pctMorteLabel;
    private javax.swing.JLabel pctMorteLabel1;
    private javax.swing.JLabel pctPopPaysVue;
    private javax.swing.JLabel pctPopPaysVuelabel;
    private javax.swing.JSpinner pctSain;
    private javax.swing.JLabel pctSaineLabel;
    private javax.swing.JLabel pctSaineLabel1;
    private javax.swing.JLabel pctSainsRegionVue;
    private javax.swing.JLabel pctmortRegionVue;
    private javax.swing.JLabel plusDeMalade;
    private javax.swing.JLabel popVue;
    private javax.swing.JLabel popVueLabel;
    private javax.swing.JLabel popVuePays;
    private javax.swing.JSpinner population;
    private javax.swing.JLabel populationInfoLabel;
    private javax.swing.JLabel populationLabel;
    private javax.swing.JSpinner populationModifie;
    private javax.swing.JLabel populationRegionVue;
    private javax.swing.JFormattedTextField populationText;
    private javax.swing.JLabel regionnalLabel;
    private javax.swing.JLabel sainsVue;
    private javax.swing.JLabel sainsVueLabel;
    private javax.swing.JTextField selectedPays;
    private javax.swing.JLabel simulationLabel1;
    private javax.swing.JPanel statPanel;
    private javax.swing.JButton statistiquesBouton;
    private javax.swing.JButton stop;
    private javax.swing.JButton supprimerAerien;
    private javax.swing.JButton supprimerMaritime;
    private javax.swing.JButton supprimerPays;
    private javax.swing.JButton supprimerTerrestre;
    private javax.swing.JTable tableApplicationMesure;
    private javax.swing.JTable tableRegion;
    private javax.swing.JSpinner tauxAerien;
    private javax.swing.JSpinner tauxAerienModifie;
    private javax.swing.JSpinner tauxGuerisonApplicationMaladie;
    private javax.swing.JSpinner tauxGuerisonModifMaladie;
    private javax.swing.JSpinner tauxMaritime;
    private javax.swing.JSpinner tauxMaritimeModifie;
    private javax.swing.JSpinner tauxMortaliteApplicationMaladie;
    private javax.swing.JSpinner tauxMortaliteModifMaladie;
    private javax.swing.JSpinner tauxRegionnal;
    private javax.swing.JSpinner tauxRegionnalModifie;
    private javax.swing.JSpinner tauxReproductionApplicationMaladie;
    private javax.swing.JSpinner tauxReproductionModifMaladie;
    private javax.swing.JSpinner tauxTerrestre;
    private javax.swing.JSpinner tauxTerrestreModifie;
    private javax.swing.JSpinner tauxTransmissionApplicationMaladie;
    private javax.swing.JSpinner tauxTransmissionModifMaladie;
    private javax.swing.JButton terminerEditionLien;
    private javax.swing.JLabel terrestre;
    private javax.swing.JLabel terrestreLabel;
    private javax.swing.JLabel titreDivisionPays;
    private javax.swing.JLabel totalInfecterRegionVue;
    private javax.swing.JLabel totalMortRegionVue;
    private javax.swing.JLabel totalSainsRegionVue;
    private javax.swing.JLabel txAerienVue;
    private javax.swing.JLabel txMaritimeVue;
    private javax.swing.JLabel txRegionnalVue;
    private javax.swing.JLabel txTerrestreVue;
    private javax.swing.JTextField txtNomMaladie;
    private javax.swing.JComboBox<String> typeLien;
    private javax.swing.JLabel vuePaysLabel;
    // End of variables declaration//GEN-END:variables
}

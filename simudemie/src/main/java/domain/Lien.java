package domain;

import java.awt.Point;

public class Lien implements java.io.Serializable, Cloneable
{
    public static enum typeLien {AERIEN, TERRESTRE, MARITIME};
    
    private Pays paysA;
    private Point pointPaysA;
    private Pays paysB;
    private Point pointPaysB;
    private typeLien type;

    public Lien(Pays p_paysA, Pays p_paysB, typeLien p_type, Point p_pointPaysA, Point p_pointPaysB)
    {
        paysA = p_paysA;
        pointPaysA = p_pointPaysA;
        pointPaysB = p_pointPaysB;
        paysB = p_paysB;
        type = p_type;
    }
    
    public Pays getPaysA()
    {
        return paysA;
    }
    
    public Point getPointPaysA()
    {
        return pointPaysA;
    }
    
    public void setPaysA(Pays p_paysA)
    {
        paysA = p_paysA;
    }
    
    public Pays getPaysB()
    {
        return paysB;
    }
    
    public Point getPointPaysB()
    {
        return pointPaysB;
    }
    
    public void setPaysB(Pays p_paysB)
    {
        paysB = p_paysB;
    }
    
    public typeLien getType()
    {
        return type;
    }
    
    public void modifierType(typeLien p_type)
    {
        type = p_type;
    }
}

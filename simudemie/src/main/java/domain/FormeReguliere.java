package domain;

import java.util.HashMap;

public class FormeReguliere extends Forme implements java.io.Serializable{
    private int scale;
    private String nomForme;
    private static HashMap<String, ListePointReguliere> choixForme;

    // add constructor with choixForme defined;

    public void setScale(int _scale){
        scale = _scale;
    }
    public int getScale() {
        return scale;
    }
    public void addChoixForme(String _nomForme, ListePointReguliere _listePoint) {
        choixForme.put(_nomForme, _listePoint);
    }
}

package domain;
//Oussama
import java.awt.*;
import java.util.ArrayList;

public abstract class Forme implements java.io.Serializable{
    protected ArrayList<Point> listePoints = new ArrayList<Point>();
    
    public ArrayList<Point> getListePoints()
    {
        return listePoints;
    }

}

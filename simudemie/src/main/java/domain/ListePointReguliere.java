package domain;

import java.awt.*;
import java.util.ArrayList;

public class ListePointReguliere {
    private ArrayList<Point> listPoints;

    ListePointReguliere(){
        listPoints = new ArrayList<Point>();
    }
    public ArrayList<Point> getlistePoints(){
        return listPoints;
    }
    public void setListePoints(ArrayList<Point> _newListPoints){
        listPoints = _newListPoints;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package domain;

import java.io.*; 
import java.util.*; 
        
/**
 *
 * @author alexa
 */
public class Projet implements java.io.Serializable
{
    private String nomProjet;
    private Simulation simulation;
    private CarteDuMonde carteDuMonde;
    private ArrayList<Maladie> ListeMaladie;
    
    public Projet(String p_nomProjet){
        nomProjet = p_nomProjet;
        carteDuMonde = new CarteDuMonde();
        initialiserSimulation(0, carteDuMonde);
        ListeMaladie = new ArrayList<Maladie>();
    }

    public void initialiserMaladie(String p_nomMaladie,float p_tauxReproduction, float p_tauxContagion, float p_tauxGuerison, float p_tauxMortalite){
        getSimulation().setMaladie(new Maladie(p_nomMaladie, p_tauxReproduction, p_tauxContagion, p_tauxGuerison, p_tauxMortalite));
    }
    
    public void modifierMaladie(String p_nomMaladie,float p_tauxReproduction, float p_tauxTransmission, float p_tauxGuerison, float p_tauxMortalite){
        for (Maladie maladieSelectionner:ListeMaladie){
            if (p_nomMaladie == maladieSelectionner.getNom()){
                maladieSelectionner.setNom(p_nomMaladie);
                maladieSelectionner.setTauxReproduction(p_tauxReproduction);
                maladieSelectionner.setTauxTransmission(p_tauxTransmission);
                maladieSelectionner.setTauxGuerison(p_tauxGuerison);
                maladieSelectionner.setTauxMortalite(p_tauxMortalite);
            }
        }
    }
    
    public void supprimerMaladie(String p_nomMaladie){
        int indexToRemove = -1;
        for (int i = 0; i<ListeMaladie.size(); i++){
            if (p_nomMaladie == ListeMaladie.get(i).getNom()){
                indexToRemove = i;
                if (getSimulation().getMaladie()!=null)
                {
                    if (ListeMaladie.get(i).getNom() == getSimulation().getMaladie().getNom()){
                        getSimulation().setMaladie(null);
                    }
                }
            }
        }
        if (indexToRemove != -1){
            if (ListeMaladie.size() == 1){
                ListeMaladie = new ArrayList<Maladie>();
            }
            else
            {
                ListeMaladie.remove(indexToRemove);
            }
        }
    }
    public ArrayList<Maladie> getListeMaladie(){
        return ListeMaladie;
    }
    
    public void setListeMaladie(ArrayList<Maladie> p_listeMaladie)
    {
        ListeMaladie = p_listeMaladie;
    }
    
    
    public void creerMaladie(String p_nomMaladie,float p_tauxReproduction, float p_tauxContagion, float p_tauxGuerison, float p_tauxMortalite){
        Maladie nouvelleMaladie = new Maladie( p_nomMaladie, p_tauxReproduction, p_tauxContagion,  p_tauxGuerison,  p_tauxMortalite);
        ListeMaladie.add(nouvelleMaladie);
    }
    
    public void selectionnerMaladie(String p_nomMaladie){
        for (Maladie maladieSelectionner:ListeMaladie){
            if (p_nomMaladie == maladieSelectionner.getNom()){
                getSimulation().setMaladie(maladieSelectionner);
            }
        }
    }
    
    public void initialiserSimulation(int p_jour, CarteDuMonde p_carteMonde){
        simulation = new Simulation(p_jour, carteDuMonde);
    }
    
    public void ouvrirFichier(String pathFichier){
       Projet p_projet = null;
        
        try
        {
            FileInputStream fileIn = new FileInputStream(pathFichier);
            ObjectInputStream in = new ObjectInputStream(fileIn);
            p_projet = (Projet) in.readObject();
            this.setNomProjet(p_projet.getNomProjet());
            this.setSimulation(p_projet.getSimulation());
            this.setCarteDuMonde(p_projet.getCarteDuMonde());
            this.setListeMaladie(p_projet.getListeMaladie());
        }
        catch(Exception c) 
        {
            c.printStackTrace();
        } 
    }
    
    public void enregistrerFichier(String pathFichier){
    try 
        {
            FileOutputStream fileOut = new FileOutputStream(pathFichier);
            ObjectOutputStream out = new ObjectOutputStream(fileOut);
            out.writeObject(this);
            out.close();
            fileOut.close();
        }
        catch(IOException i)
        {
            i.printStackTrace();
        }
    }
    
    public String getNomProjet(){
        return nomProjet;
    }
    public Simulation getSimulation(){
        return simulation;
    }   
    public CarteDuMonde getCarteDuMonde(){
        return carteDuMonde;
    }
    public void setNomProjet(String p_nomProjet){
        nomProjet = p_nomProjet;
    }
    public void setSimulation(Simulation p_simulation){
        simulation = p_simulation;
    }   
    public void setCarteDuMonde(CarteDuMonde p_carteDuMonde){
        carteDuMonde = p_carteDuMonde;
    }
}

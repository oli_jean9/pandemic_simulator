/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package domain;

import java.util.ArrayList;
import java.util.Random;
import org.apache.commons.math3.distribution.BinomialDistribution;
/**
 *
 * @author Olivier Jean
 */
public class Simulation implements java.io.Serializable, Cloneable
{
    private int jour;
    private int tempsParJour;
    private CarteDuMonde carteMonde;
    private Maladie maladie;
    
    public Simulation(int p_jour, CarteDuMonde p_carteMonde)
    {
        jour = p_jour;
        carteMonde = p_carteMonde;
        maladie = null;
    }
    
    
    public int getJour()
    {
        return jour;
    }
    
    public void setJour(int p_jour)
    {
        jour = p_jour;
    }
    
    public int gettempsParJour()
    {
        return tempsParJour;
    }
    
    public void setTempsParJour(short p_TempsParJour)
    {
        tempsParJour = p_TempsParJour;
    }
    
    public CarteDuMonde getCarteDuMonde()
    {
        return carteMonde;
    }
    
    public void setCarteMonde(CarteDuMonde p_carteMonde)
    {
        carteMonde = p_carteMonde;
    }
    
    public Maladie getMaladie()
    {
        return maladie;
    }
    
    public void setMaladie(Maladie p_maladie)
    {
        maladie = p_maladie;
    }

    public void enregistreDonneesStatistiques(){
        
        getCarteDuMonde().enregistreDonneesStatistiques();
        
        for (Pays pays:carteMonde.getListePays()){
            pays.enregistreDonneesStatistiques();
            for (Region region:pays.getListeRegion()){
                region.enregistreDonneesStatistiques();
            }
        }
    }
    
    public void afficherStatistique(){
        for (int i =0; i<jour; i++){
            for (Pays pays:carteMonde.getListePays()){
                System.out.println(pays.getNomPays());
                for (Region region:pays.getListeRegion()){
                    System.out.println(region.getNomRegion());
                    System.out.println(region.retourneStatistiqueSain(jour));
                    System.out.println(region.retourneStatistiqueInfecte(jour));
                    System.out.println(region.retourneStatistiqueMort(jour));
                }
            }
        }
    }
    
    
    public float calculerTauxReproduction(Pays p_pays, Region p_region)
    {
        float tauxImpactTotal = 0;
        ArrayList<MesureSanitaire> p_listeMesure = getCarteDuMonde().getListeMesures();
        for (MesureSanitaire mesureCourante: p_listeMesure)
        {
            ArrayList<ListeApplicationMesure> listeApplicationMesureCourante = mesureCourante.getListeApplicationMesure();
            for (ListeApplicationMesure applicationCourante:listeApplicationMesureCourante)
            {
                if (applicationCourante.getPays()== p_pays &&
                    applicationCourante.getRegion()== p_region)
                {
                    tauxImpactTotal += (mesureCourante.getImpactTauxReproduction()*applicationCourante.getTauxAcceptation());
                }
            } 
        }
        float tauxAjuster = getMaladie().getTauxReproduction() + (getMaladie().getTauxReproduction()*tauxImpactTotal);
        if (tauxAjuster > 0)
            return tauxAjuster;
        else
            return (float)0.0;
    }
    
    
    public float calculerTauxTransmission(Pays p_pays, Region p_region)
    {
        float tauxImpactTotal = 0;
        ArrayList<MesureSanitaire> p_listeMesure = getCarteDuMonde().getListeMesures();
        for (MesureSanitaire mesureCourante: p_listeMesure)
        {
            ArrayList<ListeApplicationMesure> listeApplicationMesureCourante = mesureCourante.getListeApplicationMesure();
            for (ListeApplicationMesure applicationCourante:listeApplicationMesureCourante)
            {
                if (applicationCourante.getPays()== p_pays &&
                    applicationCourante.getRegion()== p_region)
                {
                    tauxImpactTotal += (mesureCourante.getImpactTauxTransmission()*applicationCourante.getTauxAcceptation());
                }
            } 
        }
        float tauxAjuster = getMaladie().getTauxTransmission() + (getMaladie().getTauxTransmission()*tauxImpactTotal);
        if (tauxAjuster > 0)
            return tauxAjuster;
        else
            return (float)0.0;
    }
    
    public float calculerTauxGuerison(Pays p_pays, Region p_region)
    {
        float tauxImpactTotal = 0;
        ArrayList<MesureSanitaire> p_listeMesure = getCarteDuMonde().getListeMesures();
        for (MesureSanitaire mesureCourante: p_listeMesure)
        {
            ArrayList<ListeApplicationMesure> listeApplicationMesureCourante = mesureCourante.getListeApplicationMesure();
            for (ListeApplicationMesure applicationCourante:listeApplicationMesureCourante)
            {
                if (applicationCourante.getPays()== p_pays &&
                    applicationCourante.getRegion()== p_region)
                {
                    tauxImpactTotal += (mesureCourante.getImpactTauxGuerison()*applicationCourante.getTauxAcceptation());
                }
            }
        }
        float tauxAjuster = getMaladie().getTauxGuerison()+ (getMaladie().getTauxGuerison()*tauxImpactTotal);
        if (tauxAjuster > 0)
            return tauxAjuster;
        else
            return (float)0.0;
    }
    
    public float calculerTauxMortalite(Pays p_pays, Region p_region)
    {
        float tauxImpactTotal = 0;
        ArrayList<MesureSanitaire> p_listeMesure = getCarteDuMonde().getListeMesures();
        for (MesureSanitaire mesureCourante: p_listeMesure)
        {
            ArrayList<ListeApplicationMesure> listeApplicationMesureCourante = mesureCourante.getListeApplicationMesure();
            for (ListeApplicationMesure applicationCourante:listeApplicationMesureCourante)
            {
                if (applicationCourante.getPays()== p_pays &&
                    applicationCourante.getRegion()== p_region)
                {
                    tauxImpactTotal += (mesureCourante.getImpactTauxMortalite()*applicationCourante.getTauxAcceptation());
                }
            }
        }
        float tauxAjuster = getMaladie().getTauxMortalite()+ (getMaladie().getTauxMortalite()*tauxImpactTotal);
        if (tauxAjuster > 0)
            return tauxAjuster;
        else
            return (float)0.0;
    }
    
    public float calculerTauxDeplacementRegion(Pays p_pays, Region p_region)
    {
        float tauxImpactTotal = 0;
        ArrayList<MesureSanitaire> p_listeMesure = getCarteDuMonde().getListeMesures();
        for (MesureSanitaire mesureCourante: p_listeMesure)
        {
            ArrayList<ListeApplicationMesure> listeApplicationMesureCourante = mesureCourante.getListeApplicationMesure();
            for (ListeApplicationMesure applicationCourante:listeApplicationMesureCourante)
            {
                if (applicationCourante.getPays() == p_pays &&
                    applicationCourante.getRegion()== p_region)
                {
                    tauxImpactTotal += (mesureCourante.getImpactTauxDeplacementRegion()*applicationCourante.getTauxAcceptation());
                }
            }
        }
        float tauxAjuster = p_pays.getTxDepRegion()+ (p_pays.getTxDepRegion()*tauxImpactTotal);
        
        if (tauxAjuster > 0)
            return tauxAjuster;
        else
            return (float)0.0;
    }
    
    public float calculerTauxDeplacement(Lien p_lien, Pays p_paysA)
    {
        Pays paysB;
        if (p_lien.getPaysA() == p_paysA)
            paysB = p_lien.getPaysB();
        else
            paysB = p_lien.getPaysA();
        
        float tauxImpactTotalPaysA = 0;
        float tauxImpactTotalPaysB = 0;
        int compteurRegionPaysA = 0;
        int compteurRegionPaysB = 0;
        float tauxImpactSuperieur = 0;
        
        ArrayList<MesureSanitaire> p_listeMesure = getCarteDuMonde().getListeMesures();
        for (MesureSanitaire mesureCourante: p_listeMesure)
        {
            ArrayList<ListeApplicationMesure> listeApplicationMesureCourante = mesureCourante.getListeApplicationMesure();
            for (ListeApplicationMesure applicationCourante:listeApplicationMesureCourante)
            {
                if (applicationCourante.getPays() == p_paysA || applicationCourante.getPays() == paysB)
                {
                    switch (p_lien.getType())
                    {
                        case AERIEN:
                            if (mesureCourante.getImpactTauxDeplacementAerien()!= 0)
                                if (applicationCourante.getPays() == p_paysA)
                                {
                                    compteurRegionPaysA++;
                                    tauxImpactTotalPaysA += (mesureCourante.getImpactTauxDeplacementAerien()*applicationCourante.getTauxAcceptation());
                                }
                                else
                                {
                                    compteurRegionPaysB++;
                                    tauxImpactTotalPaysB += (mesureCourante.getImpactTauxDeplacementAerien()*applicationCourante.getTauxAcceptation());  
                                }
                            break;
                        case MARITIME:
                            if (mesureCourante.getImpactTauxDeplacementMaritime()!= 0)
                                if (applicationCourante.getPays() == p_paysA)
                                {
                                    compteurRegionPaysA++;
                                    tauxImpactTotalPaysA += (mesureCourante.getImpactTauxDeplacementMaritime()*applicationCourante.getTauxAcceptation());
                                }
                                else
                                {
                                    compteurRegionPaysB++;
                                    tauxImpactTotalPaysB += (mesureCourante.getImpactTauxDeplacementMaritime()*applicationCourante.getTauxAcceptation());  
                                }
                            break;
                        case TERRESTRE:
                            if (mesureCourante.getImpactTauxDeplacementTerrestre()!= 0)
                                if (applicationCourante.getPays() == p_paysA)
                                {
                                    compteurRegionPaysA++;
                                    tauxImpactTotalPaysA += (mesureCourante.getImpactTauxDeplacementTerrestre()*applicationCourante.getTauxAcceptation());
                                }
                                else
                                {
                                    compteurRegionPaysB++;
                                    tauxImpactTotalPaysB += (mesureCourante.getImpactTauxDeplacementTerrestre()*applicationCourante.getTauxAcceptation());  
                                }
                            break;
                    }
                }
            }
        }
        
        if (compteurRegionPaysA > 0)
            tauxImpactTotalPaysA /= compteurRegionPaysA;
        
        if (compteurRegionPaysB > 0)
            tauxImpactTotalPaysB /= compteurRegionPaysB;
        
        if (tauxImpactTotalPaysA <= tauxImpactTotalPaysB)
            tauxImpactSuperieur = tauxImpactTotalPaysA;
        else
            tauxImpactSuperieur = tauxImpactTotalPaysB;
        
        float tauxDeplacementPaysB=0;
        switch (p_lien.getType())
        {
        case AERIEN:
            tauxDeplacementPaysB = paysB.getTxDepAerien();
            break;
        case MARITIME:
            tauxDeplacementPaysB = paysB.getTxDepMaritime();
            break;
        case TERRESTRE:
            tauxDeplacementPaysB = paysB.getTxDepTerrestre();
            break;
        }

        tauxDeplacementPaysB *= (1+tauxImpactSuperieur);
        
        if (tauxDeplacementPaysB > 0)
            return tauxDeplacementPaysB;
        else
            return (float)0.0;
    }
    
    public float tauxTransmissionMoyenPays(Pays p_pays)
    {
        float tauxTransmissionMoyen = 0;
        ArrayList<Region> listeRegion = p_pays.getListeRegion();
        for (Region region:listeRegion)
        {
            tauxTransmissionMoyen += (calculerTauxTransmission(p_pays, region)*region.getPourcentagePopulation());
        }
        return tauxTransmissionMoyen;
    }
    
    public float tauxReproductionMoyenPays(Pays p_pays)
    {
        float tauxReproductionMoyen = 0;
        ArrayList<Region> listeRegion = p_pays.getListeRegion();
        for (Region region:listeRegion)
        {
            tauxReproductionMoyen += (calculerTauxReproduction(p_pays, region)*region.getPourcentagePopulation());
        }
        return tauxReproductionMoyen;
    }
    
    
    public int calculBinomialeDistribution(float p_taux, int p_nbrPersonnes)
    {
        double tol = 0.0001;
        
        if (p_taux > 1)
            p_taux = 1;
        
        BinomialDistribution binomial = new BinomialDistribution(p_nbrPersonnes, p_taux);
        ArrayList<Double> probabilites = new ArrayList<>();
        ArrayList<Integer> numberOfSuccess = new ArrayList<>();
        
        int x = (int)(p_taux * p_nbrPersonnes);
        double prob = binomial.probability(x);
        while (prob > tol)
        {
            probabilites.add(prob);
            numberOfSuccess.add(x);
            x++;
            prob = binomial.probability(x);
        }
        
        x = ((int)(p_taux * p_nbrPersonnes))-1;
        prob = binomial.probability(x);
        while (prob > tol && x >= 0)
        {
            probabilites.add(prob);
            numberOfSuccess.add(x);
            x--;
            prob = binomial.probability(x);
        }
        
        Random rnd = new Random();
        double randomNumber = rnd.nextDouble();
        double current = 0.0;
        int success = 0;
        
        for (int i=0; i < probabilites.size(); i++)
        {
            current += probabilites.get(i);
            if (randomNumber < current)
            {
                success = numberOfSuccess.get(i);
                break;
            }
        }
        return success;
    }
    
    public void avancerPasDeTemps()
    {
        enregistreDonneesStatistiques();
        jour++;
        ArrayList<Pays> listePays = getCarteDuMonde().getListePays();
        
        for(Pays pays:listePays)
        {
            ArrayList<Region> listeRegion = pays.getListeRegion();
            
            for(Region region:listeRegion)
            {
                //Contamination, guerison et mortalité à l'intérieur d'une région
                int populationRegion = (int)(region.getPourcentagePopulation()*pays.getPopulation());
                
                float tauxTransmission = calculerTauxTransmission(pays, region);
                float tauxGuerison = calculerTauxGuerison(pays, region);
                float tauxMortalite = calculerTauxMortalite(pays, region);
                float tauxReproduction = calculerTauxReproduction(pays, region);
                
                
                int populationInfectee = (int)(populationRegion*region.getPourcentageInfecte());
                
                int nouvelleTransmission = (int)(calculBinomialeDistribution(tauxTransmission, populationInfectee)*tauxReproduction);
                int nouvelleGuerison = calculBinomialeDistribution(tauxGuerison, populationInfectee);
                int nouvelleMortalite = calculBinomialeDistribution(tauxMortalite, populationInfectee);

                region.contaminer(nouvelleTransmission/(float)populationRegion);
                region.guerir(nouvelleGuerison/(float)populationRegion);
                region.tuer(nouvelleMortalite/(float)populationRegion);
                
                //Déplacement entre les régions
                float tauxDeplacementRegion = calculerTauxDeplacementRegion(pays, region);
                for(Region regionB:listeRegion)
                {
                    if (region != regionB)
                    {
                        int populationRegionB = (int)(regionB.getPourcentagePopulation()*pays.getPopulation());
                        float tauxTransmissionB = calculerTauxTransmission(pays, regionB);
                        float tauxReproductionB = calculerTauxReproduction(pays, regionB);
                        int nouvelleInfectionDeplacement = (int)(calculBinomialeDistribution(tauxDeplacementRegion*tauxTransmissionB, populationInfectee)*tauxReproductionB);
                        regionB.contaminer(nouvelleInfectionDeplacement/(float)populationRegionB);
                    }
                }
            }
            
            ArrayList<Lien> listeLiens = getCarteDuMonde().getListLiensPays(pays);
            
            for (Lien lien:listeLiens)
            {
                float tauxDeplacementPays = calculerTauxDeplacement(lien,pays);
                float tauxReproductionMoyen = tauxReproductionMoyenPays(pays);
                int populationInfecte = 0;
                if (lien.getPaysA()== pays)
                {
                    populationInfecte = lien.getPaysB().getPopulationInfecte();
                }
                else
                {
                    populationInfecte = lien.getPaysA().getPopulationInfecte();
                }

                int nouveauDeplacementPays = (int)(calculBinomialeDistribution(tauxDeplacementPays*tauxTransmissionMoyenPays(pays), populationInfecte)*tauxReproductionMoyen);
                pays.contaminer(nouveauDeplacementPays);
            }
        }
    }
    
    public void resetSimulation()
    {
        for (Pays pays:getCarteDuMonde().getListePays())
        {
            for (Region region:pays.getListeRegion())
            {
                region.setPourcentageIntecte(0f);
                region.setPourcentageMort(0f);
                region.setPourcentageSaine(1f);
            }
        }
        setJour(0);
    }
}

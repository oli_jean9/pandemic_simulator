/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package domain;

/**
 *
 * @author alexa
 */
public class ListeApplicationMesure implements java.io.Serializable {
    private Pays pays;
    private Region region;
    private float tauxAcceptation;
    
    public ListeApplicationMesure(Pays p_Pays, Region p_Region, 
            float p_tauxAcceptation) {
        pays = p_Pays;
        region = p_Region;
        tauxAcceptation = p_tauxAcceptation;
    }

    public Pays getPays(){
        return pays;
    }
    public Region getRegion(){
        return region;
    }
    public float getTauxAcceptation(){
        return tauxAcceptation;
    }
    public void setNomPays(Pays p_pays){
        pays = p_pays;
    }
    public void setNomRegion(Region p_region){
        region = p_region;
    }
    public void setTauxAcceptation(float p_tauxAcceptation){
        tauxAcceptation = p_tauxAcceptation;
    }
}

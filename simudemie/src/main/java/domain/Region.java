package domain;

import java.util.ArrayList;

public class Region implements java.io.Serializable{
    
    private String nomRegion;
    private float pourcentagePopulation;
    private float pourcentageSaine;
    private float pourcentageInfecte;
    private float pourcentageMort;
    private Forme forme;
    
    private ArrayList<Float> pourcentageSaineParJour;
    private ArrayList<Float> pourcentageInfecteParJour;
    private ArrayList<Float> pourcentageMortParJour;
    
    public Region(String p_nomRegion,
            float p_pourcentagePopulation,
            float p_pourcentageSaine,
            float p_pourcentageInfecte,
            float p_pourcentageMort,
            Forme p_forme)
    {
        nomRegion = p_nomRegion;
        pourcentagePopulation = p_pourcentagePopulation;
        pourcentageSaine = p_pourcentageSaine;
        pourcentageInfecte = p_pourcentageInfecte;
        pourcentageMort = p_pourcentageMort;
        forme = p_forme;
        
        pourcentageSaineParJour = new ArrayList<Float>();
        pourcentageInfecteParJour = new ArrayList<Float>();
        pourcentageMortParJour = new ArrayList<Float>();
    }
    
    public void enregistreDonneesStatistiques(){

        pourcentageSaineParJour.add(pourcentageSaine);
        pourcentageInfecteParJour.add(pourcentageInfecte);
        pourcentageMortParJour.add(pourcentageMort);
    }
    
    public float retourneStatistiqueSain(int jour){
        return pourcentageSaineParJour.get(jour-1);
    }
    public float retourneStatistiqueInfecte(int jour){
        return pourcentageInfecteParJour.get(jour-1);
    }
    public float retourneStatistiqueMort(int jour){
        return pourcentageMortParJour.get(jour-1);
    }
    
    
    public void setForme(FormeIrreguliere f){
        forme = f;
    }

    public Forme getForme()
    {
        return forme;
    }
    
    public String getNomRegion()
    {
        return nomRegion;
    }
    
    public void setNomRegion(String p_nomRegion)
    {
        nomRegion = p_nomRegion;
    }
    
    public float getPourcentagePopulation()
    {
        return pourcentagePopulation;
    }
    
    public void setPorcentagePopulation(float p_pourcentagePopulation)
    {
        pourcentagePopulation = p_pourcentagePopulation;
    }
    
    public float getPourcentageSaine()
    {
        return pourcentageSaine;
    }
    
    public void setPourcentageSaine(float p_pourcentageSaine)
    {
        pourcentageSaine = p_pourcentageSaine;
    }
    
    public float getPourcentageInfecte()
    {
        return pourcentageInfecte;
    }
    
    public void setPourcentageIntecte(float p_pourcentageIntecte)
    {
        pourcentageInfecte = p_pourcentageIntecte;
    }
    
    public float getPourcentageMort()
    {
        return pourcentageMort;
    }
    
    public void setPourcentageMort(float p_pourcentageMort)
    {
        pourcentageMort = p_pourcentageMort;
    }
    
    public void contaminer(float p_pourcentageContamine)
    {
        if (pourcentageSaine > 0.0)
        {
            if (p_pourcentageContamine > pourcentageSaine)
            {
                pourcentageInfecte += pourcentageSaine;
                pourcentageSaine = (float)0.0;
            }
            
            else
            {
                pourcentageInfecte += p_pourcentageContamine;
                pourcentageSaine -= p_pourcentageContamine;
            }
        }
    }
    
    public void guerir(float p_pourcentageGueri)
    {
        pourcentageSaine += p_pourcentageGueri;
        pourcentageInfecte -= p_pourcentageGueri;
    }
    
    public void tuer(float p_pourcentageTuer)
    {
        pourcentageInfecte -= p_pourcentageTuer;
        pourcentageMort += p_pourcentageTuer;
    }
    
    public ArrayList<Float> getPopulationSaineStats()
    {
        return pourcentageSaineParJour;
    }
    
    public ArrayList<Float> getPopulationInfecteStats()
    {
        return pourcentageInfecteParJour;
    }
    
    public ArrayList<Float> getPopulationMortStats()
    {
        return pourcentageMortParJour;
    }
    
    public void clearStats()
    {
        pourcentageInfecteParJour.clear();
        pourcentageMortParJour.clear();
        pourcentageSaineParJour.clear();
    }
}

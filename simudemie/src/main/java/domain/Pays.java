package domain;
// Oussama
import java.util.*;

public class Pays implements java.io.Serializable, Cloneable{
    private int population;
    private ArrayList<Region> listeRegion;
    private String nomPays;
    private Forme forme;
    private float txDepMaritime;
    private float txDepTerrestre;
    private float txDepAerien;
    private float txDepRegion;
    
    private ArrayList<Float> pourcentageSaineParJour;
    private ArrayList<Float> pourcentageInfecteParJour;
    private ArrayList<Float> pourcentageMortParJour;
     

    public Pays(int _pop,
            String _nomPays,
            Forme _forme,
            float _txDepMaritime,
            float _txDepTerrestre,
            float _txDepAerien,
            float _txDepRegion,
            float _pctPopSaine,
            float _pctPopInfecte,
            float _pctPopMorte){
        population = _pop;
        listeRegion = new ArrayList<Region>();
        
        listeRegion.add(new Region(_nomPays,1,_pctPopSaine,_pctPopInfecte,_pctPopMorte, new FormeIrreguliere(_forme.getListePoints())));
        nomPays = _nomPays;
        forme = _forme;
        txDepMaritime = _txDepMaritime;
        txDepTerrestre = _txDepTerrestre;
        txDepAerien = _txDepAerien;
        txDepRegion = _txDepRegion;
        
        pourcentageSaineParJour = new ArrayList<Float>();
        pourcentageInfecteParJour = new ArrayList<Float>();
        pourcentageMortParJour = new ArrayList<Float>();
    }
    public Pays(Pays pays){
        population = pays.getPopulation();
        listeRegion = pays.getListeRegion();
        nomPays = pays.getNomPays();
        forme = pays.getForme();
        txDepMaritime = pays.getTxDepMaritime();
        txDepTerrestre = pays.getTxDepTerrestre();
        txDepAerien = pays.getTxDepAerien();
        txDepRegion = pays.getTxDepRegion();
    }
    
    public void setPays(Pays pays){
        population = pays.getPopulation();
        listeRegion = pays.getListeRegion();
        nomPays = pays.getNomPays();
        forme = pays.getForme();
        txDepMaritime = pays.getTxDepMaritime();
        txDepTerrestre = pays.getTxDepTerrestre();
        txDepAerien = pays.getTxDepAerien();
        txDepRegion = pays.getTxDepRegion();
    }
    public void setPopulation(int _pop){
        population = _pop;
    }
    public void creerRegion(String _nomRegion, float _pourcentagePopulation,
            float p_pourcentageSaine,
            float p_pourcentageInfecte,
            float p_pourcentageMort, Forme p_forme)
    {
        Region nouvelleRegion = new Region(_nomRegion,
                _pourcentagePopulation,
                p_pourcentageSaine,
                p_pourcentageInfecte,
                p_pourcentageMort, p_forme);
        listeRegion.add(nouvelleRegion);
    }
    
    public Boolean regionExiste(String regNom){
        for (Region r: listeRegion){
            if (r.getNomRegion().equals(regNom)){
                return true;
            }
        }
        return false;
    }
    
    public void supprimerRegions(){
        listeRegion.clear();
    }
    
    public String getNomPays(){
        return nomPays;
    }
    public void setNomPays(String _nom) {
        nomPays = _nom;
    }
    public int getPopulation(){
        return population;
    }
    public float getTxDepMaritime(){
        return txDepMaritime;
    }
    public float getTxDepAerien(){
        return txDepAerien;
    }
    public float getTxDepTerrestre(){
        return txDepTerrestre;
    }
    public float getTxDepRegion(){
        return txDepRegion;
    }
    public ArrayList<Region> getListeRegion(){
        return listeRegion;
    }
    public Forme getForme(){
        return forme;
    }
    public void setForme(Forme _forme){
        forme = _forme;
    }
    public void setTxDepMaritime(float _tx){
        txDepMaritime = _tx;
    }
    public void setTxDepTerrestre(float _tx){
        txDepTerrestre = _tx;
    }
    public void setTxDepAerien(float _tx) {
        txDepAerien = _tx;
    }
    public void setTxDepRegion(float _tx){
        txDepRegion = _tx;
    }
    
    public int getPopulationInfecte()
    {
        int populationInfecte=0;
        ArrayList<Region> listeRegion = getListeRegion();
        for (Region region:listeRegion)
        {
            populationInfecte += (int)(region.getPourcentageInfecte()*(region.getPourcentagePopulation()*getPopulation()));
        }
        return populationInfecte;
    }
    
    public int getPopulationSaine()
    {
        int populationSaine=0;
        ArrayList<Region> listeRegion = getListeRegion();
        for (Region region:listeRegion)
        {
            populationSaine += (region.getPourcentageSaine()*(region.getPourcentagePopulation()*getPopulation()));
        }
        return populationSaine;
    }
    
    public int getPopulationMorte()
    {
        int populationMorte=0;
        ArrayList<Region> listeRegion = getListeRegion();
        for (Region region:listeRegion)
        {
            populationMorte += (region.getPourcentageMort()*(region.getPourcentagePopulation()*getPopulation()));
        }
        return populationMorte;
    }
            
    
    public void contaminer(int p_NbrPersonnes)
    {
        ArrayList<Region> listeRegion = getListeRegion();
        for (Region region:listeRegion)
        {
            int nouveauContaminer = (int)(p_NbrPersonnes*region.getPourcentagePopulation());
            region.contaminer(nouveauContaminer/(region.getPourcentagePopulation()*getPopulation()));
        }
    }
    
    public void enregistreDonneesStatistiques(){

        float pourcentageSaine = getPopulationSaine()/(float)getPopulation();
        pourcentageSaineParJour.add(pourcentageSaine);
        
        float pourcentageInfecte = getPopulationInfecte()/(float)getPopulation();
        pourcentageInfecteParJour.add(pourcentageInfecte);
        
        float pourcentageMort = getPopulationMorte()/(float)getPopulation();
        pourcentageMortParJour.add(pourcentageMort);
    }
    
    public ArrayList<Float> getPopulationSaineStats()
    {
        return pourcentageSaineParJour;
    }
    
    public ArrayList<Float> getPopulationInfecteStats()
    {
        return pourcentageInfecteParJour;
    }
    
    public ArrayList<Float> getPopulationMortStats()
    {
        return pourcentageMortParJour;
    }
    
    public void clearStats()
    {
        pourcentageInfecteParJour.clear();
        pourcentageMortParJour.clear();
        pourcentageSaineParJour.clear();
    }
}
